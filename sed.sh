#! /bin/bash

sed -i 's/"TBed.h"/"genometools\/BED\/TBed.h"/g' $1
sed -i 's/"TChromosomes.h"/"genometools\/GenomePositions\/TChromosomes.h"/g' $1
sed -i 's/"TDistances.h"/"genometools\/GenomePositions\/TDistances.h"/g' $1
sed -i 's/"TGenomePosition.h"/"genometools\/GenomePositions\/TGenomePosition.h"/g' $1
sed -i 's/"TNamesPositions.h"/"genometools\/GenomePositions\/TNamesPositions.h"/g' $1
sed -i 's/"GenotypeTypes.h"/"genometools\/GenotypeTypes.h"/g' $1
sed -i 's/"PhredProbabilityTypes.h"/"genometools\/PhredProbabilityTypes.h"/g' $1
sed -i 's/"PhredProbabilityTypesTables.h"/"genometools\/PhredProbabilityTypesTables.h"/g' $1
sed -i 's/"TGenotypeFrequencies.h"/"genometools\/TGenotypeFrequencies.h"/g' $1
sed -i 's/"THardyWeinbergGenotypeProbabilities.h"/"genometools\/THardyWeinbergGenotypeProbabilities.h"/g' $1
sed -i 's/"TSampleLikelihoods.h"/"genometools\/TSampleLikelihoods.h"/g' $1
sed -i 's/"TPopulation.h"/"genometools\/VCF\/TPopulation.h"/g' $1
sed -i 's/"TPopulationLikelihoodLocus.h"/"genometools\/VCF\/TPopulationLikelihoodLocus.h"/g' $1
sed -i 's/"TPopulationLikelihoods.h"/"genometools\/VCF\/TPopulationLikelihoods.h"/g' $1
sed -i 's/"TVCFFields.h"/"genometools\/VCF\/TVCFFields.h"/g' $1
sed -i 's/"TVcfFile.h"/"genometools\/VCF\/TVcfFile.h"/g' $1
sed -i 's/"TVcfParser.h"/"genometools\/VCF\/TVcfParser.h"/g' $1
