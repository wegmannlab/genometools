//
// Created by caduffm on 6/15/21.
//

#include "gtest/gtest.h"
#include <cstdint>

#include "coretools/Types/probability.h"
#include "genometools/TSampleLikelihoods.h"

using namespace genometools;
using coretools::LogProbability;
using coretools::Probability;
using coretools::PhredInt;
using coretools::HPPhredInt;

//-----------------------------------
// Type = PhredInt
// type should not matter here, can also be Probability or HPPhredInt
//-----------------------------------

TEST(SampleGenotypeLikelihoodsTests, PhredInt_diploid_missing) {
	using BG = BiallelicGenotype;
	// default constructor: missing diploid
	TSampleLikelihoods<PhredInt> sampleLL;

	EXPECT_TRUE(sampleLL.isMissing());
	EXPECT_FALSE(sampleLL.isHaploid());
	EXPECT_EQ(sampleLL[BG::homoFirst], 0);
	EXPECT_EQ(sampleLL[BG::het], 0);
	EXPECT_EQ(sampleLL[BG::homoSecond], 0);
	EXPECT_ANY_THROW(sampleLL[BG::missingDiploid]);
	EXPECT_ANY_THROW(sampleLL[BG::haploidFirst]);
	EXPECT_ANY_THROW(sampleLL[BG::haploidSecond]);
	EXPECT_ANY_THROW(sampleLL[BG::missingHaploid]);

	// initialize to non-missing haploid
	TSampleLikelihoods sampleLL2(PhredInt::highest(), PhredInt((uint8_t)100));
	EXPECT_FALSE(sampleLL2.isMissing());
	EXPECT_TRUE(sampleLL2.isHaploid());

	// set to missing diploid
	sampleLL2.setMissingDiploid();
	EXPECT_TRUE(sampleLL2.isMissing());
	EXPECT_FALSE(sampleLL2.isHaploid());
	EXPECT_EQ(sampleLL2[BG::homoFirst], 0);
	EXPECT_EQ(sampleLL2[BG::het], 0);
	EXPECT_EQ(sampleLL2[BG::homoSecond], 0);
	EXPECT_ANY_THROW(sampleLL2[BG::missingDiploid]);
	EXPECT_ANY_THROW(sampleLL2[BG::haploidFirst]);
	EXPECT_ANY_THROW(sampleLL2[BG::haploidSecond]);
	EXPECT_ANY_THROW(sampleLL2[BG::missingHaploid]);
}

TEST(SampleGenotypeLikelihoodsTests, PhredInt_haploid_missing) {
	using BG = BiallelicGenotype;
	// initialize non-missing diploid
	TSampleLikelihoods sampleLL(PhredInt((uint8_t)0), PhredInt((uint8_t)20), PhredInt((uint8_t)100));
	EXPECT_FALSE(sampleLL.isMissing());
	EXPECT_FALSE(sampleLL.isHaploid());

	// set to missing
	sampleLL.setMissingHaploid();
	EXPECT_TRUE(sampleLL.isMissing());
	EXPECT_TRUE(sampleLL.isHaploid());
	EXPECT_EQ(sampleLL[BG::haploidFirst], 0);
	EXPECT_EQ(sampleLL[BG::haploidSecond], 0);
	EXPECT_ANY_THROW(sampleLL[BG::missingHaploid]);
	EXPECT_ANY_THROW(sampleLL[BG::homoFirst]);
	EXPECT_ANY_THROW(sampleLL[BG::het]);
	EXPECT_ANY_THROW(sampleLL[BG::homoSecond]);
	EXPECT_ANY_THROW(sampleLL[BG::missingDiploid]);
}

TEST(SampleGenotypeLikelihoodsTests, PhredInt_nonMissing_haploid) {
	using BG = BiallelicGenotype;
	// haploid constructor
	TSampleLikelihoods<PhredInt> sampleLL(PhredInt((uint8_t)0), PhredInt((uint8_t)100));

	EXPECT_FALSE(sampleLL.isMissing());
	EXPECT_TRUE(sampleLL.isHaploid());

	EXPECT_EQ(sampleLL[BG::haploidFirst].get(), 0);
	EXPECT_EQ(sampleLL[BG::haploidSecond].get(), 100);
	EXPECT_ANY_THROW(sampleLL[BG::missingHaploid]);
	EXPECT_ANY_THROW(sampleLL[BG::homoFirst]);
	EXPECT_ANY_THROW(sampleLL[BG::het]);
	EXPECT_ANY_THROW(sampleLL[BG::homoSecond]);
	EXPECT_ANY_THROW(sampleLL[BG::missingDiploid]);

	EXPECT_EQ(sampleLL.mostLikelyGenotype(), BG::haploidFirst);
	EXPECT_EQ(sampleLL.secondMostLikelyGenotype(), BG::haploidSecond);
	EXPECT_FLOAT_EQ(sampleLL.meanPosteriorGenotype(), 1e-10);
	EXPECT_FLOAT_EQ(sampleLL.meanPosteriorAlleleFrequency().get(), 1e-10);

	// initialize to non-missing diploid
	TSampleLikelihoods sampleLL2(PhredInt((uint8_t)20), PhredInt((uint8_t)100), PhredInt((uint8_t)0));
	// ... and set to haploid
	sampleLL2.setHaploid(PhredInt((uint8_t)100), PhredInt((uint8_t)0));

	EXPECT_FALSE(sampleLL2.isMissing());
	EXPECT_TRUE(sampleLL2.isHaploid());
	EXPECT_EQ(sampleLL2[BG::haploidFirst].get(), 100);
	EXPECT_EQ(sampleLL2[BG::haploidSecond].get(), 0);
	EXPECT_ANY_THROW(sampleLL2[BG::missingHaploid]);
	EXPECT_ANY_THROW(sampleLL2[BG::homoFirst]);
	EXPECT_ANY_THROW(sampleLL2[BG::het]);
	EXPECT_ANY_THROW(sampleLL2[BG::homoSecond]);
	EXPECT_ANY_THROW(sampleLL2[BG::missingDiploid]);

	EXPECT_EQ(sampleLL2.mostLikelyGenotype(), BG::haploidSecond);
	EXPECT_EQ(sampleLL2.secondMostLikelyGenotype(), BG::haploidFirst);
	EXPECT_FLOAT_EQ(sampleLL2.meanPosteriorGenotype(), 1.);
	EXPECT_FLOAT_EQ(sampleLL2.meanPosteriorAlleleFrequency().get(), 1.);
}

TEST(SampleGenotypeLikelihoodsTests, PhredInt_nonMissing_diploid) {
	using BG = BiallelicGenotype;
	// diploid constructor, first is max
	TSampleLikelihoods<PhredInt> sampleLL(PhredInt((uint8_t)0), PhredInt((uint8_t)100),
	                                                 PhredInt((uint8_t)20));

	EXPECT_FALSE(sampleLL.isMissing());
	EXPECT_FALSE(sampleLL.isHaploid());

	EXPECT_EQ(sampleLL[BG::homoFirst].get(), 0);
	EXPECT_EQ(sampleLL[BG::het].get(), 100);
	EXPECT_EQ(sampleLL[BG::homoSecond].get(), 20);
	EXPECT_ANY_THROW(sampleLL[BG::missingDiploid]);
	EXPECT_ANY_THROW(sampleLL[BG::haploidFirst]);
	EXPECT_ANY_THROW(sampleLL[BG::haploidSecond]);
	EXPECT_ANY_THROW(sampleLL[BG::missingHaploid]);

	EXPECT_EQ(sampleLL.mostLikelyGenotype(), BG::homoFirst);
	EXPECT_EQ(sampleLL.secondMostLikelyGenotype(), BG::homoSecond);
	EXPECT_FLOAT_EQ(sampleLL.meanPosteriorGenotype(), 0.01980198);
	EXPECT_FLOAT_EQ(sampleLL.meanPosteriorAlleleFrequency().get(), 0.00990099);

	// set haploid to diploid, second is max
	TSampleLikelihoods sampleLL2(PhredInt((uint8_t)100), PhredInt((uint8_t)0));
	sampleLL2.setDiploid(PhredInt((uint8_t)20), PhredInt((uint8_t)0), PhredInt((uint8_t)100));

	EXPECT_FALSE(sampleLL2.isMissing());
	EXPECT_FALSE(sampleLL2.isHaploid());

	EXPECT_EQ(sampleLL2[BG::homoFirst].get(), 20);
	EXPECT_EQ(sampleLL2[BG::het].get(), 0);
	EXPECT_EQ(sampleLL2[BG::homoSecond].get(), 100);
	EXPECT_ANY_THROW(sampleLL2[BG::missingDiploid]);
	EXPECT_ANY_THROW(sampleLL2[BG::haploidFirst]);
	EXPECT_ANY_THROW(sampleLL2[BG::haploidSecond]);
	EXPECT_ANY_THROW(sampleLL2[BG::missingHaploid]);

	EXPECT_EQ(sampleLL2.mostLikelyGenotype(), BG::het);
	EXPECT_EQ(sampleLL2.secondMostLikelyGenotype(), BG::homoFirst);
	EXPECT_FLOAT_EQ(sampleLL2.meanPosteriorGenotype(), 0.990099);
	EXPECT_FLOAT_EQ(sampleLL2.meanPosteriorAlleleFrequency().get(), 0.4950495);

	// set to another diploid, third is max
	sampleLL2.setDiploid(PhredInt((uint8_t)100), PhredInt((uint8_t)20), PhredInt((uint8_t)0));
	EXPECT_FALSE(sampleLL2.isMissing());
	EXPECT_FALSE(sampleLL2.isHaploid());
	EXPECT_EQ(sampleLL2[BG::homoFirst].get(), 100);
	EXPECT_EQ(sampleLL2[BG::het].get(), 20);
	EXPECT_EQ(sampleLL2[BG::homoSecond].get(), 0);
	EXPECT_ANY_THROW(sampleLL2[BG::missingDiploid]);
	EXPECT_ANY_THROW(sampleLL2[BG::haploidFirst]);
	EXPECT_ANY_THROW(sampleLL2[BG::haploidSecond]);
	EXPECT_ANY_THROW(sampleLL2[BG::missingHaploid]);

	EXPECT_EQ(sampleLL2.mostLikelyGenotype(), BG::homoSecond);
	EXPECT_EQ(sampleLL2.secondMostLikelyGenotype(), BG::het);
	EXPECT_FLOAT_EQ(sampleLL2.meanPosteriorGenotype(), 1.990099);
	EXPECT_FLOAT_EQ(sampleLL2.meanPosteriorAlleleFrequency().get(), 0.9950495);
}

TEST(SampleGenotypeLikelihoodsTests, PhredInt_nonMissing_diploid_notNormalized) {
	using BG = BiallelicGenotype;
	// diploid constructor, first is max
	TSampleLikelihoods<PhredInt> sampleLL(PhredInt((uint8_t)15), PhredInt((uint8_t)100),
	                                                 PhredInt((uint8_t)50));

	EXPECT_FALSE(sampleLL.isMissing());
	EXPECT_FALSE(sampleLL.isHaploid());
	EXPECT_EQ(sampleLL[BG::homoFirst].get(), 0);
	EXPECT_EQ(sampleLL[BG::het].get(), 85);
	EXPECT_EQ(sampleLL[BG::homoSecond].get(), 35);
	EXPECT_ANY_THROW(sampleLL[BG::missingDiploid]);
	EXPECT_ANY_THROW(sampleLL[BG::haploidFirst]);
	EXPECT_ANY_THROW(sampleLL[BG::haploidSecond]);
	EXPECT_ANY_THROW(sampleLL[BG::missingHaploid]);

	EXPECT_EQ(sampleLL.mostLikelyGenotype(), BG::homoFirst);
	EXPECT_EQ(sampleLL.secondMostLikelyGenotype(), BG::homoSecond);
	EXPECT_FLOAT_EQ(sampleLL.meanPosteriorGenotype(), 0.0006322588);
	EXPECT_FLOAT_EQ(sampleLL.meanPosteriorAlleleFrequency().get(), 0.0006322588 / 2.);

	// set, second is max
	sampleLL.setDiploid(PhredInt((uint8_t)50), PhredInt((uint8_t)15), PhredInt((uint8_t)100));

	EXPECT_FALSE(sampleLL.isMissing());
	EXPECT_FALSE(sampleLL.isHaploid());
	EXPECT_EQ(sampleLL[BG::homoFirst].get(), 35);
	EXPECT_EQ(sampleLL[BG::het].get(), 0);
	EXPECT_EQ(sampleLL[BG::homoSecond].get(), 85);
	EXPECT_ANY_THROW(sampleLL[BG::missingDiploid]);
	EXPECT_ANY_THROW(sampleLL[BG::haploidFirst]);
	EXPECT_ANY_THROW(sampleLL[BG::haploidSecond]);
	EXPECT_ANY_THROW(sampleLL[BG::missingHaploid]);

	EXPECT_EQ(sampleLL.mostLikelyGenotype(), BG::het);
	EXPECT_EQ(sampleLL.secondMostLikelyGenotype(), BG::homoFirst);
	EXPECT_FLOAT_EQ(sampleLL.meanPosteriorGenotype(), 0.9996839);
	EXPECT_FLOAT_EQ(sampleLL.meanPosteriorAlleleFrequency().get(), 0.9996839 / 2.);

	// set, third is max
	sampleLL.setDiploid(PhredInt((uint8_t)100), PhredInt((uint8_t)50), PhredInt((uint8_t)15));

	EXPECT_FALSE(sampleLL.isMissing());
	EXPECT_FALSE(sampleLL.isHaploid());
	EXPECT_EQ(sampleLL[BG::homoFirst].get(), 85);
	EXPECT_EQ(sampleLL[BG::het].get(), 35);
	EXPECT_EQ(sampleLL[BG::homoSecond].get(), 0);
	EXPECT_ANY_THROW(sampleLL[BG::missingDiploid]);
	EXPECT_ANY_THROW(sampleLL[BG::haploidFirst]);
	EXPECT_ANY_THROW(sampleLL[BG::haploidSecond]);
	EXPECT_ANY_THROW(sampleLL[BG::missingHaploid]);
	EXPECT_EQ(sampleLL.mostLikelyGenotype(), BG::homoSecond);
	EXPECT_EQ(sampleLL.secondMostLikelyGenotype(), BG::het);
	EXPECT_FLOAT_EQ(sampleLL.meanPosteriorGenotype(), 1.999684);
	EXPECT_FLOAT_EQ(sampleLL.meanPosteriorAlleleFrequency().get(), 1.999684 / 2.);
}

TEST(SampleGenotypeLikelihoodsTests, PhredInt_nonMissing_haploid_notNormalized) {
	using BG = BiallelicGenotype;
	// haploid constructor, first is max
	TSampleLikelihoods<PhredInt> sampleLL(PhredInt((uint8_t)15), PhredInt((uint8_t)100));

	EXPECT_FALSE(sampleLL.isMissing());
	EXPECT_TRUE(sampleLL.isHaploid());
	EXPECT_EQ(sampleLL[BG::haploidFirst].get(), 0);
	EXPECT_EQ(sampleLL[BG::haploidSecond].get(), 85);
	EXPECT_ANY_THROW(sampleLL[BG::missingHaploid]);
	EXPECT_ANY_THROW(sampleLL[BG::homoFirst]);
	EXPECT_ANY_THROW(sampleLL[BG::het]);
	EXPECT_ANY_THROW(sampleLL[BG::homoSecond]);
	EXPECT_ANY_THROW(sampleLL[BG::missingDiploid]);

	EXPECT_EQ(sampleLL.mostLikelyGenotype(), BG::haploidFirst);
	EXPECT_EQ(sampleLL.secondMostLikelyGenotype(), BG::haploidSecond);
	EXPECT_FLOAT_EQ(sampleLL.meanPosteriorGenotype(), 3.162278e-09);
	EXPECT_FLOAT_EQ(sampleLL.meanPosteriorAlleleFrequency().get(), 3.162278e-09);

	// set, second is max
	sampleLL.setHaploid(PhredInt((uint8_t)50), PhredInt((uint8_t)15));

	EXPECT_FALSE(sampleLL.isMissing());
	EXPECT_TRUE(sampleLL.isHaploid());
	EXPECT_EQ(sampleLL[BG::haploidFirst].get(), 35);
	EXPECT_EQ(sampleLL[BG::haploidSecond].get(), 0);
	EXPECT_ANY_THROW(sampleLL[BG::missingHaploid]);
	EXPECT_ANY_THROW(sampleLL[BG::homoFirst]);
	EXPECT_ANY_THROW(sampleLL[BG::het]);
	EXPECT_ANY_THROW(sampleLL[BG::homoSecond]);
	EXPECT_ANY_THROW(sampleLL[BG::missingDiploid]);

	EXPECT_EQ(sampleLL.mostLikelyGenotype(), BG::haploidSecond);
	EXPECT_EQ(sampleLL.secondMostLikelyGenotype(), BG::haploidFirst);
	EXPECT_FLOAT_EQ(sampleLL.meanPosteriorGenotype(), 0.9996839);
	EXPECT_FLOAT_EQ(sampleLL.meanPosteriorAlleleFrequency().get(), 0.9996839);
}

TEST(SampleGenotypeLikelihoodsTests, PhredInt_Missing_diploid_HWE_Sum) {
	// diploid constructor, first is max
	TSampleLikelihoods<PhredInt> sampleLL(PhredInt((uint8_t)0), PhredInt((uint8_t)100),
	                                                 PhredInt((uint8_t)20));
	sampleLL.setMissingDiploid();

	Probability f(0.46383);
	EXPECT_FLOAT_EQ(sampleLL.HWESum<Probability>(f), 1.);
	EXPECT_FLOAT_EQ(sampleLL.HWESum<LogProbability>(f), log(1.));

	THardyWeinbergGenotypeProbabilities HWProbs(f);
	EXPECT_FLOAT_EQ(sampleLL.HWESum<Probability>(HWProbs), 1.);
	EXPECT_FLOAT_EQ(sampleLL.HWESum<LogProbability>(HWProbs), log(1.));
}

TEST(SampleGenotypeLikelihoodsTests, PhredInt_Missing_haploid_HWE_Sum) {
	// diploid constructor, first is max
	TSampleLikelihoods<PhredInt> sampleLL(PhredInt((uint8_t)0), PhredInt((uint8_t)20));
	sampleLL.setMissingHaploid();

	Probability f(0.46383);
	EXPECT_FLOAT_EQ(sampleLL.HWESum<Probability>(f), 1.);
	EXPECT_FLOAT_EQ(sampleLL.HWESum<LogProbability>(f), log(1.));

	THardyWeinbergGenotypeProbabilities HWProbs(f);
	EXPECT_FLOAT_EQ(sampleLL.HWESum<Probability>(HWProbs), 1.);
	EXPECT_FLOAT_EQ(sampleLL.HWESum<LogProbability>(HWProbs), log(1.));
}

TEST(SampleGenotypeLikelihoodsTests, PhredInt_NonMissing_diploid_HWE_Sum) {
	Probability f(0.36);
	THardyWeinbergGenotypeProbabilities HWProbs(f);

	// diploid constructor, first is max
	TSampleLikelihoods<PhredInt> sampleLL(PhredInt((uint8_t)0), PhredInt((uint8_t)20),
	                                                 PhredInt((uint8_t)10));
	EXPECT_FLOAT_EQ(sampleLL.HWESum<Probability>(f), 0.427168);
	EXPECT_FLOAT_EQ(sampleLL.HWESum<LogProbability>(f), log(0.427168));
	EXPECT_FLOAT_EQ(sampleLL.HWESum<Probability>(HWProbs), 0.427168);
	EXPECT_FLOAT_EQ(sampleLL.HWESum<LogProbability>(HWProbs), log(0.427168));

	// second is max
	sampleLL.setDiploid(PhredInt((uint8_t)20), PhredInt((uint8_t)0), PhredInt((uint8_t)15));
	EXPECT_FLOAT_EQ(sampleLL.HWESum<Probability>(f), 0.4689943118);
	EXPECT_FLOAT_EQ(sampleLL.HWESum<LogProbability>(f), log(0.4689943118));
	EXPECT_FLOAT_EQ(sampleLL.HWESum<Probability>(HWProbs), 0.4689943118);
	EXPECT_FLOAT_EQ(sampleLL.HWESum<LogProbability>(HWProbs), log(0.4689943118));

	// third is max
	sampleLL.setDiploid(PhredInt((uint8_t)15), PhredInt((uint8_t)20), PhredInt((uint8_t)0));
	EXPECT_FLOAT_EQ(sampleLL.HWESum<Probability>(f), 0.1471606893);
	EXPECT_FLOAT_EQ(sampleLL.HWESum<LogProbability>(f), log(0.1471606893));
	EXPECT_FLOAT_EQ(sampleLL.HWESum<Probability>(HWProbs), 0.1471606893);
	EXPECT_FLOAT_EQ(sampleLL.HWESum<LogProbability>(HWProbs), log(0.1471606893));
}

TEST(SampleGenotypeLikelihoodsTests, PhredInt_NonMissing_haploid_HWE_Sum) {
	Probability f(0.36);
	THardyWeinbergGenotypeProbabilities HWProbs(f);

	// diploid constructor, first is max
	TSampleLikelihoods<PhredInt> sampleLL(PhredInt((uint8_t)0), PhredInt((uint8_t)15));
	EXPECT_FLOAT_EQ(sampleLL.HWESum<Probability>(f), 0.6513841996);
	EXPECT_FLOAT_EQ(sampleLL.HWESum<LogProbability>(f), log(0.6513841996));
	EXPECT_FLOAT_EQ(sampleLL.HWESum<Probability>(HWProbs), 0.6513841996);
	EXPECT_FLOAT_EQ(sampleLL.HWESum<LogProbability>(HWProbs), log(0.6513841996));

	// second is max
	sampleLL.setHaploid(PhredInt((uint8_t)20), PhredInt((uint8_t)0));
	EXPECT_FLOAT_EQ(sampleLL.HWESum<Probability>(f), 0.3664);
	EXPECT_FLOAT_EQ(sampleLL.HWESum<LogProbability>(f), log(0.3664));
	EXPECT_FLOAT_EQ(sampleLL.HWESum<Probability>(HWProbs), 0.3664);
	EXPECT_FLOAT_EQ(sampleLL.HWESum<LogProbability>(HWProbs), log(0.3664));
}
