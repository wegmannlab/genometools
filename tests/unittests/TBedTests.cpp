//
// Created by caduffm on 8/4/20.
//
#include "genometools/TBed.h"
#include "gtest/gtest.h"
#include "coretools/Files/TOutputFile.h"


using namespace genometools;

TEST(TBedTest, empty) {
	TBed bed1;
	EXPECT_TRUE(bed1.empty());
	EXPECT_EQ(bed1.size(), 0);
	EXPECT_EQ(bed1.refID("doesNotExist"), 0); // same as size

	TBed bed2{""};
	EXPECT_TRUE(bed2.empty());
	EXPECT_EQ(bed2.size(), 0);
	EXPECT_EQ(bed2.refID("doesNotExist"), 0);

	TBed bed3;
	bed3.parse("");
	EXPECT_TRUE(bed3.empty());
	EXPECT_EQ(bed3.size(), 0);
	EXPECT_EQ(bed3.refID("doesNotExist"), 0);
}


TEST(TBedTest, noChrom) {
	TBed bed("chr1 0 100;chr3 10 99;chr1 110 120"); // will get sorted

	// bed properties
	EXPECT_FALSE(bed.empty());
	EXPECT_EQ(bed.refID("doesNotExist"), bed.NChr());

	// chr properties
	EXPECT_EQ(bed.NChr(), 2);
	EXPECT_EQ(bed.NChrWindows(), 2);
	EXPECT_EQ(bed.refID("chr1"), 0);
	EXPECT_EQ(bed.refID("chr3"), 1);
	EXPECT_EQ(bed.name(0), "chr1");
	EXPECT_EQ(bed.name(1), "chr3");

	// windows properties
	EXPECT_EQ(bed.size(), 3);
	EXPECT_EQ(bed.length(), 199);

	EXPECT_EQ(bed.size(0), 2);
	EXPECT_EQ(bed.size("chr1"), 2);
	EXPECT_FALSE(bed.empty(0));

	EXPECT_EQ(bed.size(1), 1);
	EXPECT_EQ(bed.size("chr3"), 1);
	EXPECT_FALSE(bed.empty("chr3"));
}

TEST(TBedTest, withChrom) {
	TBed bed("chr3 10 99;chr1 0 100;chr1 110 120", {"chr1", "chr2", "chr3", "chr4", "chr5"}); // will get sorted

	// bed properties
	EXPECT_FALSE(bed.empty());
	EXPECT_EQ(bed.refID("doesNotExist"), bed.NChr());

	// chr properties
	EXPECT_EQ(bed.NChr(), 5);
	EXPECT_EQ(bed.NChrWindows(), 2);
	EXPECT_EQ(bed.refID("chr1"), 0);
	EXPECT_EQ(bed.refID("chr2"), 1);
	EXPECT_EQ(bed.refID("chr3"), 2);
	EXPECT_EQ(bed.name(0), "chr1");
	EXPECT_EQ(bed.name(1), "chr2");
	EXPECT_EQ(bed.name(2), "chr3");

	// windows properties
	EXPECT_EQ(bed.size(), 3);
	EXPECT_EQ(bed.size(0), 2);
	EXPECT_EQ(bed.size("chr1"), 2);
	EXPECT_EQ(bed.length(), 199);

	EXPECT_FALSE(bed.empty(0));

	EXPECT_EQ(bed.size(1), 0);
	EXPECT_EQ(bed.size("chr2"), 0);
	EXPECT_TRUE(bed.empty(1));

	EXPECT_EQ(bed.size(2), 1);
	EXPECT_EQ(bed.size("chr3"), 1);
	EXPECT_FALSE(bed.empty(2));
}

TEST(TBedTest, merge) {
	TBed bed("chr1 0 7;chr1 5 10;chr1 10 15;chr1 17 20;chr2 20 25;chr2 15 20;chr2 10 15;chr2 3 8");

	EXPECT_EQ(bed.size(), 4);
	EXPECT_EQ(bed[0][0], (TGenomeWindow{0,0,15}));
	EXPECT_EQ(bed[0][1], (TGenomeWindow{0,17,3}));
	EXPECT_EQ(bed[1][0], (TGenomeWindow{1,3,5}));
	EXPECT_EQ(bed[1][1], (TGenomeWindow{1,10,15}));
}

TEST(TBedTest, squareBrackets) {
	TBed bed("chr1 110 120;chr1 0 100;chr3 10 99;");

	EXPECT_EQ(bed[0][0], (TGenomeWindow{0,0,100}));
	EXPECT_EQ(bed[0][1], (TGenomeWindow{0,110,10}));
	EXPECT_EQ(bed[1][0], (TGenomeWindow{1,10,89}));
}

TEST(TBedTest, overlap) {
	TBed bed("chr1 110 120;chr1 0 100;chr3 10 99;");
	EXPECT_TRUE(bed.overlaps(TGenomeWindow{0, 0, 1}));
	EXPECT_TRUE(bed.overlaps(TGenomeWindow{0, 10, 2}));
	EXPECT_FALSE(bed.overlaps(TGenomeWindow{0, 100, 4}));
	EXPECT_FALSE(bed.overlaps(TGenomeWindow{0, 120, 5}));
	EXPECT_TRUE(bed.overlaps(TGenomeWindow{1, 98, 50}));

	EXPECT_TRUE(bed[0][0].overlaps(TGenomeWindow{0, 0, 1}));
	EXPECT_TRUE(bed["chr1"][0].overlaps(TGenomeWindow{0, 10, 2}));
	EXPECT_TRUE(bed[0][0].overlaps(TGenomeWindow{0, 99, 3}));
	EXPECT_FALSE(bed["chr1"][0].overlaps(TGenomeWindow{0, 100, 4}));

	EXPECT_FALSE(bed["chr1"][1].overlaps(TGenomeWindow{0, 109, 1}));
	EXPECT_TRUE(bed[0][1].overlaps(TGenomeWindow{0, 110, 2}));
	EXPECT_TRUE(bed["chr1"][1].overlaps(TGenomeWindow{0, 115, 3}));
	EXPECT_TRUE(bed[0][1].overlaps(TGenomeWindow{0, 119, 4}));
	EXPECT_FALSE(bed["chr1"][1].overlaps(TGenomeWindow{0, 120, 5}));

	EXPECT_FALSE(bed[1][0].overlaps(TGenomeWindow{1, 0, 10}));
	EXPECT_TRUE(bed["chr3"][0].overlaps(TGenomeWindow{1, 0, 11}));
	EXPECT_TRUE(bed[1][0].overlaps(TGenomeWindow{1, 10, 10000000}));
	EXPECT_TRUE(bed["chr3"][0].overlaps(TGenomeWindow{1, 10, 1}));
	EXPECT_TRUE(bed[1][0].overlaps(TGenomeWindow{1, 98, 50}));
	EXPECT_FALSE(bed["chr3"][0].overlaps(TGenomeWindow{1, 99, 50}));
}

TEST(TBedTest, iteration) {
	TBed bed("chr1 0 100;chr1 110 120;chr3 10 99;"); // will get sorted
	EXPECT_EQ(*bed.begin(), (TGenomeWindow{0,0,100}));
	EXPECT_EQ(*bed.begin(0), (TGenomeWindow{0,0,100}));
	EXPECT_EQ(*bed.begin("chr1"), (TGenomeWindow{0,0,100}));
	EXPECT_EQ(*bed.begin(TGenomeWindow{0, 10, 10}), (TGenomeWindow{0,0,100}));

	EXPECT_EQ(*(bed.begin()+1), (TGenomeWindow{0,110,10}));
	EXPECT_EQ(*(bed.begin()+2), (TGenomeWindow{1,10,89}));
	EXPECT_EQ(bed.begin()+3, bed.end());


	EXPECT_EQ(bed.begin(TGenomeWindow{0, 0, 30}), bed.begin());
	EXPECT_EQ(bed.begin(TGenomeWindow{0, 99, 1000000}), bed.begin());

	// in-between
	EXPECT_EQ(bed.begin(TGenomeWindow{0, 101, 1}), bed.end());
	EXPECT_EQ(bed.begin(TGenomeWindow{0, 101, 9}), bed.end());

	EXPECT_EQ(bed.begin(TGenomeWindow{0, 101, 10}), bed.begin(0)+1);
	EXPECT_EQ(bed.begin(TGenomeWindow{0, 101, 1000000}), bed.begin(0)+1);
	EXPECT_EQ(bed.begin(TGenomeWindow{0, 110, 1}), bed.begin(0)+1);
	EXPECT_EQ(bed.begin(TGenomeWindow{0, 119, 1000000}), bed.begin(0)+1);

	//after
	EXPECT_EQ(bed.begin(TGenomeWindow{0, 120, 1}), bed.end());
	EXPECT_EQ(bed.begin(TGenomeWindow{0, 120, 1000000}), bed.end());

	// chr3
	EXPECT_EQ(bed.begin(TGenomeWindow{1, 0, 9}), bed.end());
	EXPECT_EQ(bed.begin(TGenomeWindow{1, 3, 9}), bed.begin()+2);
	EXPECT_EQ(bed.begin(TGenomeWindow{1, 3, 9}), bed.begin(1));
	EXPECT_EQ(bed.begin(TGenomeWindow{1, 98, 1}), bed.begin(1));
	EXPECT_EQ(bed.begin(TGenomeWindow{1, 100, 1000000}), bed.end());
}


TEST(TBedTest, Files) {
	{
	coretools::TOutputFile bed1("bed1.bed",3);
	bed1.writeln("chr1", 0, 100);
	bed1.writeln("chr2", 200, 300);
	bed1.writeln("chr1", 400, 500);

	coretools::TOutputFile bed2("bed2.bed",3);
	bed2.writeln("chr2", 0, 100);
	bed2.writeln("chr3", 200, 300);
	bed2.writeln("chr4", 400, 500);
	}

	TBed bed1("bed1.bed");
	EXPECT_EQ(bed1.NChr(), 2);

	TBed bed2("bed2.bed");
	EXPECT_EQ(bed2.NChr(), 3);

	TBed bed12("bed2.bed,bed1.bed");
	EXPECT_EQ(bed12.NChr(), 4);

	remove("bed1.bed");
	remove("bed2.bed");
}
