
#include "gtest/gtest.h"

#include "genometools/TFastaReader.h"
#include "genometools/TFastaWriter.h"
#include "genometools/TFastaIndex.h"

using namespace genometools;

//--------------------------------------------
// TFastaReader
//--------------------------------------------

void writeFasta(std::string_view name) {
	coretools::TOutputFile fwriter(name);
	fwriter.writeln(">Header0");
	const std::string as(60,'A');
	for (size_t l = 0; l < 10'000; ++l) fwriter.writeln(as);

	fwriter.writeln(">Header1");
	fwriter.writeln("ACGT");
	fwriter.writeln(">Header2");
	fwriter.writeln("N");
	fwriter.writeln(">Header3");
	for (size_t l = 0; l < 13'000; ++l) {
		for (size_t _ = 0; _ < 12; ++_) {
			fwriter.writeNoDelim("ACGTN");
		}
		fwriter.endln();
	}
	fwriter.writeln(">Header4");
	fwriter.writeln("GAGA");
	fwriter.close();
}

TEST(FastaTests, FastaReader) {
	constexpr const char* const fn = "my.fasta";
	writeFasta(fn);
	TFastaReader freader("my.fasta");

	// Start of file
	EXPECT_EQ(freader[TGenomePosition(0, 0)], Base::A);
	EXPECT_EQ(freader(0, 0), Base::A);
	EXPECT_EQ(freader.chrName(0), "Header0");

	// Jump to 2
	EXPECT_EQ(freader[TGenomePosition(2, 0)], Base::N);
	EXPECT_EQ(freader.chrName(2), "Header2");
	// Out of bounds -> N
	EXPECT_EQ(freader[TGenomePosition(2, 1)], Base::N);

	// Back to 1
	EXPECT_EQ(freader(1, 0), Base::A);
	EXPECT_EQ(freader(1, 1), Base::C);
	EXPECT_EQ(freader(1, 2), Base::G);
	EXPECT_EQ(freader(1, 3), Base::T);
	EXPECT_EQ(freader.chrName(1), "Header1");

	// Jump to 3
	EXPECT_EQ(freader[TGenomePosition(3, 5)], Base::A);
	EXPECT_EQ(freader(3, 6), Base::C);
	EXPECT_EQ(freader[TGenomePosition(3, 7)], Base::G);
	EXPECT_EQ(freader(3, 8), Base::T);
	EXPECT_EQ(freader[TGenomePosition(3, 9)], Base::N);
	EXPECT_EQ(freader(3, 10), Base::A);
	EXPECT_EQ(freader.chrName(3), "Header3");

	// Back to 0
	EXPECT_EQ(freader(0, 500'000), Base::A);
	EXPECT_EQ(freader.chrName(0), "Header0");

	// Jump to 4
	EXPECT_EQ(freader.chrName(4), "Header4");
	EXPECT_EQ(freader(4, 0), Base::G);
	EXPECT_EQ(freader(4, 1), Base::A);
	EXPECT_EQ(freader(4, 2), Base::G);
	EXPECT_EQ(freader(4, 3), Base::A);
	// Out of bounds -> N
	EXPECT_EQ(freader(4, 4), Base::N);

	// Back to 0
	EXPECT_EQ(freader(0, 500'000), Base::A);

	remove(fn);
}

TEST(FastaTests, OpenClse) {
	constexpr const char* const fn = "my.fasta";
	writeFasta(fn);

	TFastaReader reader1(fn);
	EXPECT_TRUE(reader1.isOpen());
	reader1.close();
	EXPECT_FALSE(reader1.isOpen());
	EXPECT_ANY_THROW(reader1(0, 0));

	TFastaReader reader2;
	EXPECT_FALSE(reader2.isOpen());
	reader2.open(fn);
	EXPECT_TRUE(reader2.isOpen());
	reader2.close();
	EXPECT_FALSE(reader2.isOpen());

	remove(fn);
	EXPECT_ANY_THROW(reader1.open(fn));
	EXPECT_ANY_THROW(reader2.open(fn));
}

//--------------------------------------------
// TFastaWriter
//--------------------------------------------

TEST(FastaTests, writeFasta) {
	const std::string filename = "test.fasta";

	//write FASTA with TFastaWriter
	int lineLength = 7;
	TFastaWriter writer(filename, lineLength);

	int numContigs = 5;
	std::vector<std::string> contigNames(numContigs);
	for(int c = 0; c < numContigs; ++c){
		contigNames[c] = "Contig" + coretools::str::toString(c);
		writer.newContig(contigNames[c]);
		for(int i = 0; i < c + 1; i++){
			writer.write(genometools::Base::A);
			writer.write(genometools::Base::C);
			writer.write(genometools::Base::G);
			writer.write(genometools::Base::T);
		}
	}
	writer.close();

	//check index file
	using coretools::str::fromString;

	int c = 0;
	int offset = 0;
	for (coretools::TInputFile in(filename + ".fai", coretools::FileType::NoHeader); !in.empty(); in.popFront()) {
		EXPECT_EQ(in.get(0), contigNames[c]);
		offset += 2 + contigNames[c].size();
		EXPECT_EQ(fromString<int>(in.get(2)), offset);

		EXPECT_EQ(fromString<int>(in.get(1)), (c + 1) * 4);
		offset += fromString<int>(in.get(1)) + std::ceil(fromString<int>(in.get(1)) / (double) lineLength);

		EXPECT_EQ(fromString<int>(in.get(3)), lineLength);
		EXPECT_EQ(fromString<int>(in.get(4)), lineLength + 1);
		++c;
	}
	EXPECT_EQ(c, numContigs);

	//read and check with TFastaReader
	//TODO: add test with fasta reader to check offset
	remove(filename.c_str());
}


//--------------------------------------------
// TFastaIndex
//--------------------------------------------

TEST(FastaTests, fastaIndex) {
	std::string filename = "test.fasta";

	//write FASTA with TFastaWriter
	int lineLength = 7;
	TFastaWriter writer(filename, lineLength);

	int numContigs = 5;
	std::vector<std::string> contigNames(numContigs);
	for(int c = 0; c < numContigs; ++c){
		contigNames[c] = "Contig" + coretools::str::toString(c);
		writer.newContig(contigNames[c]);
		for(int i = 0; i < c + 1; i++){
			writer.write(genometools::Base::A);
			writer.write(genometools::Base::C);
			writer.write(genometools::Base::G);
			writer.write(genometools::Base::T);
		}
	}
	writer.close();

	//check index file with FastaIndex
	const std::string index = filename + ".fai";
	TFastaIndex testIndex(index);
	int contigNum = 0;
	int offset = 0;
	for(const auto& fi : testIndex){	
		EXPECT_EQ(fi.contig, contigNames[contigNum]);
		offset += 2 + contigNames[contigNum].size();
		EXPECT_EQ(fi.offset, offset);

		EXPECT_EQ(fi.length, (contigNum + 1) * 4);
		offset += fi.length + std::ceil(fi.length / (double) lineLength);

		EXPECT_EQ(fi.lineBases, lineLength);
		EXPECT_EQ(fi.lineBytes, lineLength + 1);
		++contigNum;
	}
	remove(filename.c_str());
	remove(index.c_str());
}
