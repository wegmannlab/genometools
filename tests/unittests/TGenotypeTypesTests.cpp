//
// Created by madleina on 14.06.21.
//

/*
 * TTypesTests.cpp
 *
 *  Created on: May 10, 2021
 *      Author: phaentu
 */

#include "gtest/gtest.h"

#include "genometools/Genotypes/AllelicCombination.h"
#include "genometools/Genotypes/Base.h"
#include "genometools/Genotypes/BaseContext.h"
#include "genometools/Genotypes/BiallelicGenotype.h"
#include "genometools/Genotypes/BiallelicGenotypesWithAlleles.h"
#include "genometools/Genotypes/Genotype.h"
#include "genometools/Genotypes/TwoBases.h"
#include "genometools/Genotypes/Ploidy.h"

using namespace genometools;

//------------------------------------------------
// Compile time unit tests
//------------------------------------------------

// Base
static_assert(Base::min < Base::max);
static_assert(Base::min == Base::A);
static_assert(Base::A < Base::C);
static_assert(Base::C < Base::G);
static_assert(Base::G < Base::T);
static_assert(Base::T < Base::N);
static_assert(Base::max == Base::N);

static_assert(char2base('a') == Base::A);
static_assert(char2base('A') == Base::A);
static_assert(char2base('c') == Base::C);
static_assert(char2base('C') == Base::C);
static_assert(char2base('g') == Base::G);
static_assert(char2base('G') == Base::G);
static_assert(char2base('t') == Base::T);
static_assert(char2base('T') == Base::T);
static_assert(char2base('X') == Base::N);
static_assert(char2base('Y') == Base::N);
static_assert(char2base('N') == Base::N);

static_assert(base2char(Base::min) == 'A');
static_assert(base2char(Base::A) == 'A');
static_assert(base2char(Base::C) == 'C');
static_assert(base2char(Base::G) == 'G');
static_assert(base2char(Base::T) == 'T');
static_assert(base2char(Base::N) == 'N');
static_assert(base2char(Base::max) == 'N');

static_assert(flipped(Base::A) == Base::T);
static_assert(flipped(Base::G) == Base::C);
static_assert(flipped(Base::C) == Base::G);
static_assert(flipped(Base::T) == Base::A);
static_assert(flipped(Base::N) == Base::N);

static_assert(coretools::next(Base::A) == Base::C);
static_assert(coretools::next(Base::C) == Base::G);
static_assert(coretools::next(Base::G) == Base::T);
static_assert(coretools::next(Base::T) == Base::N);

// Genotype
static_assert(genotype(Base::A, Base::A) == Genotype::AA);
static_assert(genotype(Base::A, Base::C) == Genotype::AC);
static_assert(genotype(Base::A, Base::G) == Genotype::AG);
static_assert(genotype(Base::A, Base::T) == Genotype::AT);
static_assert(genotype(Base::A, Base::N) == Genotype::NN);

static_assert(genotype(Base::C, Base::A) == Genotype::AC);
static_assert(genotype(Base::C, Base::C) == Genotype::CC);
static_assert(genotype(Base::C, Base::G) == Genotype::CG);
static_assert(genotype(Base::C, Base::T) == Genotype::CT);
static_assert(genotype(Base::C, Base::N) == Genotype::NN);

static_assert(genotype(Base::G, Base::A) == Genotype::AG);
static_assert(genotype(Base::G, Base::C) == Genotype::CG);
static_assert(genotype(Base::G, Base::G) == Genotype::GG);
static_assert(genotype(Base::G, Base::T) == Genotype::GT);
static_assert(genotype(Base::G, Base::N) == Genotype::NN);

static_assert(genotype(Base::T, Base::A) == Genotype::AT);
static_assert(genotype(Base::T, Base::C) == Genotype::CT);
static_assert(genotype(Base::T, Base::G) == Genotype::GT);
static_assert(genotype(Base::T, Base::T) == Genotype::TT);
static_assert(genotype(Base::T, Base::N) == Genotype::NN);

static_assert(genotype(Base::N, Base::A) == Genotype::NN);
static_assert(genotype(Base::N, Base::C) == Genotype::NN);
static_assert(genotype(Base::N, Base::G) == Genotype::NN);
static_assert(genotype(Base::N, Base::T) == Genotype::NN);
static_assert(genotype(Base::N, Base::A) == Genotype::NN);

static_assert(first(Genotype::AA) == Base::A);
static_assert(first(Genotype::AC) == Base::A);
static_assert(first(Genotype::AG) == Base::A);
static_assert(first(Genotype::AT) == Base::A);
static_assert(first(Genotype::CC) == Base::C);
static_assert(first(Genotype::CG) == Base::C);
static_assert(first(Genotype::CT) == Base::C);
static_assert(first(Genotype::GG) == Base::G);
static_assert(first(Genotype::GT) == Base::G);
static_assert(first(Genotype::TT) == Base::T);
static_assert(first(Genotype::NN) == Base::N);

static_assert(second(Genotype::AC) == Base::C);
static_assert(second(Genotype::CG) == Base::G);
static_assert(second(Genotype::GT) == Base::T);
static_assert(second(Genotype::NN) == Base::N);

static_assert(isHomozygous(Genotype::AA));
static_assert(isHomozygous(Genotype::CC));
static_assert(isHomozygous(Genotype::GG));
static_assert(isHomozygous(Genotype::TT));
static_assert(!isHomozygous(Genotype::AC));

static_assert(isHeterozygous(Genotype::AC));
static_assert(!isHeterozygous(Genotype::AA));

// TwoBase
static_assert(twoBase(Base::A, Base::A) == TwoBase::AA);
static_assert(twoBase(Base::A, Base::C) == TwoBase::AC);
static_assert(twoBase(Base::A, Base::G) == TwoBase::AG);
static_assert(twoBase(Base::A, Base::T) == TwoBase::AT);
static_assert(twoBase(Base::A, Base::N) == TwoBase::AN);

static_assert(twoBase(Base::C, Base::A) == TwoBase::CA);
static_assert(twoBase(Base::C, Base::C) == TwoBase::CC);
static_assert(twoBase(Base::C, Base::G) == TwoBase::CG);
static_assert(twoBase(Base::C, Base::T) == TwoBase::CT);
static_assert(twoBase(Base::C, Base::N) == TwoBase::CN);

static_assert(twoBase(Base::G, Base::A) == TwoBase::GA);
static_assert(twoBase(Base::G, Base::C) == TwoBase::GC);
static_assert(twoBase(Base::G, Base::G) == TwoBase::GG);
static_assert(twoBase(Base::G, Base::T) == TwoBase::GT);
static_assert(twoBase(Base::G, Base::N) == TwoBase::GN);

static_assert(twoBase(Base::T, Base::A) == TwoBase::TA);
static_assert(twoBase(Base::T, Base::C) == TwoBase::TC);
static_assert(twoBase(Base::T, Base::G) == TwoBase::TG);
static_assert(twoBase(Base::T, Base::T) == TwoBase::TT);
static_assert(twoBase(Base::T, Base::N) == TwoBase::TN);

static_assert(twoBase(Base::N, Base::A) == TwoBase::NA);
static_assert(twoBase(Base::N, Base::C) == TwoBase::NC);
static_assert(twoBase(Base::N, Base::G) == TwoBase::NG);
static_assert(twoBase(Base::N, Base::T) == TwoBase::NT);
static_assert(twoBase(Base::N, Base::N) == TwoBase::NN);

static_assert(first(TwoBase::AA) == Base::A);
static_assert(first(TwoBase::AC) == Base::A);
static_assert(first(TwoBase::AG) == Base::A);
static_assert(first(TwoBase::AT) == Base::A);
static_assert(first(TwoBase::AN) == Base::A);
static_assert(first(TwoBase::CA) == Base::C);
static_assert(first(TwoBase::CC) == Base::C);
static_assert(first(TwoBase::CG) == Base::C);
static_assert(first(TwoBase::CT) == Base::C);
static_assert(first(TwoBase::CN) == Base::C);
static_assert(first(TwoBase::GA) == Base::G);
static_assert(first(TwoBase::GC) == Base::G);
static_assert(first(TwoBase::GG) == Base::G);
static_assert(first(TwoBase::GT) == Base::G);
static_assert(first(TwoBase::GN) == Base::G);
static_assert(first(TwoBase::TA) == Base::T);
static_assert(first(TwoBase::TC) == Base::T);
static_assert(first(TwoBase::TG) == Base::T);
static_assert(first(TwoBase::TT) == Base::T);
static_assert(first(TwoBase::TN) == Base::T);
static_assert(first(TwoBase::NA) == Base::N);
static_assert(first(TwoBase::NC) == Base::N);
static_assert(first(TwoBase::NG) == Base::N);
static_assert(first(TwoBase::NT) == Base::N);
static_assert(first(TwoBase::NN) == Base::N);

static_assert(second(TwoBase::AA) == Base::A);
static_assert(second(TwoBase::AC) == Base::C);
static_assert(second(TwoBase::AG) == Base::G);
static_assert(second(TwoBase::AT) == Base::T);
static_assert(second(TwoBase::AN) == Base::N);
static_assert(second(TwoBase::CA) == Base::A);
static_assert(second(TwoBase::CC) == Base::C);
static_assert(second(TwoBase::CG) == Base::G);
static_assert(second(TwoBase::CT) == Base::T);
static_assert(second(TwoBase::CN) == Base::N);
static_assert(second(TwoBase::GA) == Base::A);
static_assert(second(TwoBase::GC) == Base::C);
static_assert(second(TwoBase::GG) == Base::G);
static_assert(second(TwoBase::GT) == Base::T);
static_assert(second(TwoBase::GN) == Base::N);
static_assert(second(TwoBase::TA) == Base::A);
static_assert(second(TwoBase::TC) == Base::C);
static_assert(second(TwoBase::TG) == Base::G);
static_assert(second(TwoBase::TT) == Base::T);
static_assert(second(TwoBase::TN) == Base::N);
static_assert(second(TwoBase::NA) == Base::A);
static_assert(second(TwoBase::NC) == Base::C);
static_assert(second(TwoBase::NG) == Base::G);
static_assert(second(TwoBase::NT) == Base::T);
static_assert(second(TwoBase::NN) == Base::N);

// BaseContext
static_assert(baseContext(Base::A, Base::A) == BaseContext::AA);
static_assert(baseContext(Base::A, Base::C) == BaseContext::AC);
static_assert(baseContext(Base::A, Base::G) == BaseContext::AG);
static_assert(baseContext(Base::A, Base::T) == BaseContext::AT);

static_assert(baseContext(Base::C, Base::A) == BaseContext::CA);
static_assert(baseContext(Base::C, Base::C) == BaseContext::CC);
static_assert(baseContext(Base::C, Base::G) == BaseContext::CG);
static_assert(baseContext(Base::C, Base::T) == BaseContext::CT);

static_assert(baseContext(Base::G, Base::A) == BaseContext::GA);
static_assert(baseContext(Base::G, Base::C) == BaseContext::GC);
static_assert(baseContext(Base::G, Base::G) == BaseContext::GG);
static_assert(baseContext(Base::G, Base::T) == BaseContext::GT);

static_assert(baseContext(Base::T, Base::A) == BaseContext::TA);
static_assert(baseContext(Base::T, Base::C) == BaseContext::TC);
static_assert(baseContext(Base::T, Base::G) == BaseContext::TG);
static_assert(baseContext(Base::T, Base::T) == BaseContext::TT);

static_assert(baseContext(Base::N, Base::A) == BaseContext::NA);
static_assert(baseContext(Base::N, Base::G) == BaseContext::NG);
static_assert(baseContext(Base::N, Base::T) == BaseContext::NT);
static_assert(baseContext(Base::N, Base::A) == BaseContext::NA);

static_assert(baseContext(Base::A, Base::N) == BaseContext::NN);
static_assert(baseContext(Base::G, Base::N) == BaseContext::NN);
static_assert(baseContext(Base::T, Base::N) == BaseContext::NN);
static_assert(baseContext(Base::A, Base::N) == BaseContext::NN);

// BiallelicGenotype

static_assert(isHomozygous(BiallelicGenotype::homoFirst));
static_assert(!isHeterozygous(BiallelicGenotype::homoFirst));
static_assert(!isHaploid(BiallelicGenotype::homoFirst));
static_assert(isDiploid(BiallelicGenotype::homoFirst));
static_assert(!isMissing(BiallelicGenotype::homoFirst));
static_assert(altAlleleCounts(BiallelicGenotype::homoFirst) == 0);

static_assert(!isHomozygous(BiallelicGenotype::het));
static_assert(isHeterozygous(BiallelicGenotype::het));
static_assert(!isHaploid(BiallelicGenotype::het));
static_assert(isDiploid(BiallelicGenotype::het));
static_assert(!isMissing(BiallelicGenotype::het));
static_assert(altAlleleCounts(BiallelicGenotype::het) == 1);

static_assert(isHomozygous(BiallelicGenotype::homoSecond));
static_assert(!isHeterozygous(BiallelicGenotype::homoSecond));
static_assert(!isHaploid(BiallelicGenotype::homoSecond));
static_assert(isDiploid(BiallelicGenotype::homoSecond));
static_assert(!isMissing(BiallelicGenotype::homoSecond));
static_assert(altAlleleCounts(BiallelicGenotype::homoSecond) == 2);

static_assert(!isHomozygous(BiallelicGenotype::missingDiploid));
static_assert(!isHeterozygous(BiallelicGenotype::missingDiploid));
static_assert(!isHaploid(BiallelicGenotype::missingDiploid));
static_assert(isDiploid(BiallelicGenotype::missingDiploid));
static_assert(isMissing(BiallelicGenotype::missingDiploid));
static_assert(altAlleleCounts(BiallelicGenotype::missingDiploid) == 0);

static_assert(!isHomozygous(BiallelicGenotype::haploidFirst));
static_assert(!isHeterozygous(BiallelicGenotype::haploidFirst));
static_assert(isHaploid(BiallelicGenotype::haploidFirst));
static_assert(!isDiploid(BiallelicGenotype::haploidFirst));
static_assert(!isMissing(BiallelicGenotype::haploidFirst));
static_assert(altAlleleCounts(BiallelicGenotype::haploidFirst) == 0);

static_assert(!isHomozygous(BiallelicGenotype::haploidSecond));
static_assert(!isHeterozygous(BiallelicGenotype::haploidSecond));
static_assert(isHaploid(BiallelicGenotype::haploidSecond));
static_assert(!isDiploid(BiallelicGenotype::haploidSecond));
static_assert(!isMissing(BiallelicGenotype::haploidSecond));
static_assert(altAlleleCounts(BiallelicGenotype::haploidSecond) == 1);

static_assert(!isHomozygous(BiallelicGenotype::missingHaploid));
static_assert(!isHeterozygous(BiallelicGenotype::missingHaploid));
static_assert(isHaploid(BiallelicGenotype::missingHaploid));
static_assert(!isDiploid(BiallelicGenotype::missingHaploid));
static_assert(isMissing(BiallelicGenotype::missingHaploid));
static_assert(altAlleleCounts(BiallelicGenotype::missingHaploid) == 0);

static_assert(coretools::index(BiallelicGenotype::minDiploid) == 0);
static_assert(coretools::index(BiallelicGenotype::maxDiploid) == 3);
static_assert(coretools::index(BiallelicGenotype::minHaploid) == 4);
static_assert(coretools::index(BiallelicGenotype::maxHaploid) == 6);

//Diploid/Haploid

static_assert(diploid(BiallelicGenotype::homoFirst) == Diploid::homoFirst);
static_assert(diploid(BiallelicGenotype::het) == Diploid::het);
static_assert(diploid(BiallelicGenotype::homoSecond) == Diploid::homoSecond);
static_assert(diploid(BiallelicGenotype::missingDiploid) == Diploid::max);
static_assert(diploid(BiallelicGenotype::maxDiploid) == Diploid::max);
static_assert(diploid(BiallelicGenotype::haploidFirst) == Diploid::max);
static_assert(diploid(BiallelicGenotype::haploidSecond) == Diploid::max);
static_assert(diploid(BiallelicGenotype::missingHaploid) == Diploid::max);

static_assert(haploid(BiallelicGenotype::homoFirst) == Haploid::max);
static_assert(haploid(BiallelicGenotype::het) == Haploid::max);
static_assert(haploid(BiallelicGenotype::homoSecond) == Haploid::max);
static_assert(haploid(BiallelicGenotype::missingDiploid) == Haploid::max);
static_assert(haploid(BiallelicGenotype::haploidFirst) == Haploid::first);
static_assert(haploid(BiallelicGenotype::haploidSecond) == Haploid::second);
static_assert(haploid(BiallelicGenotype::missingHaploid) == Haploid::max);

// AllelicCombination
static_assert(allelicCombination(Base::A, Base::C) == AllelicCombination::AC);
static_assert(allelicCombination(Base::A, Base::G) == AllelicCombination::AG);
static_assert(allelicCombination(Base::A, Base::T) == AllelicCombination::AT);

static_assert(allelicCombination(Base::C, Base::A) == AllelicCombination::AC);
static_assert(allelicCombination(Base::C, Base::G) == AllelicCombination::CG);
static_assert(allelicCombination(Base::C, Base::T) == AllelicCombination::CT);

static_assert(allelicCombination(Base::G, Base::A) == AllelicCombination::AG);
static_assert(allelicCombination(Base::G, Base::C) == AllelicCombination::CG);
static_assert(allelicCombination(Base::G, Base::T) == AllelicCombination::GT);

static_assert(allelicCombination(Base::T, Base::A) == AllelicCombination::AT);
static_assert(allelicCombination(Base::T, Base::C) == AllelicCombination::CT);
static_assert(allelicCombination(Base::T, Base::G) == AllelicCombination::GT);

static_assert(allelicCombination(Base::N, Base::A) == AllelicCombination::NN);
static_assert(allelicCombination(Base::N, Base::G) == AllelicCombination::NN);
static_assert(allelicCombination(Base::N, Base::T) == AllelicCombination::NN);
static_assert(allelicCombination(Base::N, Base::A) == AllelicCombination::NN);

static_assert(allelicCombination(Base::A, Base::N) == AllelicCombination::NN);
static_assert(allelicCombination(Base::G, Base::N) == AllelicCombination::NN);
static_assert(allelicCombination(Base::T, Base::N) == AllelicCombination::NN);
static_assert(allelicCombination(Base::A, Base::N) == AllelicCombination::NN);

static_assert(first(AllelicCombination::AC) == Base::A);
static_assert(first(AllelicCombination::AG) == Base::A);
static_assert(first(AllelicCombination::AT) == Base::A);
static_assert(first(AllelicCombination::CG) == Base::C);
static_assert(first(AllelicCombination::CT) == Base::C);
static_assert(first(AllelicCombination::GT) == Base::G);

static_assert(second(AllelicCombination::AC) == Base::C);
static_assert(second(AllelicCombination::AG) == Base::G);
static_assert(second(AllelicCombination::AT) == Base::T);
static_assert(second(AllelicCombination::CG) == Base::G);
static_assert(second(AllelicCombination::CT) == Base::T);
static_assert(second(AllelicCombination::GT) == Base::T);

static_assert(homoFirst(AllelicCombination::AC) == Genotype::AA);
static_assert(het(AllelicCombination::AC) == Genotype::AC);
static_assert(homoSecond(AllelicCombination::AC) == Genotype::CC);

static_assert(homoFirst(AllelicCombination::CG) == Genotype::CC);
static_assert(het(AllelicCombination::CG) == Genotype::CG);
static_assert(homoSecond(AllelicCombination::CG) == Genotype::GG);

TEST(TypesTests, Base) {
	Base b{};
	EXPECT_EQ(b, Base::min);
	EXPECT_EQ(b, Base::A);
	++b;
	EXPECT_EQ(b, Base::C);
	++b;
	EXPECT_EQ(b, Base::G);
	++b;
	EXPECT_EQ(b, Base::T);
	++b;
	EXPECT_EQ(b, Base::N);
	flip(b);
	EXPECT_EQ(b, Base::N);
	b = Base::A;
	flip(b);
	EXPECT_EQ(b, Base::T);
}

TEST(TypesTests, Genotype) {
	EXPECT_EQ(toString(genotype(Base::A, Base::A)), std::string("AA"));
	EXPECT_EQ(toString(genotype(Base::A, Base::C)), std::string("AC"));
	EXPECT_EQ(toString(genotype(Base::A, Base::G)), std::string("AG"));
	EXPECT_EQ(toString(genotype(Base::A, Base::T)), std::string("AT"));
	EXPECT_EQ(toString(genotype(Base::C, Base::A)), std::string("AC"));

	EXPECT_EQ(toString(genotype(Base::C, Base::A)), std::string("AC"));
	EXPECT_EQ(toString(genotype(Base::C, Base::C)), std::string("CC"));
	EXPECT_EQ(toString(genotype(Base::C, Base::G)), std::string("CG"));
	EXPECT_EQ(toString(genotype(Base::C, Base::T)), std::string("CT"));

	EXPECT_EQ(toString(genotype(Base::G, Base::A)), std::string("AG"));
	EXPECT_EQ(toString(genotype(Base::G, Base::C)), std::string("CG"));
	EXPECT_EQ(toString(genotype(Base::G, Base::G)), std::string("GG"));
	EXPECT_EQ(toString(genotype(Base::G, Base::T)), std::string("GT"));

	EXPECT_EQ(toString(genotype(Base::T, Base::A)), std::string("AT"));
	EXPECT_EQ(toString(genotype(Base::T, Base::C)), std::string("CT"));
	EXPECT_EQ(toString(genotype(Base::T, Base::G)), std::string("GT"));
	EXPECT_EQ(toString(genotype(Base::T, Base::T)), std::string("TT"));

	EXPECT_EQ(toString(genotype(Base::N, Base::N)), std::string("NN"));
	EXPECT_EQ(toString(genotype(Base::A, Base::N)), std::string("NN"));
	EXPECT_EQ(toString(genotype(Base::N, Base::C)), std::string("NN"));
}

TEST(TypesTests, TwoBase) {
	EXPECT_EQ(toString(twoBase(Base::A, Base::A)), std::string("AA"));
	EXPECT_EQ(toString(twoBase(Base::A, Base::C)), std::string("AC"));
	EXPECT_EQ(toString(twoBase(Base::A, Base::G)), std::string("AG"));
	EXPECT_EQ(toString(twoBase(Base::A, Base::T)), std::string("AT"));
	EXPECT_EQ(toString(twoBase(Base::A, Base::N)), std::string("AN"));

	EXPECT_EQ(toString(twoBase(Base::C, Base::A)), std::string("CA"));
	EXPECT_EQ(toString(twoBase(Base::C, Base::C)), std::string("CC"));
	EXPECT_EQ(toString(twoBase(Base::C, Base::G)), std::string("CG"));
	EXPECT_EQ(toString(twoBase(Base::C, Base::T)), std::string("CT"));
	EXPECT_EQ(toString(twoBase(Base::C, Base::N)), std::string("CN"));

	EXPECT_EQ(toString(twoBase(Base::G, Base::A)), std::string("GA"));
	EXPECT_EQ(toString(twoBase(Base::G, Base::C)), std::string("GC"));
	EXPECT_EQ(toString(twoBase(Base::G, Base::G)), std::string("GG"));
	EXPECT_EQ(toString(twoBase(Base::G, Base::T)), std::string("GT"));
	EXPECT_EQ(toString(twoBase(Base::G, Base::N)), std::string("GN"));

	EXPECT_EQ(toString(twoBase(Base::T, Base::A)), std::string("TA"));
	EXPECT_EQ(toString(twoBase(Base::T, Base::C)), std::string("TC"));
	EXPECT_EQ(toString(twoBase(Base::T, Base::G)), std::string("TG"));
	EXPECT_EQ(toString(twoBase(Base::T, Base::T)), std::string("TT"));
	EXPECT_EQ(toString(twoBase(Base::T, Base::N)), std::string("TN"));

	EXPECT_EQ(toString(twoBase(Base::N, Base::A)), std::string("NA"));
	EXPECT_EQ(toString(twoBase(Base::N, Base::C)), std::string("NC"));
	EXPECT_EQ(toString(twoBase(Base::N, Base::G)), std::string("NG"));
	EXPECT_EQ(toString(twoBase(Base::N, Base::T)), std::string("NT"));
	EXPECT_EQ(toString(twoBase(Base::N, Base::N)), std::string("NN"));
}

TEST(TypesTests, BaseContext) {
	// cast to string
	EXPECT_EQ(toString(baseContext(Base::A, Base::A)), std::string("AA"));
	EXPECT_EQ(toString(baseContext(Base::A, Base::C)), std::string("AC"));
	EXPECT_EQ(toString(baseContext(Base::A, Base::G)), std::string("AG"));
	EXPECT_EQ(toString(baseContext(Base::A, Base::T)), std::string("AT"));

	EXPECT_EQ(toString(baseContext(Base::C, Base::A)), std::string("CA"));
	EXPECT_EQ(toString(baseContext(Base::C, Base::C)), std::string("CC"));
	EXPECT_EQ(toString(baseContext(Base::C, Base::G)), std::string("CG"));
	EXPECT_EQ(toString(baseContext(Base::C, Base::T)), std::string("CT"));

	EXPECT_EQ(toString(baseContext(Base::G, Base::A)), std::string("GA"));
	EXPECT_EQ(toString(baseContext(Base::G, Base::C)), std::string("GC"));
	EXPECT_EQ(toString(baseContext(Base::G, Base::G)), std::string("GG"));
	EXPECT_EQ(toString(baseContext(Base::G, Base::T)), std::string("GT"));

	EXPECT_EQ(toString(baseContext(Base::T, Base::A)), std::string("TA"));
	EXPECT_EQ(toString(baseContext(Base::T, Base::C)), std::string("TC"));
	EXPECT_EQ(toString(baseContext(Base::T, Base::G)), std::string("TG"));
	EXPECT_EQ(toString(baseContext(Base::T, Base::T)), std::string("TT"));

	EXPECT_EQ(toString(baseContext(Base::A, Base::N)), std::string("NN"));
	EXPECT_EQ(toString(baseContext(Base::C, Base::N)), std::string("NN"));
	EXPECT_EQ(toString(baseContext(Base::G, Base::N)), std::string("NN"));
	EXPECT_EQ(toString(baseContext(Base::T, Base::N)), std::string("NN"));
	EXPECT_EQ(toString(baseContext(Base::N, Base::N)), std::string("NN"));
}

//-------------------------------------
// BiallelicGenotype
//-------------------------------------

TEST(TypesTests, BiallelicGenotype) {
	// ++ operator
	BiallelicGenotype bg{};
	EXPECT_EQ(bg, BiallelicGenotype::homoFirst);
	EXPECT_EQ(++bg, BiallelicGenotype::het);
	EXPECT_EQ(++bg, BiallelicGenotype::homoSecond);
	EXPECT_EQ(++bg, BiallelicGenotype::missingDiploid);
	EXPECT_EQ(++bg, BiallelicGenotype::haploidFirst);
	EXPECT_EQ(++bg, BiallelicGenotype::haploidSecond);
	EXPECT_EQ(++bg, BiallelicGenotype::missingHaploid);
}

//-------------------------------------
// BiallelicGenotypes
//-------------------------------------

TEST(TypesTests, BiallelicGenotypesWithAlleles) {
	// same base -> error
	EXPECT_ANY_THROW(BiallelicGenotypesWithAlleles g((Base(Base::A)), Base(Base::A)));

	// constructor, homoFirst(), het(), homoSecond()
	BiallelicGenotypesWithAlleles g((Base(Base::A)), Base(Base::C));
	EXPECT_EQ(g.homoFirst(), genotype(Base::A, Base::A));
	EXPECT_EQ(g.het(), genotype(Base::A, Base::C));
	EXPECT_EQ(g.homoSecond(), genotype(Base::C, Base::C));

	// operator []
	EXPECT_EQ(g[BiallelicGenotype::homoFirst], genotype(Base::A, Base::A));
	EXPECT_EQ(g[BiallelicGenotype::het], genotype(Base::A, Base::C));
	EXPECT_EQ(g[BiallelicGenotype::homoSecond], genotype(Base::C, Base::C));
	EXPECT_ANY_THROW(Genotype value = g[BiallelicGenotype::missingDiploid]; (void) value);
	EXPECT_ANY_THROW(Genotype value = g[BiallelicGenotype::haploidFirst]; (void) value);
	EXPECT_ANY_THROW(Genotype value = g[BiallelicGenotype::haploidSecond]; (void) value);
	EXPECT_ANY_THROW(Genotype value = g[BiallelicGenotype::missingHaploid]; (void) value);
}

//------------------------------------------------
// AllelicCombination
//------------------------------------------------

TEST(TypesTests, AllelicCombination) {
	EXPECT_ANY_THROW(allelicCombination(Base::A, Base::A));
	EXPECT_ANY_THROW(allelicCombination(Base::C, Base::C));
	EXPECT_ANY_THROW(allelicCombination(Base::G, Base::G));
	EXPECT_ANY_THROW(allelicCombination(Base::T, Base::T));
}
