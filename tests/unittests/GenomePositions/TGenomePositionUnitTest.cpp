//
// Created by madleina on 02.08.20.
//

#include "gtest/gtest.h"

#include "genometools/GenomePositions/TGenomePosition.h"
#include "genometools/GenomePositions/TGenomeWindow.h"

//-----------------------------------------------------
// TGenomePosition
//-----------------------------------------------------

TEST(TGenomePositionTest, constructor_noParams){
    genometools::TGenomePosition pos;
    EXPECT_EQ(pos.refID(), 0);
    EXPECT_EQ(pos.position(), 0);
}

TEST(TGenomePositionTest, constructor_refID_pos){
    genometools::TGenomePosition pos(10, 100);
    EXPECT_EQ(pos.refID(), 10);
    EXPECT_EQ(pos.position(), 100);
}

TEST(TGenomePositionTest, constructor_otherObject){
    genometools::TGenomePosition pos1(10, 100);
    genometools::TGenomePosition pos2(pos1);
    EXPECT_EQ(pos2.refID(), 10);
    EXPECT_EQ(pos2.position(), 100);
}

TEST(TGenomePositionTest, move_refID_pos){
    genometools::TGenomePosition pos(10, 100);
    pos.move(1, 50);
    EXPECT_EQ(pos.refID(), 1);
    EXPECT_EQ(pos.position(), 50);
}

TEST(TGenomePositionTest, move_otherObject){
    genometools::TGenomePosition pos1(10, 100);
    genometools::TGenomePosition pos2(1, 50);
    pos1.move(pos2);
    EXPECT_EQ(pos1.refID(), 1);
    EXPECT_EQ(pos1.position(), 50);
    EXPECT_EQ(pos2.refID(), 1);
    EXPECT_EQ(pos2.position(), 50);
}

TEST(TGenomePositionTest, move_pos){
    genometools::TGenomePosition pos(10, 100);
    pos.moveOnRef(50);
    EXPECT_EQ(pos.refID(), 10);
    EXPECT_EQ(pos.position(), 50);
}

TEST(TGenomePositionTest, operator_Eq){
    genometools::TGenomePosition pos2(1, 50);
    genometools::TGenomePosition pos1(2, 100);
    pos1 = pos2;
    EXPECT_EQ(pos1.refID(), 1);
    EXPECT_EQ(pos1.position(), 50);
    EXPECT_EQ(pos2.refID(), 1);
    EXPECT_EQ(pos2.position(), 50);
}

TEST(TGenomePositionTest, operator_Plus){
    genometools::TGenomePosition pos1(10, 100);
    genometools::TGenomePosition pos2 = pos1 + 10;
    EXPECT_EQ(pos2.refID(), 10);
    EXPECT_EQ(pos2.position(), 110);
}

TEST(TGenomePositionTest, operator_Minus_length){
    genometools::TGenomePosition pos1(10, 100);
    // this is ok
    genometools::TGenomePosition pos2 = pos1 - 10;
    EXPECT_EQ(pos2.refID(), 10);
    EXPECT_EQ(pos2.position(), 90);
    // this is negative, so set it to zero
    pos2 = pos1 - 200;
    EXPECT_EQ(pos2.refID(), 10);
    EXPECT_EQ(pos2.position(), 0);
}

TEST(TGenomePositionTest, operator_Minus_otherObject){
    genometools::TGenomePosition pos1(10, 100);

    // same chromosome -> ok
    genometools::TGenomePosition pos2(10, 50);
    uint32_t position = pos1 - pos2;
    EXPECT_EQ(position, 50);

    // same chromosome, but pos2 is larger than pos1 -> set to zero
    pos2.move(10, 200);
    position = pos1 - pos2;
    EXPECT_EQ(position, 0);

    // other chromosomes -> throw
    pos2.move(1, 100);
    EXPECT_THROW(pos1 - pos2, coretools::err::TDevError);
}

TEST(TGenomePositionTest, operator_PlusEq){
    genometools::TGenomePosition pos(10, 100);
    pos += 10;
    EXPECT_EQ(pos.refID(), 10);
    EXPECT_EQ(pos.position(), 110);
}

TEST(TGenomePositionTest, operator_MinusEq){
    genometools::TGenomePosition pos(10, 100);
    // this is ok
    pos -= 10;
    EXPECT_EQ(pos.refID(), 10);
    EXPECT_EQ(pos.position(), 90);

    // this is negative, so set it to zero
    pos -= 100;
    EXPECT_EQ(pos.refID(), 10);
    EXPECT_EQ(pos.position(), 0);
}

TEST(TGenomePositionTest, operator_PlusPlus){
    genometools::TGenomePosition pos(10, 100);
    ++pos;
    EXPECT_EQ(pos.refID(), 10);
    EXPECT_EQ(pos.position(), 101);
}

TEST(TGenomePositionTest, operator_MinusMinus){
    genometools::TGenomePosition pos(10, 100);
    // this is ok
    --pos;
    EXPECT_EQ(pos.refID(), 10);
    EXPECT_EQ(pos.position(), 99);

    // this is negative, so set it to zero
    pos.move(10, 0);
    --pos;
    EXPECT_EQ(pos.refID(), 10);
    EXPECT_EQ(pos.position(), 0);
}

TEST(TGenomePositionTest, sameChr){
    genometools::TGenomePosition pos1(10, 100);
    genometools::TGenomePosition pos2(10, 99);
    // same chr
    EXPECT_TRUE(pos1.sameChr(pos2));

    // other chr
    pos2.move(11, 99);
    EXPECT_FALSE(pos1.sameChr(pos2));
}

TEST(TGenomePositionTest, operator_equalEqual){
    genometools::TGenomePosition pos1(10, 100);
    genometools::TGenomePosition pos2(10, 100);
    // same pos and chr
    EXPECT_TRUE(pos1 == pos2);

    // same pos, other chr
    pos2.move(11, 100);
    EXPECT_FALSE(pos1 == pos2);

    // other pos, same chr
    pos2.move(10, 99);
    EXPECT_FALSE(pos1 == pos2);

    // other pos, other chr
    pos2.move(11, 99);
    EXPECT_FALSE(pos1 == pos2);
}

TEST(TGenomePositionTest, operator_smaller){
    genometools::TGenomePosition pos1(10, 100);
    genometools::TGenomePosition pos2(10, 200);
    // same chr, smaller pos
    EXPECT_TRUE(pos1 < pos2);

    // same chr, larger pos
    pos2.move(10, 1);
    EXPECT_FALSE(pos1 < pos2);

    // same chr, equal pos
    pos2.move(10, 100);
    EXPECT_FALSE(pos1 < pos2);

    // smaller chr
    pos2.move(11, 100);
    EXPECT_TRUE(pos1 < pos2);

    // larger chr
    pos2.move(9, 100);
    EXPECT_FALSE(pos1 < pos2);
}


TEST(TGenomePositionTest, operator_larger){
    genometools::TGenomePosition pos1(10, 200);
    genometools::TGenomePosition pos2(10, 100);
    // same chr, larger pos
    EXPECT_TRUE(pos1 > pos2);

    // same chr, smaller pos
    pos2.move(300, 1);
    EXPECT_FALSE(pos1 > pos2);

    // same chr, equal pos
    pos2.move(10, 200);
    EXPECT_FALSE(pos1 > pos2);

    // larger chr
    pos2.move(9, 100);
    EXPECT_TRUE(pos1 > pos2);

    // smaller chr
    pos2.move(11, 100);
    EXPECT_FALSE(pos1 > pos2);
}

TEST(TGenomePositionTest, operator_smallerEqual){
    genometools::TGenomePosition pos1(10, 100);
    genometools::TGenomePosition pos2(10, 200);
    // same chr, smaller pos
    EXPECT_TRUE(pos1 <= pos2);

    // same chr, larger pos
    pos2.move(10, 1);
    EXPECT_FALSE(pos1 <= pos2);

    // same chr, equal pos
    pos2.move(10, 100);
    EXPECT_TRUE(pos1 <= pos2);

    // smaller chr
    pos2.move(11, 100);
    EXPECT_TRUE(pos1 <= pos2);

    // larger chr
    pos2.move(9, 100);
    EXPECT_FALSE(pos1 <= pos2);
}

TEST(TGenomePositionTest, operator_largerEqual){
    genometools::TGenomePosition pos1(10, 200);
    genometools::TGenomePosition pos2(10, 100);
    // same chr, larger pos
    EXPECT_TRUE(pos1 >= pos2);

    // same chr, smaller pos
    pos2.move(300, 1);
    EXPECT_FALSE(pos1 >= pos2);

    // same chr, equal pos
    pos2.move(10, 200);
    EXPECT_TRUE(pos1 >= pos2);

    // larger chr
    pos2.move(9, 100);
    EXPECT_TRUE(pos1 >= pos2);

    // smaller chr
    pos2.move(11, 100);
    EXPECT_FALSE(pos1 >= pos2);
}

TEST(TGenomePositionTest, operator_smallerWindow){
    genometools::TGenomeWindow window(1, 10, 10);

    // pos is larger than _to
    genometools::TGenomePosition pos(1, 25);
    EXPECT_TRUE(pos > window);

    // pos is equal to _to
    pos.move(1, 20);
    EXPECT_TRUE(pos > window);

    // pos is smaller than _to
    pos.move(1, 15);
    EXPECT_FALSE(pos > window);

    // pos is smaller than _from
    pos.move(1, 5);
    EXPECT_FALSE(pos > window);

    // pos is a smaller chromosome
    pos.move(0, 25);
    EXPECT_FALSE(pos > window);

    // pos is a larger chromosome
    pos.move(2, 25);
    EXPECT_TRUE(pos > window);
}

TEST(TGenomePositionTest, operator_largerWindow){
    genometools::TGenomeWindow window(1, 10, 20);

    // pos is smaller than _from
    genometools::TGenomePosition pos(1, 5);
    EXPECT_TRUE(pos < window);

    // pos is equal to _from
    pos.move(1, 10);
    EXPECT_FALSE(pos < window);

    // pos is larger than _from
    pos.move(1, 15);
    EXPECT_FALSE(pos < window);

    // pos is larger than _to
    pos.move(1, 25);
    EXPECT_FALSE(pos < window);

    // pos is a smaller chromosome
    pos.move(0, 25);
    EXPECT_TRUE(pos < window);

    // pos is a larger chromosome
    pos.move(2, 25);
    EXPECT_FALSE(pos < window);
}

TEST(TGenomePositionTest, operator_string){    
    genometools::TGenomePosition pos(10, 100);
    
    EXPECT_EQ((std::string) pos, "10:100");
}
