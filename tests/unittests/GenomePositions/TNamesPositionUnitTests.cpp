//
// Created by caduffm on 3/8/22.
//

#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "genometools/GenomePositions/TNamesPositions.h"

using namespace genometools;
using namespace coretools;
using namespace testing;

TEST(TNamesUnitTests, TNamesPositions) {
	// create positions object and fill it
	TPositionsRaw positions;
	positions.add(10, "chunk_1");
	positions.add(15, "chunk_1");
	positions.add(100, "chunk_1");
	positions.add(5, "chunk_2");
	positions.add(20, "chunk_2");
	positions.add(80, "chunk_10");
	positions.finalizeFilling();

	// base class ptr
	std::unique_ptr<TNamesEmpty> base = std::make_unique<TNamesPositions>(&positions);

	EXPECT_EQ(base->size(), 6);
	EXPECT_TRUE(base->isFilled());

	// []
	EXPECT_EQ((*base)[0], "chunk_1:10");
	EXPECT_EQ((*base)[1], "chunk_1:15");
	EXPECT_EQ((*base)[2], "chunk_1:100");
	EXPECT_EQ((*base)[3], "chunk_2:5");
	EXPECT_EQ((*base)[4], "chunk_2:20");
	EXPECT_EQ((*base)[5], "chunk_10:80");

	// get name
	EXPECT_THAT(base->getName(0), ElementsAre("chunk_1", "10"));
	EXPECT_THAT(base->getName(1), ElementsAre("chunk_1", "15"));
	EXPECT_THAT(base->getName(2), ElementsAre("chunk_1", "100"));
	EXPECT_THAT(base->getName(3), ElementsAre("chunk_2", "5"));
	EXPECT_THAT(base->getName(4), ElementsAre("chunk_2", "20"));
	EXPECT_THAT(base->getName(5), ElementsAre("chunk_10", "80"));

	// exists
	EXPECT_TRUE(base->exists("chunk_1:10"));
	EXPECT_EQ(base->getIndex("chunk_1:10"), 0);
	EXPECT_TRUE(base->exists("chunk_1:15"));
	EXPECT_EQ(base->getIndex("chunk_1:15"), 1);
	EXPECT_TRUE(base->exists("chunk_1:100"));
	EXPECT_EQ(base->getIndex("chunk_1:100"), 2);
	EXPECT_TRUE(base->exists("chunk_2:5"));
	EXPECT_EQ(base->getIndex("chunk_2:5"), 3);
	EXPECT_TRUE(base->exists("chunk_2:20"));
	EXPECT_EQ(base->getIndex("chunk_2:20"), 4);
	EXPECT_TRUE(base->exists("chunk_10:80"));
	EXPECT_EQ(base->getIndex("chunk_10:80"), 5);
	EXPECT_FALSE(base->exists("chunk_0:9"));
	EXPECT_ANY_THROW(base->getIndex("chunk_0:9"));
	EXPECT_FALSE(base->exists("chunk_3:1"));
	EXPECT_ANY_THROW(base->getIndex("chunk_3:1"));
	EXPECT_FALSE(base->exists("chunk_1:5"));
	EXPECT_ANY_THROW(base->getIndex("chunk_1:5"));
	EXPECT_FALSE(base->exists("chunk_2:80"));
	EXPECT_ANY_THROW(base->getIndex("chunk_2:80"));

	// title
	EXPECT_EQ(base->getTitle(), "-:-");

	// operator ==
	TPositionsRaw positions2;
	positions2.add(11, "chunk_1");
	std::unique_ptr<TNamesEmpty> other = std::make_unique<TNamesPositions>(&positions2);
	EXPECT_FALSE((*base) == (*other));
	EXPECT_TRUE((*base) != (*other));

	TPositionsRaw positions3;
	positions3.add(10, "chunk_1");
	positions3.add(15, "chunk_1");
	positions3.add(80, "chunk_10");
	positions3.finalizeFilling();
	std::unique_ptr<TNamesEmpty> other2 = std::make_unique<TNamesPositions>(&positions3);
	EXPECT_FALSE((*base) == (*other2));
	EXPECT_TRUE((*base) != (*other2));

	std::unique_ptr<TNamesEmpty> other4 = std::make_unique<TNamesPositions>(&positions);
	EXPECT_TRUE((*base) == (*other4));
	EXPECT_FALSE((*base) != (*other4));
}
