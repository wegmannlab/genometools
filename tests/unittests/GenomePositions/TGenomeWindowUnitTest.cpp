#include "gtest/gtest.h"

#include "genometools/GenomePositions/TGenomePosition.h"
#include "genometools/GenomePositions/TGenomeWindow.h"

//-----------------------------------------------------
// TGenomeWindow
//-----------------------------------------------------

TEST(TGenomeWindowTest, constructor_noParams){
    genometools::TGenomeWindow window;
    EXPECT_EQ(window.refID(), 0);
    EXPECT_EQ(window.fromOnChr(), 0);
    EXPECT_EQ(window.toOnChr(), 0);
}

TEST(TGenomeWindowTest, constructor_refID_start_end){
    genometools::TGenomeWindow window(10, 0, 100);
    EXPECT_EQ(window.refID(), 10);
    EXPECT_EQ(window.fromOnChr(), 0);
    EXPECT_EQ(window.toOnChr(), 100);
}

TEST(TGenomeWindowTest, constructor_refID_start){
    genometools::TGenomeWindow window(10, 100);
    EXPECT_EQ(window.refID(), 10);
    EXPECT_EQ(window.fromOnChr(), 100);
    EXPECT_EQ(window.toOnChr(), 101);
}


TEST(TGenomeWindowTest, constructor_positionFrom){
    genometools::TGenomePosition pos(10, 100);
    genometools::TGenomeWindow window(pos);
    EXPECT_EQ(window.refID(), 10);
    EXPECT_EQ(window.fromOnChr(), 100);
    EXPECT_EQ(window.toOnChr(), 101);
}

TEST(TGenomeWindowTest, constructor_positionFrom_positionTo){
    genometools::TGenomePosition from(10, 100);
    genometools::TGenomePosition to(10, 200);
    genometools::TGenomeWindow window(from, to);
    EXPECT_EQ(window.refID(), 10);
    EXPECT_EQ(window.fromOnChr(), 100);
    EXPECT_EQ(window.toOnChr(), 200);
}

TEST(TGenomeWindowTest, constructor_otherWindow){
    genometools::TGenomeWindow window(10, 100, 100);
    genometools::TGenomeWindow window2(window);
    EXPECT_EQ(window.refID(), 10);
    EXPECT_EQ(window.fromOnChr(), 100);
    EXPECT_EQ(window.toOnChr(), 200);
    EXPECT_EQ(window2.refID(), 10);
    EXPECT_EQ(window2.fromOnChr(), 100);
    EXPECT_EQ(window2.toOnChr(), 200);
}

TEST(TGenomeWindowTest, clear){
    genometools::TGenomeWindow window(10, 10, 100);
    window.clear();
    EXPECT_EQ(window.refID(), 0);
    EXPECT_EQ(window.fromOnChr(), 0);
    EXPECT_EQ(window.toOnChr(), 0);
}

TEST(TGenomeWindowTest, getters){
    genometools::TGenomeWindow window(10, 10, 90);

    genometools::TGenomePosition from = window.from();
    EXPECT_EQ(from.refID(), 10);
    EXPECT_EQ(from.position(), 10);
    EXPECT_EQ(window.fromOnChr(), 10);

    genometools::TGenomePosition to = window.to();
    EXPECT_EQ(to.refID(), 10);
    EXPECT_EQ(to.position(), 100);
    EXPECT_EQ(window.toOnChr(), 100);

    EXPECT_EQ(window.size(), 90);
}

TEST(TGenomeWindowTest, move_refId_from_to){
    genometools::TGenomeWindow window(10, 0, 100);

    // move ok
    window.move(1, 10, 1);
    EXPECT_EQ(window.refID(), 1);
    EXPECT_EQ(window.fromOnChr(), 10);
    EXPECT_EQ(window.toOnChr(), 11);
}

TEST(TGenomeWindowTest, move_from_length){
    genometools::TGenomeWindow window(10, 0, 100);

    // move ok
    genometools::TGenomePosition pos(1, 10);
    window.move(pos, 20);
    EXPECT_EQ(window.refID(), 1);
    EXPECT_EQ(window.fromOnChr(), 10);
    EXPECT_EQ(window.toOnChr(), 30);
}

TEST(TGenomeWindowTest, move_from_to){
    genometools::TGenomeWindow window(10, 0, 100);

    // move ok
    genometools::TGenomePosition from(1, 10);
    genometools::TGenomePosition to(1, 11);

    window.move(from, to);
    EXPECT_EQ(window.refID(), 1);
    EXPECT_EQ(window.fromOnChr(), 10);
    EXPECT_EQ(window.toOnChr(), 11);

    // not same chr -> throw
    to.move(2, 11);
    EXPECT_THROW(window.move(from, to), coretools::err::TDevError);

    // to > from -> throw
    to.move(1, 9);
    EXPECT_THROW(window.move(from, to), coretools::err::TDevError);
}

TEST(TGenomeWindowTest, move_otherWindow){
    genometools::TGenomeWindow window1(10, 0, 100);
    genometools::TGenomeWindow window2(1, 10, 10);

    window1.move(window2);
    EXPECT_EQ(window1.refID(), 1);
    EXPECT_EQ(window1.fromOnChr(), 10);
    EXPECT_EQ(window1.toOnChr(), 20);
    EXPECT_EQ(window2.refID(), 1);
    EXPECT_EQ(window2.fromOnChr(), 10);
    EXPECT_EQ(window2.toOnChr(), 20);
}

TEST(TGenomeWindowTest, operator_equal){
    genometools::TGenomeWindow window1;
    genometools::TGenomeWindow window2(1, 10, 10);

    window1 = window2;
    EXPECT_EQ(window1.refID(), 1);
    EXPECT_EQ(window1.fromOnChr(), 10);
    EXPECT_EQ(window1.toOnChr(), 20);
    EXPECT_EQ(window2.refID(), 1);
    EXPECT_EQ(window2.fromOnChr(), 10);
    EXPECT_EQ(window2.toOnChr(), 20);
}

TEST(TGenomeWindowTest, operator_plus){
    genometools::TGenomeWindow window1(1, 10, 10);
    genometools::TGenomeWindow window2 = window1 + 10;

    EXPECT_EQ(window1.refID(), 1);
    EXPECT_EQ(window1.fromOnChr(), 10);
    EXPECT_EQ(window1.toOnChr(), 20);
    EXPECT_EQ(window2.refID(), 1);
    EXPECT_EQ(window2.fromOnChr(), 20);
    EXPECT_EQ(window2.toOnChr(), 30);
}

TEST(TGenomeWindowTest, operator_minus){
    genometools::TGenomeWindow window1(1, 10, 10);

    // this is ok
    genometools::TGenomeWindow window2 = window1 - 5;
    EXPECT_EQ(window1.refID(), 1); // window1 doesn't change
    EXPECT_EQ(window1.fromOnChr(), 10);
    EXPECT_EQ(window1.toOnChr(), 20);
    EXPECT_EQ(window2.refID(), 1);
    EXPECT_EQ(window2.fromOnChr(), 5);
    EXPECT_EQ(window2.toOnChr(), 15);

    // overshoot _from -> set _from to zero
    window2 = window1 - 11;
    EXPECT_EQ(window2.refID(), 1);
    EXPECT_EQ(window2.fromOnChr(), 0);
    EXPECT_EQ(window2.toOnChr(), 9);

    // overshoot _from and _to -> set _from to 0 and _to to 1
    window2 = window1 - 21;
    EXPECT_EQ(window2.refID(), 1);
    EXPECT_EQ(window2.fromOnChr(), 0);
    EXPECT_EQ(window2.toOnChr(), 1);
}

TEST(TGenomeWindowTest, sameChr){
    genometools::TGenomeWindow window1(1, 10, 10);
    genometools::TGenomeWindow window2(1, 100, 100);
    // same chr
    EXPECT_TRUE(window1.sameChr(window2));

    // other chr
    window2.move(11, 100, 200);
    EXPECT_FALSE(window1.sameChr(window2));
}

TEST(TGenomeWindowTest, within){
    genometools::TGenomeWindow window(1, 10, 10);

    // these are ok
    genometools::TGenomePosition pos(1, 15);
    EXPECT_TRUE(window.within(pos));
    pos.move(1, 10);
    EXPECT_TRUE(window.within(pos));

    // position is outside
    pos.move(1, 20);
    EXPECT_FALSE(window.within(pos));
    pos.move(1, 9);
    EXPECT_FALSE(window.within(pos));

    // chromosome is different
    pos.move(2, 15);
    EXPECT_FALSE(window.within(pos));
}

TEST(TGenomeWindowTest, contains_window){
    genometools::TGenomeWindow window1(1, 10, 10);

    // these are ok
    genometools::TGenomeWindow window2(1, 11, 8);
    EXPECT_TRUE(window1.contains(window2));
    window2.move(1, 10, 9);
    EXPECT_TRUE(window1.contains(window2));
    window2.move(1, 11, 9);
    EXPECT_TRUE(window1.contains(window2));
    window2.move(1, 10, 10);
    EXPECT_TRUE(window1.contains(window2));

    // positions are outside
    window2.move(1, 9, 9);
    EXPECT_FALSE(window1.contains(window2));
    window2.move(1, 10, 11);
    EXPECT_FALSE(window1.contains(window2));

    // chromosome is different
    window2.move(2, 11, 9);
    EXPECT_FALSE(window1.contains(window2));
}

TEST(TGenomeWindowTest, overlaps){
    genometools::TGenomeWindow window1(1, 10, 10);

    // fully within
    genometools::TGenomeWindow window2(1, 11, 8);
    EXPECT_TRUE(window1.overlaps(window2));
    window2.move(1, 10, 9);
    EXPECT_TRUE(window1.overlaps(window2));
    window2.move(1, 11, 9);
    EXPECT_TRUE(window1.overlaps(window2));
    window2.move(1, 10, 10);
    EXPECT_TRUE(window1.overlaps(window2));

    // overlaps on left side
    window2.move(1, 5, 6);
    EXPECT_TRUE(window1.overlaps(window2));

    // overlaps on right side
    window2.move(1, 19, 6);
    EXPECT_TRUE(window1.overlaps(window2));

    // overlaps on both sides
    window2.move(1, 9, 12);
    EXPECT_TRUE(window1.overlaps(window2));

    // positions don't overlap
    window2.move(1, 20, 5); // extends, but doesnt overlap
    EXPECT_FALSE(window1.overlaps(window2));
    window2.move(1, 5, 5);
    EXPECT_FALSE(window1.overlaps(window2)); // extends, but doesnt overlap
    window2.move(1, 5, 2);
    EXPECT_FALSE(window1.overlaps(window2));
    window2.move(1, 21, 4);
    EXPECT_FALSE(window1.overlaps(window2));

    // chromosome is different
    window2.move(2, 11, 8);
    EXPECT_FALSE(window1.overlaps(window2));
}

TEST(TGenomeWindowTest, overlapsOrExtends){
    genometools::TGenomeWindow window1(1, 10, 10);

    // fully within
    genometools::TGenomeWindow window2(1, 11, 8);
    EXPECT_TRUE(window1.overlapsOrExtends(window2));
    window2.move(1, 10, 9);
    EXPECT_TRUE(window1.overlapsOrExtends(window2));
    window2.move(1, 11, 9);
    EXPECT_TRUE(window1.overlapsOrExtends(window2));
    window2.move(1, 10, 10);
    EXPECT_TRUE(window1.overlapsOrExtends(window2));

    // overlaps on left side
    window2.move(1, 5, 6);
    EXPECT_TRUE(window1.overlapsOrExtends(window2));
    window2.move(1, 5, 5);
    EXPECT_TRUE(window1.overlapsOrExtends(window2));

    // overlaps on right side
    window2.move(1, 19, 6);
    EXPECT_TRUE(window1.overlapsOrExtends(window2));
    window2.move(1, 20, 5); // extends
    EXPECT_TRUE(window1.overlapsOrExtends(window2));

    // overlaps on both sides
    window2.move(1, 9, 12);
    EXPECT_TRUE(window1.overlapsOrExtends(window2));

    // positions don't overlap
    window2.move(1, 5, 2);
    EXPECT_FALSE(window1.overlapsOrExtends(window2));
    window2.move(1, 21, 4);
    EXPECT_FALSE(window1.overlapsOrExtends(window2));

    // chromosome is different
    window2.move(2, 11, 8);
    EXPECT_FALSE(window1.overlapsOrExtends(window2));
}

TEST(TGenomeWindowTest, mergeWith){
    genometools::TGenomeWindow window1(1, 10, 10);

    // fully within
    genometools::TGenomeWindow window2(1, 11, 8);
    EXPECT_TRUE(window1.mergeWith(window2));
    EXPECT_EQ(window1.refID(), 1);
    EXPECT_EQ(window1.fromOnChr(), 10);
    EXPECT_EQ(window1.toOnChr(), 20);
    EXPECT_EQ(window2.refID(), 1); // window2 shouldn't change
    EXPECT_EQ(window2.fromOnChr(), 11);
    EXPECT_EQ(window2.toOnChr(), 19);

    // overlaps on left side
    window1.move(1, 10, 10);
    window2.move(1, 5, 10);
    EXPECT_TRUE(window1.mergeWith(window2));
    EXPECT_EQ(window1.refID(), 1);
    EXPECT_EQ(window1.fromOnChr(), 5);
    EXPECT_EQ(window1.toOnChr(), 20);

    // extends on left side
    window1.move(1, 10, 10);
    window2.move(1, 5, 5);
    EXPECT_TRUE(window1.mergeWith(window2));
    EXPECT_EQ(window1.refID(), 1);
    EXPECT_EQ(window1.fromOnChr(), 5);
    EXPECT_EQ(window1.toOnChr(), 20);

    // overlaps on right side
    window1.move(1, 10, 10);
    window2.move(1, 15, 10);
    EXPECT_TRUE(window1.mergeWith(window2));
    EXPECT_EQ(window1.refID(), 1);
    EXPECT_EQ(window1.fromOnChr(), 10);
    EXPECT_EQ(window1.toOnChr(), 25);

    // extends on right side
    window1.move(1, 10, 10);
    window2.move(1, 20, 5);
    EXPECT_TRUE(window1.mergeWith(window2));
    EXPECT_EQ(window1.refID(), 1);
    EXPECT_EQ(window1.fromOnChr(), 10);
    EXPECT_EQ(window1.toOnChr(), 25);

    // overlaps on both sides
    window1.move(1, 10, 11);
    window2.move(1, 9, 2);
    EXPECT_TRUE(window1.mergeWith(window2));
    EXPECT_EQ(window1.refID(), 1);
    EXPECT_EQ(window1.fromOnChr(), 9);
    EXPECT_EQ(window1.toOnChr(), 21);

    // positions don't overlap -> nothing happens
    window1.move(1, 10, 10);
    window2.move(1, 5, 4);
    EXPECT_FALSE(window1.mergeWith(window2));
    EXPECT_EQ(window1.refID(), 1);
    EXPECT_EQ(window1.fromOnChr(), 10);
    EXPECT_EQ(window1.toOnChr(), 20);

    // chromosome is different -> nothing happens
    window1.move(1, 10, 10);
    window2.move(2, 19, 6);
    EXPECT_FALSE(window1.mergeWith(window2));
    EXPECT_EQ(window1.refID(), 1);
    EXPECT_EQ(window1.fromOnChr(), 10);
    EXPECT_EQ(window1.toOnChr(), 20);
}

TEST(TGenomeWindowTest, operator_plusEqual){
    genometools::TGenomeWindow window(1, 10, 10);
    window += 10;

    EXPECT_EQ(window.refID(), 1);
    EXPECT_EQ(window.fromOnChr(), 20);
    EXPECT_EQ(window.toOnChr(), 30);
}

TEST(TGenomeWindowTest, operator_minusEqual){
    genometools::TGenomeWindow window(1, 10, 10);

    // this is ok
    window -= 5;
    EXPECT_EQ(window.refID(), 1);
    EXPECT_EQ(window.fromOnChr(), 5);
    EXPECT_EQ(window.toOnChr(), 15);

    // overshoot _from -> set _from to zero
    window.move(1, 10, 10); // reset
    window -= 11;
    EXPECT_EQ(window.refID(), 1);
    EXPECT_EQ(window.fromOnChr(), 0);
    EXPECT_EQ(window.toOnChr(), 9);

    // overshoot _from and _to -> set _from to 0 and _to to 1
    window.move(1, 10, 10); // reset
    window -= 21;
    EXPECT_EQ(window.refID(), 1);
    EXPECT_EQ(window.fromOnChr(), 0);
    EXPECT_EQ(window.toOnChr(), 1);
}

TEST(TGenomeWindowTest, resize){
    genometools::TGenomeWindow window(1, 10, 10);
    window.resize(20);

    EXPECT_EQ(window.refID(), 1);
    EXPECT_EQ(window.fromOnChr(), 10);
    EXPECT_EQ(window.toOnChr(), 30);
}

TEST(TGenomeWindowTest, operator_smallerPosition){
    genometools::TGenomeWindow window(1, 10, 10);

    // pos is larger than _to
    genometools::TGenomePosition pos(1, 25);
    EXPECT_TRUE(window < pos);

    // pos is equal to _to
    pos.move(1, 20);
    EXPECT_TRUE(window < pos);

    // pos is smaller than _to
    pos.move(1, 14);
    EXPECT_FALSE(window < pos);

    // pos is smaller than _from
    pos.move(1, 4);
    EXPECT_FALSE(window < pos);

    // pos is a smaller chromosome
    pos.move(0, 25);
    EXPECT_FALSE(window < pos);

    // pos is a larger chromosome
    pos.move(2, 23);
    EXPECT_TRUE(window < pos);
}

TEST(TGenomeWindowTest, operator_largerPosition){
    genometools::TGenomeWindow window(1, 10, 10);

    // pos is smaller than _from
    genometools::TGenomePosition pos(1, 5);
    EXPECT_TRUE(window > pos);

    // pos is equal to _from
    pos.move(1, 10);
    EXPECT_FALSE(window > pos);

    // pos is larger than _from
    pos.move(1, 14);
    EXPECT_FALSE(window > pos);

    // pos is larger than _to
    pos.move(1, 24);
    EXPECT_FALSE(window > pos);

    // pos is a smaller chromosome
    pos.move(0, 25);
    EXPECT_TRUE(window > pos);

    // pos is a larger chromosome
    pos.move(2, 23);
    EXPECT_FALSE(window > pos);
}

TEST(TGenomeWindowTest, operator_smallerWindow){
    genometools::TGenomeWindow window1(1, 10, 10);

    // window1.from is smaller than window2.from
    genometools::TGenomeWindow window2(1, 15, 3);
    EXPECT_TRUE(window1 < window2);

    // window1.from is equal to window2.from
    window2.move(1, 10, 5);
    EXPECT_FALSE(window1 < window2);

    // window1.from is larger than window2.from
    window2.move(1, 5, 5);
    EXPECT_FALSE(window1 < window2);

    // window1 is on a larger chromosome than window2
    window2.move(0, 25, 5);
    EXPECT_FALSE(window1 < window2);

    // window1 is on a smaller chromosome than window2
    window2.move(2, 25, 5);
    EXPECT_TRUE(window1 < window2);
}

TEST(TGenomeWindowTest, operator_largerWindow){
    genometools::TGenomeWindow window1(1, 10, 10);

    // window1.from is larger than window2.from
    genometools::TGenomeWindow window2(1, 5, 10);
    EXPECT_TRUE(window1 > window2);

    // window1.from is equal to window2.from
    window2.move(1, 10, 5);
    EXPECT_FALSE(window1 > window2);

    // window1.from is smaller than window2.from
    window2.move(1, 12, 3);
    EXPECT_FALSE(window1 > window2);

    // window1 is on a smaller chromosome than window2
    window2.move(2, 25, 5);
    EXPECT_FALSE(window1 > window2);

    // window1 is on a larger chromosome than window2
    window2.move(0, 25, 5);
    EXPECT_TRUE(window1 > window2);
}

TEST(TGenomeWindowTest, merge){
    genometools::TGenomeWindow window1(1, 10, 10);

    // fully within
    genometools::TGenomeWindow window2(1, 11, 8);
    genometools::TGenomeWindow res = merge(window1, window2);
    EXPECT_EQ(res.refID(), 1);
    EXPECT_EQ(res.fromOnChr(), 10);
    EXPECT_EQ(res.toOnChr(), 20);

    // overlaps on left side
    window2.move(1, 5, 10);
    res = merge(window1, window2);
    EXPECT_EQ(res.refID(), 1);
    EXPECT_EQ(res.fromOnChr(), 5);
    EXPECT_EQ(res.toOnChr(), 20);

    // extends on left side
    window2.move(1, 5, 5);
    res = merge(window1, window2);
    EXPECT_EQ(res.refID(), 1);
    EXPECT_EQ(res.fromOnChr(), 5);
    EXPECT_EQ(res.toOnChr(), 20);

    // overlaps on right side
    window2.move(1, 15, 10);
    res = merge(window1, window2);
    EXPECT_EQ(res.refID(), 1);
    EXPECT_EQ(res.fromOnChr(), 10);
    EXPECT_EQ(res.toOnChr(), 25);

    // extends on right side
    window2.move(1, 20, 5);
    res = merge(window1, window2);
    EXPECT_EQ(res.refID(), 1);
    EXPECT_EQ(res.fromOnChr(), 10);
    EXPECT_EQ(res.toOnChr(), 25);

    // overlaps on both sides
    window2.move(1, 9, 12);
    res = merge(window1, window2);
    EXPECT_EQ(res.refID(), 1);
    EXPECT_EQ(res.fromOnChr(), 9);
    EXPECT_EQ(res.toOnChr(), 21);

    // positions don't overlap -> nothing happens
    window2.move(1, 5, 4);
    EXPECT_THROW(merge(window1, window2), coretools::err::TDevError);

    // chromosome is different -> nothing happens
    window2.move(2, 19, 6);
    EXPECT_THROW(merge(window1, window2), coretools::err::TDevError);
}

TEST(TGenomeWindowTest, operator_string){    
    genometools::TGenomeWindow window(10, 100, 100);

    EXPECT_EQ((std::string) window, "10:100-10:200");
}
