//
// Created by caduffm on 3/11/22.
//

#include "gtest/gtest.h"

#include "coretools/Types/probability.h"
#include "genometools/THardyWeinbergGenotypeProbabilities.h"

using namespace genometools;
using coretools::P;

TEST(THardyWeinbergGenotypeProbabilitiesTest, all) {
	using BG = BiallelicGenotype;
	const auto f = P(0.78);
	THardyWeinbergGenotypeProbabilities probs(f);

	EXPECT_DOUBLE_EQ(probs.f(), f);
	EXPECT_DOUBLE_EQ(probs[BG::haploidFirst], 1. - f);
	EXPECT_DOUBLE_EQ(probs[BG::haploidSecond], f);
	EXPECT_DOUBLE_EQ(probs[BG::homoFirst], (1. - f) * (1. - f));
	EXPECT_DOUBLE_EQ(probs[BG::het], 2. * f * (1. - f));
	EXPECT_DOUBLE_EQ(probs[BG::homoSecond], f * f);

	EXPECT_DOUBLE_EQ(probs[BG::homoFirst] + probs[BG::het] + probs[BG::homoSecond], 1.0);
}

TEST(THardyWeinbergWithInbreedingGenotypeProbabilitiesTest, all) {
	using BG = BiallelicGenotype;
	const auto f = P(0.78);
	const auto F = P(0.4);
	THardyWeinbergWithInbreedingGenotypeProbabilities probs(f, F);

	EXPECT_DOUBLE_EQ(probs.f(), f);
	EXPECT_DOUBLE_EQ(probs[BG::haploidFirst], 1. - f);
	EXPECT_DOUBLE_EQ(probs[BG::haploidSecond], f);
	EXPECT_DOUBLE_EQ(probs[BG::homoFirst], (1. - F) * (1. - f) * (1. - f) + F * (1. - f));
	EXPECT_DOUBLE_EQ(probs[BG::het], (1. - F) * 2. * f * (1. - f));
	EXPECT_DOUBLE_EQ(probs[BG::homoSecond], (1. - F) * f * f + F * f);

	EXPECT_DOUBLE_EQ(probs[BG::homoFirst] + probs[BG::het] + probs[BG::homoSecond], 1.0);
}
