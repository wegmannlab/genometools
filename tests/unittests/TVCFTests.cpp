#include "coretools/Containers/TStrongArray.h"
#include "coretools/Types/probability.h"
#include "genometools/Genotypes/Ploidy.h"
#include "genometools/VCF/TVCFWriter.h"
#include "gtest/gtest.h"

using namespace genometools;
using coretools::HPPhredInt;

TEST(VCFTests, write) {
	TChromosomes chrs;
	chrs.appendChromosome("chr1", 4321);
	chrs.appendChromosome("chr2", 5432);
	chrs.appendChromosome("chr3", 6543);
	chrs.back().setInUse(false);

	TVCFWriter writer("test.vcf", "testSource", {"diplo1", "diplo2", "haplo1"}, chrs, false);

	writer.writeSite("chr1\t1\t.\t.\t.\t0\t.\tDP=0");
	writer.writeSite("chr1", 2, ".", Base::A, Base::C, coretools::fromChar('A'), ".", "DP=5");
	writer.writeSite("chr1", 3, ".", "G", "T", coretools::fromChar('A'), ".", "DP=5");
	writer.writeSite("chr1", 4, ".", Base::N, Base::G, coretools::fromChar('A'), ".", "DP=2", "GT",
	                 {"0/0", "1/1", "0/1"});

	std::vector<TVCFEntry> entries;
	for (size_t i = 0; i < 2; ++i) {
		coretools::TStrongArray<HPPhredInt, Diploid> sa = {{HPPhredInt(i), HPPhredInt(i + 1), HPPhredInt(i + 2)}};
		entries.emplace_back(sa, i + 1);
	}
	coretools::TStrongArray<HPPhredInt, Haploid> sa = {{HPPhredInt(0), HPPhredInt(100)}};
	entries.emplace_back(sa, 3);
	writer.writeSite("chr2", 1, Base::A, Base::C, coretools::fromChar('Z'), entries);

	std::swap(entries[0].values[Diploid::homoFirst], entries[0].values[Diploid::het]);
	for (auto &e : entries) e.depth += 1;
	writer.writeSite("chr2", 2, Base::G, Base::T, coretools::fromChar('W'), entries);

	std::swap(entries[0].values[Diploid::het], entries[0].values[Diploid::homoSecond]);
	for (auto &e : entries) e.depth += 1;
	writer.writeSite("chr2", 99, Base::C, Base::A, coretools::fromChar('A'), entries);
}

TEST(VCFTests, findMajorMinorAllele) {
	// 1) ref is unique major allele, and there is a unique minor allele
	coretools::TStrongArray<size_t, Base, 4> alleleCounts1({10, 20, 5, 15});
	auto ref            = Base::C;
	auto [major, minor] = findMajorMinorAllele(alleleCounts1, ref);
	EXPECT_EQ(major, ref);
	EXPECT_EQ(minor, Base::T);

	// 2) ref is unique major allele, and there are multiple minor alleles
	//    -> chose randomly among minor alleles
	coretools::TStrongArray<size_t, Base, 4> alleleCounts2({0, 0, 10, 0});
	ref = Base::G;

	size_t N = 1e04;
	coretools::TStrongArray<size_t, Base, 4> counter{};
	for (size_t i = 0; i < N; ++i) {
		auto [major, minor] = findMajorMinorAllele(alleleCounts2, ref);
		EXPECT_EQ(major, ref);
		counter[minor]++;
	}
	EXPECT_EQ(counter[Base::A] + counter[Base::C] + counter[Base::T], N);
	EXPECT_TRUE(counter[Base::A] > 0);
	EXPECT_TRUE(counter[Base::C] > 0);
	EXPECT_TRUE(counter[Base::T] > 0);

	// 3) there are multiple major alleles, but ref is none of them
	//    -> this should never happen for bi-allelic simulators!
	coretools::TStrongArray<size_t, Base, 4> alleleCounts3({10, 0, 10, 0});
	ref = Base::C;
	EXPECT_ANY_THROW(findMajorMinorAllele(alleleCounts3, ref));

	// 4) there are multiple major alleles, one of them is ref
	//    -> chose randomly among major alleles; if ref is not major allele, then it must be the minor allele
	coretools::TStrongArray<size_t, Base, 4> alleleCounts4({10, 10, 10, 0});
	ref = Base::G;

	coretools::TStrongArray<size_t, Base, 4> counterMajor{};
	coretools::TStrongArray<size_t, Base, 4> counterMinor{};
	for (size_t i = 0; i < N; ++i) {
		auto [major, minor] = findMajorMinorAllele(alleleCounts4, ref);
		EXPECT_TRUE((major == ref || minor == ref));  // must be either major or minor
		EXPECT_FALSE((major == ref && minor == ref)); // can not be both at once
		counterMajor[major]++;
		counterMinor[minor]++;
	}
	EXPECT_EQ(counterMajor[Base::A] + counterMajor[Base::C] + counterMajor[Base::G], N);
	EXPECT_EQ(counterMinor[Base::A] + counterMinor[Base::C] + counterMinor[Base::G], N);

	EXPECT_TRUE(counterMajor[Base::A] > 0);
	EXPECT_TRUE(counterMajor[Base::G] > 0);
	EXPECT_TRUE(counterMajor[Base::C] > 0);
	EXPECT_TRUE(counterMinor[Base::A] > 0);
	EXPECT_TRUE(counterMinor[Base::G] > 0);
	EXPECT_TRUE(counterMinor[Base::C] > 0);
}
