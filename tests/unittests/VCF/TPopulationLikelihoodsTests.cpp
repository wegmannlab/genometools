//
// Created by caduffm on 3/14/22.
//
#include "gmock/gmock.h"
#include "gtest/gtest.h"

#include "coretools/Files/TOutputFile.h"
#include "coretools/Files/gzstream.h"
#include "coretools/Types/probability.h"
#include "genometools/VCF/TPopulationLikelihoods.h"

using namespace testing;
using namespace genometools;
using namespace coretools::str;
using namespace coretools::instances;
using coretools::PhredInt;

//--------------------------------------------
// TPopulationLikelihoods
//--------------------------------------------

class TPopulationLikelihoodsTest : public Test {
protected:
	std::vector<uint8_t> phred_g1 = {0, 1, 0, 0, 3, 0, 1, 2, 0, 0, 2, 0, 1, 0, 0, 1, 0, 0, 1, 1, 1, 0, 0, 3, 1,
	                                  0, 0, 1, 0, 1, 1, 0, 0, 1, 1, 1, 2, 3, 2, 0, 3, 0, 0, 1, 0, 0, 0, 1, 2, 0};
	std::vector<uint8_t> phred_g2 = {1, 2, 0, 0, 0, 1, 1, 0, 1, 1, 1, 0, 0, 2, 1, 2, 2, 0, 1, 1, 0, 2, 0, 0, 0,
	                                  0, 0, 0, 2, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1};
	std::vector<uint8_t> phred_g3 = {1, 0, 1, 3, 1, 2, 0, 1, 0, 1, 0, 0, 1, 0, 2, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0,
	                                  1, 3, 0, 0, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0, 1, 2, 3, 0, 0, 1, 0, 2, 1, 0, 1};
	std::vector<uint16_t> depth    = {14, 11, 9, 9, 6,  8, 12, 9,  10, 11, 5,  14, 7,  13, 13, 11, 11,
                                   5,  9,  8, 9, 8,  6, 7,  10, 14, 10, 11, 13, 10, 7,  9,  11, 13,
                                   8,  7,  9, 6, 10, 6, 15, 15, 10, 9,  11, 9,  6,  14, 9,  14};

public:
	std::string filename;

	size_t numLoci;
	size_t numIndiv;

	std::shared_ptr<TDistancesBinned<uint8_t>> distances;

	using Type = TSampleLikelihoods<PhredInt>;
	TPopulationLikelihoods<Type> likelihoods;

	void SetUp() {
		numLoci  = 10;
		numIndiv = 5;
		filename = "geneticData.vcf.gz";
		parameters().add("vcf", filename);
		parameters().add("minMAF", "0");
		distances = std::make_shared<TDistancesBinned<uint8_t>>(1000000);
	}

	void writeVCFHeader(gz::ogzstream &VCF, const std::shared_ptr<coretools::TNamesEmpty> &SampleNames) {
		// write info
		VCF << "##fileformat=VCFv4.2\n";
		VCF << "##source=Simulation\n";
		VCF << "##FORMAT=<ID=GT,Number=1,Type=String,Description=\"Genotype\">\n";
		VCF << "##FORMAT=<ID=GQ,Number=1,Type=Integer,Description=\"Genotype Quality\">\n";
		VCF << "##FORMAT=<ID=PL,Number=G,Type=Integer,Description=\"Phred-scaled genotype likelihoods\">\n";
		VCF << "##FORMAT=<ID=DP,Number=G,Type=Integer,Description=\"Depth at site\">\n";

		// write header
		VCF << "#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT";
		for (size_t i = 0; i < SampleNames->size(); i++) { VCF << "\t" << (*SampleNames)[i]; }
	}

	void writeToVcfFile(std::vector<std::string> &SampleNames) {
		// generate positions
		std::unique_ptr<TPositionsRaw> positions = std::make_unique<TPositionsRaw>();
		for (size_t l = 0; l < numLoci; ++l) { // loop over loci
			positions->add(l + 1, "1");
		}
		positions->finalizeFilling();

		// generate name classes
		std::shared_ptr<coretools::TNamesEmpty> individualNames =
		    std::make_shared<coretools::TNamesStrings>(SampleNames);
		std::unique_ptr<coretools::TNamesEmpty> lociNames = std::make_unique<TNamesPositions>(positions.get());

		// write VCF
		gz::ogzstream vcf;
		vcf.open(filename.c_str());
		writeVCFHeader(vcf, individualNames);

		size_t linearIndex = 0;
		for (size_t l = 0; l < numLoci; ++l) {
			// write new site
			std::vector<std::string> chunk_pos = lociNames->getName(l);
			vcf << '\n' << chunk_pos[0] << '\t' << chunk_pos[1] << "\t.\tA\tC\t50\t.\t.\tGT:GQ:PL:DP";
			for (size_t i = 0; i < numIndiv; ++i, ++linearIndex) {
				TSampleLikelihoods data((PhredInt(phred_g1[linearIndex])),
				                        PhredInt(phred_g2[linearIndex]),
				                        PhredInt(phred_g3[linearIndex]));
				vcf << '\t' << toString(data.mostLikelyGenotype()) << ':' << toString(data.secondMostLikelyGenotype()) << ':';
				vcf << toString(data[BiallelicGenotype::homoFirst]) + "," + toString(data[BiallelicGenotype::het]) +
				           "," + toString(data[BiallelicGenotype::homoSecond]);
				vcf << ":" << depth[linearIndex];
			}
		}
		vcf.close();
	}
};

TEST_F(TPopulationLikelihoodsTest, prepareVCF) {
	std::vector<std::string> sampleNames = {"Indiv1", "Indiv2", "Indiv3", "Indiv4", "Indiv5"};
	writeToVcfFile(sampleNames);

	likelihoods.readData();

	auto storage = likelihoods.getStorage();

	EXPECT_EQ(storage.getDimensionName(1)->size(), 5);
	EXPECT_EQ((*storage.getDimensionName(1))[0], "Indiv1");
	EXPECT_EQ((*storage.getDimensionName(1))[1], "Indiv2");
	EXPECT_EQ((*storage.getDimensionName(1))[2], "Indiv3");
	EXPECT_EQ((*storage.getDimensionName(1))[3], "Indiv4");
	EXPECT_EQ((*storage.getDimensionName(1))[4], "Indiv5");

	remove(filename.c_str());
}

TEST_F(TPopulationLikelihoodsTest, prepareVCF_duplicateSampleNames) {
	std::vector<std::string> sampleNames = {"Indiv1", "Indiv2", "Indiv2", "Indiv4", "Indiv5"};
	writeToVcfFile(sampleNames);

	EXPECT_ANY_THROW(likelihoods.readData());

	remove(filename.c_str());
}

TEST_F(TPopulationLikelihoodsTest, prepareVCF_noShuffling) {
	std::vector<std::string> sampleNames = {"Indiv1", "Indiv2", "Indiv3", "Indiv4", "Indiv5"};
	writeToVcfFile(sampleNames);

	std::vector<std::pair<bool, size_t>> orderOfIndividuals;
	for (size_t i = 0; i < sampleNames.size(); i++) { orderOfIndividuals.emplace_back(std::make_pair(true, i)); }

	// write sample file
	coretools::TOutputFile file("samples.txt", {"Sample", "Population"});
	file.writeln("Indiv1", "PopA");
	file.writeln("Indiv2", "PopA");
	file.writeln("Indiv3", "PopA");
	file.writeln("Indiv4", "PopA");
	file.writeln("Indiv5", "PopA");
	file.close();

	parameters().add("samples", "samples.txt");
	likelihoods.readData();
	auto storage = likelihoods.getStorage();

	// check for dimensions
	EXPECT_EQ(storage.numDim(), 2);
	EXPECT_THAT(storage.dimensions(), ElementsAre(10, 5));

	// check for elements
	for (size_t il = 0; il < phred_g1.size(); il++) {
		EXPECT_FLOAT_EQ(coretools::Probability(storage[il][BiallelicGenotype::homoFirst]),
		                TVcfParser::dePhred(phred_g1[il]));
		EXPECT_FLOAT_EQ(coretools::Probability(storage[il][BiallelicGenotype::het]), TVcfParser::dePhred(phred_g2[il]));
		EXPECT_FLOAT_EQ(coretools::Probability(storage[il][BiallelicGenotype::homoSecond]),
		                TVcfParser::dePhred(phred_g3[il]));
	}

	// check for names
	auto lociNames = storage.getDimensionName(0);
	EXPECT_EQ(lociNames->size(), 10);
	EXPECT_EQ((*lociNames)[0], "1:1");
	EXPECT_EQ((*lociNames)[1], "1:2");
	EXPECT_EQ((*lociNames)[2], "1:3");
	EXPECT_EQ((*lociNames)[3], "1:4");
	EXPECT_EQ((*lociNames)[4], "1:5");
	EXPECT_EQ((*lociNames)[5], "1:6");
	EXPECT_EQ((*lociNames)[6], "1:7");
	EXPECT_EQ((*lociNames)[7], "1:8");
	EXPECT_EQ((*lociNames)[8], "1:9");
	EXPECT_EQ((*lociNames)[9], "1:10");

	auto indivNames = storage.getDimensionName(1);
	EXPECT_EQ(indivNames->size(), 5);
	EXPECT_EQ((*indivNames)[0], "Indiv1");
	EXPECT_EQ((*indivNames)[1], "Indiv2");
	EXPECT_EQ((*indivNames)[2], "Indiv3");
	EXPECT_EQ((*indivNames)[3], "Indiv4");
	EXPECT_EQ((*indivNames)[4], "Indiv5");

	remove(filename.c_str());
	remove("samples.txt");
}

TEST_F(TPopulationLikelihoodsTest, prepareVCF_ShuffleSkip) {
	std::vector<std::string> sampleNames = {"Indiv3", "Indiv1", "Indiv10", "Indiv4", "Indiv2"};
	writeToVcfFile(sampleNames);

	std::vector<std::pair<bool, size_t>> orderOfIndividuals;
	orderOfIndividuals.emplace_back(std::make_pair(true, 2));
	orderOfIndividuals.emplace_back(std::make_pair(true, 0));
	orderOfIndividuals.emplace_back(std::make_pair(false, 0));
	orderOfIndividuals.emplace_back(std::make_pair(true, 3));
	orderOfIndividuals.emplace_back(std::make_pair(true, 1));

	// write sample file
	coretools::TOutputFile file("samples.txt", {"Sample"});
	file.writeln("Indiv1");
	file.writeln("Indiv2");
	file.writeln("Indiv3");
	file.writeln("Indiv4");
	file.close();
	numIndiv = 4;

	parameters().add("samples", "samples.txt");
	likelihoods.readData();
	auto storage = likelihoods.getStorage();

	// check for dimensions
	EXPECT_EQ(storage.numDim(), 2);
	EXPECT_THAT(storage.dimensions(), ElementsAre(10, 4));

	// check for elements
	std::vector<uint16_t> phred_g1_shuffled = {1, 3, 0, 0, 1, 0, 0, 0, 0, 0, 2, 0, 0, 1, 1, 1, 0, 1, 1, 3,
	                                           0, 1, 0, 0, 0, 1, 1, 1, 2, 0, 1, 2, 0, 0, 3, 1, 0, 0, 0, 2};
	std::vector<uint16_t> phred_g2_shuffled = {2, 0, 1, 0, 1, 1, 1, 1, 0, 1, 1, 2, 2, 1, 2, 1, 2, 0, 0, 0,
	                                           0, 0, 0, 2, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0};
	std::vector<uint16_t> phred_g3_shuffled = {0, 1, 1, 3, 0, 1, 2, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	                                           3, 1, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 3, 1, 2, 0, 2, 1, 0, 0};

	for (size_t il = 0; il < phred_g1_shuffled.size(); il++) {
		EXPECT_FLOAT_EQ(coretools::Probability(storage[il][BiallelicGenotype::homoFirst]),
		                TVcfParser::dePhred(phred_g1_shuffled[il]));
		EXPECT_FLOAT_EQ(coretools::Probability(storage[il][BiallelicGenotype::het]),
		                TVcfParser::dePhred(phred_g2_shuffled[il]));
		EXPECT_FLOAT_EQ(coretools::Probability(storage[il][BiallelicGenotype::homoSecond]),
		                TVcfParser::dePhred(phred_g3_shuffled[il]));
	}

	// check for names
	auto lociNames = storage.getDimensionName(0);
	EXPECT_EQ(lociNames->size(), 10);
	EXPECT_EQ((*lociNames)[0], "1:1");
	EXPECT_EQ((*lociNames)[1], "1:2");
	EXPECT_EQ((*lociNames)[2], "1:3");
	EXPECT_EQ((*lociNames)[3], "1:4");
	EXPECT_EQ((*lociNames)[4], "1:5");
	EXPECT_EQ((*lociNames)[5], "1:6");
	EXPECT_EQ((*lociNames)[6], "1:7");
	EXPECT_EQ((*lociNames)[7], "1:8");
	EXPECT_EQ((*lociNames)[8], "1:9");
	EXPECT_EQ((*lociNames)[9], "1:10");

	auto indivNames = storage.getDimensionName(1);
	EXPECT_EQ(indivNames->size(), 4);
	EXPECT_EQ((*indivNames)[0], "Indiv1");
	EXPECT_EQ((*indivNames)[1], "Indiv2");
	EXPECT_EQ((*indivNames)[2], "Indiv3");
	EXPECT_EQ((*indivNames)[3], "Indiv4");

	remove(filename.c_str());
	remove("samples.txt");
}
