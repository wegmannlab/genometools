#include "coretools/Main/TParameters.h"
#include "genometools/GLF/TGLFMultiReader.h"
#include "genometools/GLF/TGLFReader.h"
#include "genometools/GLF/TGLFWriter.h"
#include "genometools/GenomePositions/TChromosomes.h"
#include "genometools/GenomePositions/TGenomePosition.h"
#include "genometools/Genotypes/Containers.h"
#include "gtest/gtest.h"
#include <algorithm>

using namespace genometools;
using coretools::P;
using coretools::instances::parameters;

namespace impl {
void remove(std::string filename) {
	std::remove(filename.c_str());
	filename.pop_back();
	filename.pop_back();
	filename += "idx";
	std::remove(filename.c_str());
}
}

TEST(GLFTests, writeRead) {
	const auto fName = "test.values.gz";
	TChromosomes chrs;
	chrs.appendChromosome("1", 12);
	chrs.appendChromosome("2", 123);
	chrs.appendChromosome("3", 1234);
	chrs.appendChromosome("4", 12345);


	std::array<TGenotypeLikelihoods, 3> lks{TGenotypeLikelihoods(P(1)), TGenotypeLikelihoods(P(0)),
										TGenotypeLikelihoods(P(0))};

	lks[1][Genotype::AA] = P(1);
	lks[1][Genotype::CC] = P(1);
	lks[1][Genotype::GG] = P(1);
	lks[1][Genotype::TT] = P(1);

	lks[2][Genotype::AA] = P(1);
	lks[2][Genotype::AC] = P(0.6);
	lks[2][Genotype::AG] = P(0.3);
	lks[2][Genotype::AT] = P(0.1);

	constexpr std::array<size_t, 4> addon = {1, 11, 111, 1111};

	TGLFWriter writer(fName, chrs);
	for (size_t i = 0; i < chrs.size(); ++i) {
		writer.newChromosome(chrs[i]);
		for (size_t j = 0; j < lks.size(); ++j) {
			writer.writeSite(j+addon[i], j+addon[i], 0, lks[j]);
		}
	}
	writer.close();

	TGLFReader reader(fName);

	for (size_t refID = 0; refID < 4; ++refID) {
		EXPECT_EQ(reader.position(), TGenomePosition(refID, addon[refID]));
		EXPECT_EQ(reader.depth(), addon[refID]);
		EXPECT_EQ(reader.curChr().name(), chrs[refID].name());
		EXPECT_TRUE(std::equal(reader.front().values.begin() + 1, reader.front().values.end(), reader.front().values.begin()));

		reader.popFront();
		EXPECT_EQ(reader.position(), TGenomePosition(refID, addon[refID] + 1));
		EXPECT_EQ(reader.depth(), 1 + addon[refID]);
		EXPECT_EQ(reader.front()[Genotype::AA], reader.front()[Genotype::CC]);
		EXPECT_EQ(reader.front()[Genotype::GG], reader.front()[Genotype::CC]);
		EXPECT_EQ(reader.front()[Genotype::GG], reader.front()[Genotype::TT]);

		reader.popFront();
		EXPECT_EQ(reader.position(), TGenomePosition(refID, 2 + addon[refID]));
		EXPECT_EQ(reader.depth(), 2 + addon[refID]);
		EXPECT_TRUE(reader.front()[Genotype::AA] < reader.front()[Genotype::AC]);
		EXPECT_TRUE(reader.front()[Genotype::AC] < reader.front()[Genotype::AG]);
		EXPECT_TRUE(reader.front()[Genotype::AG] < reader.front()[Genotype::AT]);
		EXPECT_TRUE(reader.front()[Genotype::AT] < reader.front()[Genotype::CC]);
		reader.popFront();
	}
	impl::remove(fName);
}

TEST(GLFTests, multiReader) {
	TChromosomes chrs;
	chrs.appendChromosome("1", 12);
	chrs.appendChromosome("2", 123);
	chrs.appendChromosome("3", 1234);
	chrs.appendChromosome("4", 12345);

	std::array<TGenotypeLikelihoods, 3> lks{TGenotypeLikelihoods(P(1)), TGenotypeLikelihoods(P(0)),
										TGenotypeLikelihoods(P(0))};

	lks[1][Genotype::AA] = P(1);
	lks[1][Genotype::CC] = P(1);
	lks[1][Genotype::GG] = P(1);
	lks[1][Genotype::TT] = P(1);

	lks[2][Genotype::AA] = P(1);
	lks[2][Genotype::AC] = P(0.6);
	lks[2][Genotype::AG] = P(0.3);
	lks[2][Genotype::AT] = P(0.1);

	constexpr std::array fNames{"1.values.gz", "2.values.gz", "3.values.gz", "4.values.gz", "5.values.gz"};

	for (const auto &fName : fNames) {
		constexpr std::array<size_t, 4> addon = {1, 11, 111, 1111};

		TGLFWriter writer(fName, chrs);
		for (size_t i = 0; i < chrs.size(); ++i) {
			writer.newChromosome(chrs[i]);
			for (size_t j = 0; j < lks.size(); ++j) { writer.writeSite(j + addon[i], j + addon[i], 0, lks[j]); }
		}
		writer.close();
	}

	parameters().add("glf", "1.values.gz,2.values.gz,3.values.gz,4.values.gz,5.values.gz");
	size_t refID = 0;
	for(TGLFMultiReader muReader; !muReader.empty(); muReader.popFront()) {
		EXPECT_EQ(muReader.curChr().refID(), refID);
		++refID;
	}

	for (const auto &fName : fNames) { impl::remove(fName); }
}
