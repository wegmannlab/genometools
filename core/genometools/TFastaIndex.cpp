#include "TFastaIndex.h"

namespace genometools{

    TFastaIndex::TFastaIndex(std::string_view filename){
        open(filename);
    }

    void TFastaIndex::open(std::string_view filename){   
        if(_file.isOpen()){
            DEVERROR("Fasta index file is already open!");
        }

        //open file
        _file.open(filename, coretools::FileType::NoHeader);

        popFront();
    }

    void TFastaIndex::close(){
        _file.close();
    }

    // read next line, if there is no next line -> close file
    void TFastaIndex::popFront(){
		if (_file.empty()) {
			_file.close();
			return;
		}

        //set chr and position
        _fastaIndex.contig = _file.get(0);
		_file.fill(1, _fastaIndex.length);
		_file.fill(2, _fastaIndex.offset);
		_file.fill(3, _fastaIndex.lineBases);
		_file.fill(4, _fastaIndex.lineBytes);
		_file.popFront();
    }
}; //end namespace
