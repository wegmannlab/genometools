#include "TAlleles.h"

#include "coretools/Files/TInputFile.h"
#include "coretools/Main/TLog.h"
#include "genometools/GenomePositions/TChromosomes.h"

namespace genometools {
using coretools::instances::logfile;

void TAlleles::parse(std::string_view Filename, const TChromosomes &Chromosomes, Morphic M) {
	_chromosomes.assign(Chromosomes.size(), "");
	for (const auto &chr : Chromosomes) { _chromosomes[chr.refID()] = chr.name(); }
	parse(Filename, M);
}

void TAlleles::parse(std::string_view Filename, coretools::TConstView<std::string> Chromosomes, Morphic M) {
	_chromosomes.clear();
	_chromosomes.reserve(Chromosomes.size());
	for (const auto &chr : Chromosomes) { _chromosomes.push_back(chr); }
	parse(Filename, M);
}

void TAlleles::parse(std::string_view Filename, Morphic M) {
	coretools::instances::logfile().list("Reading sites to be used from '", Filename, "'.");

	_allSites.clear();
	_sites.clear();
	if (Filename.empty()) { return; }

	coretools::TInputFile in(Filename, coretools::FileType::Header);

    std::array<size_t, 4> idx;
	idx[0] = in.indexOfFirstMatch({"CHR", "Chr", "chr", "CHROMOSOME", "Chromosome", "chromosome", "#CHROM"});
	idx[1] = in.indexOfFirstMatch({"POS", "Pos", "pos", "POSITION", "Position", "position"});
	idx[2] = in.indexOfFirstMatch({"REF", "Ref", "ref", "REFERENCE", "Reference", "reference", "Allele", "Allele1"});

	if (M == Morphic::Poly) {
		idx[3] = in.indexOfFirstMatch({"ALT", "Alt", "alt", "ALTERNATIVE", "Alternative", "alternative", "Allele2",
									   "DERIVED", "Derived", "derived"});
	}

    std::vector<std::string> unknownChrom;
    for (; !in.empty(); in.popFront()) {
		const auto chr = in.get(idx[0]);
		const auto rID = refID(chr);
		if (rID == _chromosomes.size()) {
			unknownChrom.emplace_back(chr);
			continue;
		}
		
		const auto pos = in.get<size_t>(idx[1]) - 1;
		const auto ref = char2base(in.get(idx[2]).front());
		if (M == Morphic::Mono) {
			_allSites.emplace_back(rID, pos, ref, Base::N);
		} else {
			const auto alt = char2base(in.get(idx[3]).front());
			if (alt == ref) {
				UERROR("reference and alternative base are the same in sites file '", Filename, "': ", in.frontRaw(), "!");
			}
			_allSites.emplace_back(rID, pos, ref, alt);
		}
	}

	if (unknownChrom.size() > 0) {
		std::sort(unknownChrom.begin(), unknownChrom.end());
		unknownChrom.erase(std::unique(unknownChrom.begin(), unknownChrom.end()), unknownChrom.end());
		std::string list;
		for (const auto &u : unknownChrom) { list += "'" + u + "', "; }
		list.resize(list.size() - 2);
		if (_allSites.empty()) {
			UERROR("None of the chromosome-names :", list, " exist in BAM header / GLF index!");
		} else {
			logfile().warning(
				"Sites of the following chromosomes were ignored because these chromosomes were not in the "
				"BAM header / GLF index: ",
				list, ".");
		}
	}

	if (_allSites.empty()) {
		logfile().warning("Empty alleles-file!");
		return;
	}

	std::sort(_allSites.begin(), _allSites.end());

	_sites.assign(_chromosomes.size(), {end(), 0});
	_nChrSites   = 0;
	auto refID   = _allSites.front().position.refID();
	auto data    = _allSites.data();
	size_t n     = 1;
	for (size_t i = 1; i < _allSites.size(); ++i) {
		if (_allSites[i].position == _allSites[i - 1].position) {
			UERROR("Duplicates in sites file '", Filename, "': position ", name(_allSites[i].position.refID()), ":",
				   _allSites[i].position.position(), " is present multiple times!");
		}
		if (_allSites[i].position.refID() != refID) {
			_sites[refID] = {data, n};
			data          = _allSites.data() + i;
			n             = 1;
			refID         = _allSites[i].position.refID();
			++_nChrSites;
		} else {
			n++;
		}
	}
	_sites[refID] = {data, n};
	++_nChrSites;

	coretools::instances::logfile().conclude("Parsed ", size(), " sites on ", _nChrSites, " chromosomes.");

}
}
