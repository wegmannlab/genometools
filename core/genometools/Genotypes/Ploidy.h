#ifndef GENOMETOOLS_GENOTYPES_PLOIDY_H
#define GENOMETOOLS_GENOTYPES_PLOIDY_H

#include "coretools/enum.h"
#include "genometools/Genotypes/BiallelicGenotype.h"
#include <cstdint>
#include <string>
#include <array>

namespace genometools {
enum class Ploidy : uint8_t { min = 0, haploid = min, diploid, max};
enum class Haploid: uint8_t {min = 0, first = min, second, max};
enum class Diploid: uint8_t {min = 0, homoFirst = min, het, homoSecond, max};

constexpr Diploid diploid(BiallelicGenotype bg) {
	std::array dps = {Diploid::homoFirst, Diploid::het, Diploid::homoSecond, Diploid::max, Diploid::max, Diploid::max, Diploid::max};
	return dps[coretools::index(bg)];
}

constexpr Haploid haploid(BiallelicGenotype bg) {
	std::array dps = {Haploid::max, Haploid::max, Haploid::max, Haploid::max, Haploid::first, Haploid::second, Haploid::max};
	return dps[coretools::index(bg)];
}


inline std::string toString(genometools::Ploidy p) {
	constexpr std::array strs = {"haploid", "diploid"};
	return strs[coretools::index(p)];
}

inline std::string toString(genometools::Haploid h) {
	constexpr std::array strs = {"first", "second"};
	return strs[coretools::index(h)];
}

inline std::string toString(genometools::Diploid d) {
	constexpr std::array strs = {"homoFirst", "het", "homoSecond"};
	return strs[coretools::index(d)];
}
} // namespace coretools

#endif
