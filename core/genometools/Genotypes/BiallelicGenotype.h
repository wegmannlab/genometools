#ifndef GENOMETOOLS_GENOTYPES_BIALLELICGENOTYPE_H
#define GENOMETOOLS_GENOTYPES_BIALLELICGENOTYPE_H

#include <array>
#include <string>
#include <cstdint>

#include "coretools/enum.h"
#include "genometools/Genotypes/Base.h"

namespace genometools {
enum class BiallelicGenotype : uint8_t {
	min        = 0,
	homoFirst  = min,
	minDiploid = homoFirst,
	het,
	homoSecond,
	missingDiploid,
	maxDiploid = missingDiploid,
	haploidFirst,
	minHaploid = haploidFirst,
	haploidSecond,
	missingHaploid,
	maxHaploid = missingHaploid,
	max        = missingHaploid
};

constexpr bool isHomozygous(BiallelicGenotype bg) {
	constexpr std::array hom = {true, false, true, false, false, false, false};
	return hom[coretools::index(bg)];
}

constexpr bool isHeterozygous(BiallelicGenotype bg) {
	constexpr std::array het = {false, true, false, false, false, false, false};
	return het[coretools::index(bg)];
}

constexpr bool isHaploid(BiallelicGenotype bg) {
	constexpr std::array hap = {false, false, false, false, true, true, true};
	return hap[coretools::index(bg)];
}

constexpr bool isDiploid(BiallelicGenotype bg) { return !isHaploid(bg); }

constexpr bool isMissing(BiallelicGenotype bg) {
	constexpr std::array mis = {false, false, false, true, false, false, true};
	return mis[coretools::index(bg)];
}
constexpr uint8_t altAlleleCounts(BiallelicGenotype bg) {
	constexpr std::array alt = {0, 1, 2, 0, 0, 1, 0};
	return alt[coretools::index(bg)];
}

constexpr BiallelicGenotype biallelicGenotype(Base a, Base b, Base Ref, bool IsDiploid){
	if (IsDiploid && a == b && a == Ref){
		//diploid homozygous ref (0)
		return BiallelicGenotype::homoFirst;
	} else if (!IsDiploid && a == Ref) {
		//haploid ref
		return BiallelicGenotype::haploidFirst;
	} else if (IsDiploid && a == b && a != Ref){
		//diploid homozygous alt (2)
		return BiallelicGenotype::homoSecond;
	} else if (!IsDiploid && a != Ref) {
		//haploid alt
		return BiallelicGenotype::haploidSecond;
	}
	// heterozygous (1)
	return BiallelicGenotype::het;
}

inline std::string toString(genometools::BiallelicGenotype bg) {
	constexpr std::array strs = {"0/0", "0/1", "1/1", "./.", "0", "1", "."};
	return strs[coretools::index(bg)];
}
}

#endif
