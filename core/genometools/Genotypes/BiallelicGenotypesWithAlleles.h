#ifndef GENOMETOOLS_GENOTYPES_BIALLELICGTWA_H
#define GENOMETOOLS_GENOTYPES_BIALLELICGTWA_H

#include "coretools/Main/TError.h"

#include "genometools/Genotypes/BiallelicGenotype.h"
#include "genometools/Genotypes/Genotype.h"

#include <array>

namespace genometools {
class BiallelicGenotypesWithAlleles {
private:
	std::array<Genotype, 3> _genotypes;

public:
	BiallelicGenotypesWithAlleles(Base First, Base Second) {
		if (First == Second) {
			DEVERROR("First base is equal to the second base -> this is not a biallelic genotype!");
		}
		_genotypes[0] = genotype(First, First);
		_genotypes[1] = genotype(First, Second);
		_genotypes[2] = genotype(Second, Second);
	}

	[[nodiscard]] constexpr Genotype homoFirst() const noexcept { return _genotypes[0]; }
	[[nodiscard]] constexpr Genotype het() const noexcept { return _genotypes[1]; }
	[[nodiscard]] constexpr Genotype homoSecond() const noexcept { return _genotypes[2]; }

	[[nodiscard]] Genotype operator[](const BiallelicGenotype bg) const {
		if (isHaploid(bg)) DEVERROR("Genotype is haploid!");
		if (isMissing(bg))
			DEVERROR("Genotype is missing!");
		else
			return _genotypes[coretools::index(bg)];
	}
};
}

#endif
