#ifndef GENOMETOOLS_GENOTYPES_GENOTYPE_H
#define GENOMETOOLS_GENOTYPES_GENOTYPE_H

#include <array>
#include <string>
#include <cstdint>

#include "coretools/enum.h"

#include "genometools/Genotypes/Base.h"

namespace genometools {
enum class Genotype : uint8_t { min = 0, AA = min, AC, AG, AT, CC, CG, CT, GG, GT, TT, NN, max = NN };

constexpr Genotype genotype(Base a, Base b) noexcept {

	constexpr Genotype gs[5][5] = {{Genotype::AA, Genotype::AC, Genotype::AG, Genotype::AT, Genotype::NN},
								   {Genotype::AC, Genotype::CC, Genotype::CG, Genotype::CT, Genotype::NN},
								   {Genotype::AG, Genotype::CG, Genotype::GG, Genotype::GT, Genotype::NN},
								   {Genotype::AT, Genotype::CT, Genotype::GT, Genotype::TT, Genotype::NN},
	                               {Genotype::NN, Genotype::NN, Genotype::NN, Genotype::NN, Genotype::NN}};

	return gs[coretools::index(a)][coretools::index(b)];
}

constexpr Base first(Genotype g) noexcept {
	constexpr std::array bases = {Base::A, Base::A, Base::A, Base::A, Base::C, Base::C,
	                              Base::C, Base::G, Base::G, Base::T, Base::N};
	return bases[coretools::index(g)];
}

constexpr Base second(Genotype g) noexcept {
	constexpr std::array bases = {Base::A, Base::C, Base::G, Base::T, Base::C, Base::G,
	                              Base::T, Base::G, Base::T, Base::T, Base::N};
	return bases[coretools::index(g)];
}

constexpr bool isHomozygous(Genotype g) {
	constexpr std::array hom = {true, false, false, false, true, false, false, true, false, true, false};
	return hom[coretools::index(g)];
}

constexpr bool isHeterozygous(Genotype g) {
	constexpr std::array het = {false, true, true, true, false, true, true, false, true, false, false};
	return het[coretools::index(g)];
}

inline std::string toString(Genotype g) {
	constexpr std::array strs = {"AA", "AC", "AG", "AT", "CC", "CG", "CT", "GG", "GT", "TT", "NN"};
	return strs[coretools::index(g)];
}

}

#endif
