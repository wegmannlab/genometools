#ifndef GENOTYPECONTAINERS_H_
#define GENOTYPECONTAINERS_H_

#include "coretools/Containers/TMassFunction.h"
#include "coretools/Containers/TStrongArray.h"
#include "coretools/Types/probability.h"

#include "genometools/Genotypes/Base.h"
#include "genometools/Genotypes/Genotype.h"

namespace genometools {

using TBaseProbabilities     = coretools::TStrongMassFunction<coretools::Probability, Base, 4>;
using TBaseLikelihoods       = coretools::TStrongArray<coretools::Probability, Base, 4>;
using TBaseData              = coretools::TStrongArray<double, Base, 4>;
using TBaseCounts            = coretools::TStrongArray<uint32_t, Base, 5>;
using TGenotypeLikelihoods   = coretools::TStrongArray<coretools::Probability, Genotype, 10>;
using TGenotypeProbabilities = coretools::TStrongMassFunction<coretools::Probability, Genotype, 10>;
using TGenotypeData          = coretools::TStrongArray<double, Genotype, 10>;
using TBaseMassFunctions     = coretools::TStrongArray<TBaseProbabilities, Base, 4>;


} // namespace GenotypeLikelihoods

#endif /* TGENOTYPEDATA_H_ */
