#ifndef GENOMETOOLS_GENOTYPES_TWOBASE_H
#define GENOMETOOLS_GENOTYPES_TWOBASE_H

#include "coretools/enum.h"
#include "genometools/Genotypes/Base.h"
#include <array>
#include <cstdint>
#include <string>

namespace genometools {
enum class TwoBase : uint8_t { min = 0,
		AA = min, AC, AG, AT, AN,
		CA, CC, CG, CT, CN,
		GA, GC, GG, GT, GN,
		TA, TC, TG, TT, TN,
		NA, NC, NG, NT, NN, max = NN };

constexpr TwoBase twoBase(Base a, Base b) noexcept {
	constexpr TwoBase gs[5][5] = {{TwoBase::AA, TwoBase::AC, TwoBase::AG, TwoBase::AT, TwoBase::AN},
								  {TwoBase::CA, TwoBase::CC, TwoBase::CG, TwoBase::CT, TwoBase::CN},
								  {TwoBase::GA, TwoBase::GC, TwoBase::GG, TwoBase::GT, TwoBase::GN},
								  {TwoBase::TA, TwoBase::TC, TwoBase::TG, TwoBase::TT, TwoBase::TN},
								  {TwoBase::NA, TwoBase::NC, TwoBase::NG, TwoBase::NT, TwoBase::NN}};

	return gs[coretools::index(a)][coretools::index(b)];
}

constexpr Base first(TwoBase Tb) {
	constexpr std::array bases = {
		Base::A, Base::A, Base::A, Base::A, Base::A,
		Base::C, Base::C, Base::C, Base::C, Base::C,
		Base::G, Base::G, Base::G, Base::G, Base::G,
		Base::T, Base::T, Base::T, Base::T, Base::T,
		Base::N, Base::N, Base::N, Base::N, Base::N,
	};
	return bases[coretools::index(Tb)];
}

constexpr Base second(TwoBase Tb) {
	constexpr std::array bases = {
		Base::A, Base::C, Base::G, Base::T, Base::N,
		Base::A, Base::C, Base::G, Base::T, Base::N,
		Base::A, Base::C, Base::G, Base::T, Base::N,
		Base::A, Base::C, Base::G, Base::T, Base::N,
		Base::A, Base::C, Base::G, Base::T, Base::N,
	};
	return bases[coretools::index(Tb)];
}

inline std::string toString(genometools::TwoBase b) {
	constexpr std::array strs = {"AA", "AC", "AG", "AT", "AN",
		"CA", "CC", "CG", "CT", "CN",
		"GA", "GC", "GG", "GT", "GN",
		"TA", "TC", "TG", "TT", "TN",
		"NA", "NC", "NG", "NT", "NN", "NN"};
	return strs[coretools::index(b)];
}

} // namespace genometools
#endif
