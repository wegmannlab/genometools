#ifndef GENOMETOOLS_GENOTYPES_BASE_H
#define GENOMETOOLS_GENOTYPES_BASE_H

#include "coretools/enum.h"
#include <array>
#include <cstdint>
#include <string>

namespace genometools {
enum class Base : uint8_t { min = 0, A = min, C, G, T, N, max = N };

constexpr Base char2base(char c) noexcept {
	constexpr auto c2b = [](){
		std::array<Base, 'z'> ar{};
		for (auto& a: ar) a = Base::N;

		ar['a'] = Base::A;
		ar['A'] = Base::A;
		ar['c'] = Base::C;
		ar['C'] = Base::C;
		ar['g'] = Base::G;
		ar['G'] = Base::G;
		ar['t'] = Base::T;
		ar['T'] = Base::T;
		return ar;
	}();
	return c2b[c];
}

constexpr char base2char(Base b) noexcept {
	constexpr char bases[] = "ACGTN";
	return bases[coretools::index(b)];
}


constexpr Base flipped(Base b) noexcept {
	constexpr std::array flips = {Base::T, Base::G, Base::C, Base::A, Base::N};
	return flips[coretools::index(b)];
}

constexpr void flip(Base &b) noexcept { b = flipped(b); }

inline std::string toString(genometools::Base b) {
	constexpr std::array strs = {"A", "C", "G", "T", "N"};
	return strs[coretools::index(b)];
}

} // namespace coretools
#endif
