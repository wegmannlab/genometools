#ifndef GENOMETOOLS_GENOTYPES_ALLELICCOMBINATION_H
#define GENOMETOOLS_GENOTYPES_ALLELICCOMBINATION_H

#include <array>
#include <cstdint>

#include "coretools/Main/TError.h"
#include "coretools/enum.h"

#include "genometools/Genotypes/Base.h"
#include "genometools/Genotypes/Genotype.h"

namespace genometools {
enum class AllelicCombination : uint8_t { min = 0, AC = min, AG, AT, CG, CT, GT, NN, max = NN };

constexpr AllelicCombination allelicCombination(Base a, Base b) {
	if (a == Base::N || b == Base::N) return AllelicCombination::NN;
	if (a > b) return allelicCombination(b, a);
	if (a == b) DEVERROR("first == second!");
	if (a == Base::A) { return static_cast<AllelicCombination>(coretools::index(b) - 1); }
	if (a == Base::C) { return static_cast<AllelicCombination>(coretools::index(b) + 1); }
	return AllelicCombination::GT;
}
constexpr Base first(AllelicCombination ac) noexcept {
	constexpr std::array bases = {Base::A, Base::A, Base::A, Base::C, Base::C, Base::G, Base::N};
	return bases[coretools::index(ac)];
}

constexpr Base second(AllelicCombination ac) noexcept {
	constexpr std::array bases = {Base::C, Base::G, Base::T, Base::G, Base::T, Base::T, Base::N};
	return bases[coretools::index(ac)];
}

constexpr Genotype homoFirst(AllelicCombination ac) noexcept {
	const auto f = first(ac);
	return genotype(f, f);
}

constexpr Genotype homoSecond(AllelicCombination ac) noexcept {
	const auto s = second(ac);
	return genotype(s, s);
}

constexpr Genotype het(AllelicCombination ac) noexcept { return genotype(first(ac), second(ac)); }

inline std::string toString(genometools::AllelicCombination ac) {
	constexpr std::array strs = {"A-C", "A-G", "A-T", "C-G", "C-T", "G-T", "N-N"};
	return strs[coretools::index(ac)];
}

}

#endif
