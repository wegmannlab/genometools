/*
 * TGenotypeFreqencies.h
 *
 *  Created on: Apr 30, 2019
 *      Author: wegmannd
 */

#ifndef POPULATIONTOOLS_TGENOTYPEFREQUENCIES_H_
#define POPULATIONTOOLS_TGENOTYPEFREQUENCIES_H_

#include "coretools/Types/probability.h"
#include "coretools/Math/TSumLog.h"
#include "genometools/Genotypes/BiallelicGenotype.h"

namespace coretools {
class TOutputFile;
} // namespace coretools

namespace genometools {

using coretools::Probability;

//------------------------------------------------
// TGenotypeFreqencies
//------------------------------------------------
class TGenotypeFrequencies {
private:
	std::array<Probability, 3> _diploidFrequencies;
	std::array<Probability, 2> _haploidFrequencies;

	Probability _alleleFrequency;
	Probability _MAF;
	uint32_t _numDiploidSamples = 0;
	uint32_t _numHaploidSamples = 0;

	// modify frequencies
	void _ensureAllFrequenciesAreNonZero() noexcept;
	void _capFrequency(Probability &Frequency) const noexcept;
	void _normalizeHaploid() noexcept;
	void _normalizeDiploid() noexcept;

	template<bool hasHaploid, bool hasDiploid, bool hasMissing, typename SampleType>
	void _iterate(const SampleType &Samples, uint32_t NumSamples, double EpsilonF, size_t maxIter) {
		using coretools::P;
		using BG                   = BiallelicGenotype;
		constexpr bool onlyHaploid = hasHaploid && !hasDiploid;
		constexpr bool onlyDiploid = !hasHaploid && hasDiploid;

		const double nHaplo_1 = 1./_numHaploidSamples;
		const double nDiplo_1 = 1./_numDiploidSamples;

		for (size_t _ = 0; _ < maxIter; ++_) {
			// set genofreq
			double hplF0 = 0;
			double dplF0 = 0;
			double dplF2 = 0;

			// estimate new genotype frequencies
			for (uint32_t i = 0; i < NumSamples; i++) {
				if constexpr (hasMissing) {
					if (Samples[i].isMissing()) continue;
				}

				if constexpr (onlyHaploid) {
					const double weights0 = (Probability)Samples[i][BG::haploidFirst] * _haploidFrequencies[0];
					const double weights1 = (Probability)Samples[i][BG::haploidSecond] * _haploidFrequencies[1];
					const double sum      = weights0 + weights1;
					hplF0 += weights0 / sum;
				} else if constexpr (onlyDiploid) {
					const double weights0 = (Probability)Samples[i][BG::homoFirst] * _diploidFrequencies[0];
					const double weights1 = (Probability)Samples[i][BG::het] * _diploidFrequencies[1];
					const double weights2 = (Probability)Samples[i][BG::homoSecond] * _diploidFrequencies[2];
					const double sum      = weights0 + weights1 + weights2;
					dplF0 += weights0 / sum;
					dplF2 += weights2 / sum;
				} else {
					if (Samples[i].isHaploid()) {
						const double weights0 = (Probability)Samples[i][BG::haploidFirst] * _haploidFrequencies[0];
						const double weights1 = (Probability)Samples[i][BG::haploidSecond] * _haploidFrequencies[1];
						const double sum      = weights0 + weights1;
						hplF0 += weights0 / sum;

					} else {
						const double weights0 = (Probability)Samples[i][BG::homoFirst] * _diploidFrequencies[0];
						const double weights1 = (Probability)Samples[i][BG::het] * _diploidFrequencies[1];
						const double weights2 = (Probability)Samples[i][BG::homoSecond] * _diploidFrequencies[2];
						const double sum      = weights0 + weights1 + weights2;
						dplF0 += weights0 / sum;
						dplF2 += weights2 / sum;
					}
				}
			}
			double maxF;
			if constexpr (hasHaploid) {
				hplF0 *= nHaplo_1;
				const auto hplF1 = 1.0 - std::min(1.0, hplF0);

				// check if we stop
				maxF = std::max(fabs(hplF0 - _haploidFrequencies[0]), fabs(hplF1 - _haploidFrequencies[1]));

				_haploidFrequencies[0] = P(hplF0);
				_haploidFrequencies[1] = P(hplF1);
			}
			if constexpr (hasDiploid) {
				dplF0 *= nDiplo_1;
				dplF2 *= nDiplo_1;
				const auto dplF1 = 1.0 - std::min(1.0, (dplF0 + dplF2));
				// 1 - sum ensures range despite numeric inaccuracies

				// check if we stop
				if constexpr (hasHaploid) {
					maxF = std::max({maxF, fabs(dplF0 - _diploidFrequencies[0]), fabs(dplF1 - _diploidFrequencies[1]),
									 fabs(dplF2 - _diploidFrequencies[2])});
				} else {
					maxF = std::max({fabs(dplF0 - _diploidFrequencies[0]), fabs(dplF1 - _diploidFrequencies[1]),
									 fabs(dplF2 - _diploidFrequencies[2])});
				}

				_diploidFrequencies[0] = P(dplF0);
				_diploidFrequencies[1] = P(dplF1);
				_diploidFrequencies[2] = P(dplF2);
			}
			if (maxF < EpsilonF) break;
		}
	}

public:
	TGenotypeFrequencies() = default;
	void clear() noexcept;

	// set, flip
	void set(const TGenotypeFrequencies &other) noexcept;
	void flip() noexcept; // flip major / minor

	// getters
	bool isMonomorphic() const noexcept;
	[[nodiscard]] uint32_t numHaploid() const noexcept;
	[[nodiscard]] uint32_t numDiploid() const noexcept;
	[[nodiscard]] Probability alleleFrequency() const noexcept;
	[[nodiscard]] Probability MAF() const noexcept;
	Probability operator[](BiallelicGenotype Genotype) const noexcept;
	Probability &operator[](BiallelicGenotype Genotype) noexcept;
	[[nodiscard]] const std::array<Probability, 3> &diploidFrequencies() const noexcept;
	[[nodiscard]] const std::array<Probability, 2> &haploidFrequencies() const noexcept;

	// write
	void writeDiploidFrequencies(coretools::TOutputFile &out);
	void writeHaploidFrequencies(coretools::TOutputFile &out);

	// template functions to estimate frequencies
	// we use templates so it works with TSampleLikelihoods (in population tools) and non-standardize likelihoods from
	// GLFs (in major minor)
	template<bool hasMissing, typename SampleType> void guess(const SampleType &Samples, uint32_t NumSamples) noexcept {
		using BG = BiallelicGenotype;
		using coretools::P;
		using coretools::operator""_P;
		// calculate by using MLE genotype for each individual
		clear();

		std::array<uint32_t, 3> diploidCounts{};
		std::array<uint32_t, 2> haploidCounts{};

		for (uint32_t i = 0; i < NumSamples; i++) {
			const auto &s = Samples[i];
			if constexpr (hasMissing) {
				if (s.isMissing()) continue;
			}

			if (s.isHaploid()) {
				const size_t haploIndex = s[BG::haploidSecond].moreProbable(s[BG::haploidFirst]);
				++haploidCounts[haploIndex];
			} else {
				const size_t diploIndex = s[BG::homoSecond].moreProbable(s[BG::homoFirst])
					? 1 + s[BG::homoSecond].moreProbable(s[BG::het])
					: s[BG::het].moreProbable(s[BG::homoFirst]);
				++diploidCounts[diploIndex];
			}
		}

		_numDiploidSamples = diploidCounts[0] + diploidCounts[1] + diploidCounts[2];
		_numHaploidSamples = haploidCounts[0] + haploidCounts[1];

		if (_numDiploidSamples > 0) {
			_diploidFrequencies[0] = P((double)diploidCounts[0] / _numDiploidSamples);
			_diploidFrequencies[1] = P((double)diploidCounts[1] / _numDiploidSamples);
			_diploidFrequencies[2] = P((double)diploidCounts[2] / _numDiploidSamples);
		} else {
			_diploidFrequencies[0] = 0.0_P;
			_diploidFrequencies[1] = 0.0_P;
			_diploidFrequencies[2] = 0.0_P;
		}

		if (_numHaploidSamples > 0) {
			_haploidFrequencies[0] = P((double)haploidCounts[0] / _numHaploidSamples);
			_haploidFrequencies[1] = P((double)haploidCounts[1] / _numHaploidSamples);
		} else {
			_haploidFrequencies[0] = 0.0_P;
			_haploidFrequencies[1] = 0.0_P;
		}
	}

	template<bool hasMissing, typename SampleType>
	void estimate(const SampleType &Samples, uint32_t NumSamples, double EpsilonF) noexcept {
		// estimate initial frequencies from MLEs
		guess<hasMissing>(Samples, NumSamples);
		_ensureAllFrequenciesAreNonZero();

		const bool hasHaploid = _numHaploidSamples > 0;
		const bool hasDiploid = _numDiploidSamples > 0;
		constexpr size_t maxS = 1000;

		if (hasHaploid) {
			if (hasDiploid)
				_iterate<true, true, hasMissing>(Samples, NumSamples, EpsilonF, maxS);
			else
				_iterate<true, false, hasMissing>(Samples, NumSamples, EpsilonF, maxS);
		} else {
			_iterate<false, true, hasMissing>(Samples, NumSamples, EpsilonF, maxS);
			// false false -> only missing samples
		}

		// update parameters
		_alleleFrequency =
		    coretools::P((_numDiploidSamples * (2.0 * (double)_diploidFrequencies[2] + (double)_diploidFrequencies[1]) +
		     _numHaploidSamples * (double)_haploidFrequencies[1]) /
			  (2.0 * _numDiploidSamples + _numHaploidSamples));

		_MAF = _alleleFrequency > 0.5 ? _alleleFrequency.complement() : _alleleFrequency;
	};

	template<typename SampleType>
	coretools::Log10Probability calculateLog10Likelihood(const SampleType &Samples, uint32_t NumSamples) const noexcept {
		using BG  = BiallelicGenotype;
		coretools::TSumLogProbability LL;
		for (uint32_t i = 0; i < NumSamples; i++) {
			if (Samples[i].isMissing()) continue;

			if (Samples[i].isHaploid()) {
				LL.add((double)(Probability)Samples[i][BG::haploidFirst] * _haploidFrequencies[0] +
				            (double)(Probability)Samples[i][BG::haploidSecond] * _haploidFrequencies[1]);
			} else {
				LL.add((double)(Probability)Samples[i][BG::homoFirst] * _diploidFrequencies[0] +
				            (double)(Probability)Samples[i][BG::het] * _diploidFrequencies[1] +
				            (double)(Probability)Samples[i][BG::homoSecond] * _diploidFrequencies[2]);
			}
		}
		constexpr double l10_1 = 0.43429448190325176;
		return coretools::log10P(LL.getSum()*l10_1);
	};
};

}; // namespace genometools

#endif /* POPULATIONTOOLS_TGENOTYPEFREQUENCIES_H_ */
