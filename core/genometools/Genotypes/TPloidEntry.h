#ifndef TPLOIDENTRY_H_
#define TPLOIDENTRY_H_

#include "coretools/Types/probability.h"
#include "genometools/Genotypes/BiallelicGenotype.h"
#include "genometools/Genotypes/Ploidy.h"

#include "coretools/Containers/TDualStrongArray.h"
#include "coretools/enum.h"
#include <cstdint>
#include <type_traits>

namespace genometools {

template<typename Index1, typename Index2>
struct TPloidEntry {
	using DualArray = coretools::TDualStrongArray<coretools::HPPhredInt, Index1, Index2, coretools::index(Index1::max),coretools::index(Index2::max), Ploidy>;

	DualArray values{coretools::HPPhredInt::highest(), Ploidy::haploid};
	uint16_t depth = 0;

	constexpr TPloidEntry() = default;
	constexpr TPloidEntry(const DualArray& Entry, uint16_t Depth) : values(Entry), depth(Depth) {};

	constexpr bool isHaploid() const noexcept { return values.isType(Ploidy::haploid); };

	constexpr coretools::HPPhredInt operator[](Index1 I1) const noexcept {
		return values[I1]; // asserts if wrong index
	};

	constexpr coretools::HPPhredInt operator[](Index2 I2) const noexcept {
		return values[I2]; // asserts if diploid
	};

	constexpr coretools::HPPhredInt operator[](BiallelicGenotype BG) const noexcept {
		static_assert((std::is_same_v<Index1, Haploid> && std::is_same_v<Index2, Diploid>) ||
					  (std::is_same_v<Index1, Diploid> && std::is_same_v<Index2, Haploid>));
		if (genometools::isHaploid(BG)) {
			return values[haploid(BG)];
		} else {
			return values[diploid(BG)];
		}
	};

	constexpr bool isMissing() const noexcept {return depth == 0;}

	constexpr static TPloidEntry empty(bool isHaploid) {
		TPloidEntry entry{};
		entry.values.type = Ploidy(!isHaploid);

		return entry;
	}
};

}

#endif
