#ifndef GENOMETOOLS_GENOTYPES_BASECONTEXT_H
#define GENOMETOOLS_GENOTYPES_BASECONTEXT_H

#include <array>
#include <string>
#include <cstdint>

#include "coretools/enum.h"
#include "genometools/Genotypes/Base.h"

namespace genometools {
enum class BaseContext : uint8_t {
	min = 0,
	AA  = min,
	AC,
	AG,
	AT,
	CA,
	CC,
	CG,
	CT,
	GA,
	GC,
	GG,
	GT,
	TA,
	TC,
	TG,
	TT,
	NA,
	NC,
	NG,
	NT,
	NN,
	max = NN
};

constexpr BaseContext baseContext(Base a, Base b) {
	if (b == Base::N) return BaseContext::NN;
	return static_cast<BaseContext>(4 * coretools::index(a) + coretools::index(b));
}

inline std::string toString(BaseContext bc) {
	constexpr std::array strs = {"AA", "AC", "AG", "AT", "CA", "CC", "CG", "CT", "GA", "GC", "GG",
	                             "GT", "TA", "TC", "TG", "TT", "NA", "NC", "NG", "NT", "NN"};
	return strs[coretools::index(bc)];
}
}

#endif
