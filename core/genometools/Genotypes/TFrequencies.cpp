/*
 * TGenotypeFreqencies.cpp
 *
 *  Created on: Apr 30, 2019
 *      Author: wegmannd
 */

#include "genometools/Genotypes/TFrequencies.h"
#include "coretools/Files/TOutputFile.h"

#include <stdint.h> // for uint32_t
#include <utility>  // for swap

#include "coretools/Types/probability.h"

namespace genometools {
using coretools::P;

//--------------------------
// TGenotypeFreqencies
//--------------------------

void TGenotypeFrequencies::clear() noexcept {
	_numDiploidSamples = 0;
	_numHaploidSamples = 0;
}

void TGenotypeFrequencies::set(const TGenotypeFrequencies &other) noexcept {
	_diploidFrequencies[0] = other._diploidFrequencies[0];
	_diploidFrequencies[1] = other._diploidFrequencies[1];
	_diploidFrequencies[2] = other._diploidFrequencies[2];

	_haploidFrequencies[0] = other._haploidFrequencies[0];
	_haploidFrequencies[1] = other._haploidFrequencies[1];

	_alleleFrequency = other._alleleFrequency;
	_MAF             = other._MAF;

	_numDiploidSamples = other._numDiploidSamples;
	_numHaploidSamples = other._numHaploidSamples;
}

void TGenotypeFrequencies::flip() noexcept {
	// flip major / minor
	std::swap(_haploidFrequencies[0], _haploidFrequencies[1]);
	std::swap(_diploidFrequencies[0], _diploidFrequencies[2]);
	_alleleFrequency = _alleleFrequency.complement();
}

bool TGenotypeFrequencies::isMonomorphic() const noexcept {
	// monomorphic -> allele frequency must be 0.0 or 1.0 -> MAF must be exactly 0.0
	return _MAF == 0.0;
}

uint32_t TGenotypeFrequencies::numHaploid() const noexcept { return _numHaploidSamples; }

uint32_t TGenotypeFrequencies::numDiploid() const noexcept { return _numDiploidSamples; }

Probability TGenotypeFrequencies::alleleFrequency() const noexcept { return _alleleFrequency; }

Probability TGenotypeFrequencies::MAF() const noexcept { return _MAF; }

Probability TGenotypeFrequencies::operator[](BiallelicGenotype Geno) const noexcept {
	// access frequencies
	if (isHaploid(Geno)) {
		return _haploidFrequencies[altAlleleCounts(Geno)]; // 0 or 1
	}
	return _diploidFrequencies[altAlleleCounts(Geno)]; // 0 or 1 or 2
}

Probability &TGenotypeFrequencies::operator[](BiallelicGenotype Geno) noexcept {
	// access frequencies
	if (isHaploid(Geno)) {
		return _haploidFrequencies[altAlleleCounts(Geno)]; // 0 or 1
	}
	return _diploidFrequencies[altAlleleCounts(Geno)]; // 0 or 1 or 2
}

const std::array<Probability, 3> &TGenotypeFrequencies::diploidFrequencies() const noexcept {
	return _diploidFrequencies;
}

const std::array<Probability, 2> &TGenotypeFrequencies::haploidFrequencies() const noexcept {
	return _haploidFrequencies;
}

void TGenotypeFrequencies::_capFrequency(Probability &Frequency) const noexcept {
	if (Frequency < 0.000001) { Frequency = P(0.000001); }
}

void TGenotypeFrequencies::_normalizeHaploid() noexcept {
	const double sum = _haploidFrequencies[0] + _haploidFrequencies[1];
	_haploidFrequencies[0].scale(sum);
	_haploidFrequencies[1].scale(sum);
}

void TGenotypeFrequencies::_normalizeDiploid() noexcept {
	const double sum = _diploidFrequencies[0] + _diploidFrequencies[1] + _diploidFrequencies[2];
	_diploidFrequencies[0].scale(sum);
	_diploidFrequencies[1].scale(sum);
	_diploidFrequencies[2].scale(sum);
}

void TGenotypeFrequencies::_ensureAllFrequenciesAreNonZero() noexcept {
	// diploid
	if (_numDiploidSamples > 0) {
		_capFrequency(_diploidFrequencies[0]);
		_capFrequency(_diploidFrequencies[1]);
		_capFrequency(_diploidFrequencies[2]);

		// renormalize
		_normalizeDiploid();
	}

	// haploid
	if (_numHaploidSamples > 0) {
		_capFrequency(_haploidFrequencies[0]);
		_capFrequency(_haploidFrequencies[1]);

		// renormalize
		_normalizeHaploid();
	}
}

void TGenotypeFrequencies::writeDiploidFrequencies(coretools::TOutputFile &out) {
	if (_numDiploidSamples > 0) {
		out.write(_diploidFrequencies[0], _diploidFrequencies[1], _diploidFrequencies[2]);;
	} else {
		out.write('-', '-', '-');
	}
};

void TGenotypeFrequencies::writeHaploidFrequencies(coretools::TOutputFile &out) {
	if (_numHaploidSamples > 0) {
		out.writeln(_haploidFrequencies[0], _haploidFrequencies[1]);
	} else {
		out.write('-', '-');
	}
};

}; // namespace genometools
