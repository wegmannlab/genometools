/*
 * THardyWeinbergGenotypeProbabilities.h
 *
 *  Created on: Jul 2, 2021
 *      Author: wegmannd
 */

#ifndef GENOMETOOLS_CORE_THARDYWEINBERGGENOTYPEPROBABILITIES_H_
#define GENOMETOOLS_CORE_THARDYWEINBERGGENOTYPEPROBABILITIES_H_

#include "coretools/Types/probability.h"
#include "coretools/enum.h"
#include "genometools/Genotypes/BiallelicGenotype.h"

namespace genometools {

//------------------------------------------------
// THardyWeinbergGenotypeProbabilities
//------------------------------------------------
class THardyWeinbergGenotypeProbabilities {
protected:
	// store one probability per BiallelicGenotype type for fast lookup
	// these are     homoFirst, het,       homoSecond, missingDiploid, haploidFirst, haploidSecond, missingHaploid
	// corresponding (1-f)^2,   2*f*(1-f), f^2,        1.0,            (1-f),        f,             1.0
	// Note: 1.0 for missing genotypes as P(g=?|f) = 1.0
	std::array<coretools::Probability, 7> _probabilities{coretools::P(1.)};

public:
	THardyWeinbergGenotypeProbabilities(const coretools::Probability F = coretools::P(0.5)) noexcept { set(F); }

	constexpr void set(const coretools::Probability f) noexcept {
		using BG                                            = BiallelicGenotype;
		_probabilities[coretools::index(BG::haploidFirst)]  = f.complement();
		_probabilities[coretools::index(BG::haploidSecond)] = f;

		_probabilities[coretools::index(BG::homoFirst)] =
		    _probabilities[coretools::index(BG::haploidFirst)] * _probabilities[coretools::index(BG::haploidFirst)];
		_probabilities[coretools::index(BG::homoSecond)] =
		    _probabilities[coretools::index(BG::haploidSecond)] * _probabilities[coretools::index(BG::haploidSecond)];
		_probabilities[coretools::index(BG::het)] =
		    coretools::P(1.0 - (_probabilities[coretools::index(BG::homoFirst)] + _probabilities[coretools::index(BG::homoSecond)]));
	}

	constexpr coretools::Probability f() const noexcept {
		return _probabilities[coretools::index(BiallelicGenotype::haploidSecond)];
	}
	constexpr coretools::Probability operator[](const BiallelicGenotype g) const noexcept {
		return _probabilities[coretools::index(g)];
	}
};

//---------------------------------------------------
// THardyWeinbergWithInbreedingGenotypeProbabilities
// Calculates HW probabilities in the presence of inbreeding
//---------------------------------------------------

class THardyWeinbergWithInbreedingGenotypeProbabilities : public THardyWeinbergGenotypeProbabilities {
public:
	THardyWeinbergWithInbreedingGenotypeProbabilities(const coretools::Probability p = coretools::P(0.5),
	                                                  const coretools::Probability F = coretools::P(0.0)) noexcept {
		set(p, F);
	}

	constexpr void set(const coretools::Probability p, const coretools::Probability F) noexcept {
		using coretools::P;
		using BG = BiallelicGenotype;

		_probabilities[coretools::index(BG::haploidFirst)]  = p.complement(); // (1-p)
		_probabilities[coretools::index(BG::haploidSecond)] = p;              // p
		_probabilities[coretools::index(BG::homoFirst)] =
		    P(F.complement() * _probabilities[coretools::index(BG::haploidFirst)] *
		        _probabilities[coretools::index(BG::haploidFirst)] +
			  F * _probabilities[coretools::index(BG::haploidFirst)]); // (1-F)*(1-p)^2 + F(1-p)
		_probabilities[coretools::index(BG::het)] = P(F.complement() * 2.0 *
		                                            _probabilities[coretools::index(BG::haploidFirst)] *
													  _probabilities[coretools::index(BG::haploidSecond)]); // (1-F)2p(1-p)
		_probabilities[coretools::index(BG::homoSecond)] =
		    P(1.0 - _probabilities[coretools::index(BG::homoFirst)] - _probabilities[coretools::index(BG::het)]);
	}
};

} // end namespace genometools

#endif /* GENOMETOOLS_CORE_THARDYWEINBERGGENOTYPEPROBABILITIES_H_ */
