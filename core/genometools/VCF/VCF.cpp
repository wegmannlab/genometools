#include "VCF.h"

#include "coretools/Main/TRandomGenerator.h"

namespace genometools {

using coretools::P;
using coretools::instances::randomGenerator;

namespace impl {
size_t simulateNumReads(BiallelicGenotype genotype, size_t Depth) {
	constexpr coretools::Probability error{0.05};
	if ((isDiploid(genotype) && genotype == BiallelicGenotype::homoFirst) ||
	    (isHaploid(genotype) && genotype == BiallelicGenotype::haploidFirst)) {
		// homozygous reference (haploid and diploid)
		return randomGenerator().getBinomialRand(error.complement(), Depth);
	} else if ((isDiploid(genotype) && genotype == BiallelicGenotype::homoSecond) ||
	           (isHaploid(genotype) && genotype == BiallelicGenotype::haploidSecond)) {
		// homozygous alternative (haploid and diploid)
		return randomGenerator().getBinomialRand(error, Depth);
	}
	// heterozygous
	return randomGenerator().getBinomialRand(P(0.5), Depth);
}

} // namespace impl

std::vector<TVCFEntry> fill(coretools::TConstView<TGLFEntry> samples, AllelicCombination ac) {
	using coretools::HPPhredInt;
	using coretools::TStrongArray;
	std::vector<TVCFEntry> storage;
	storage.reserve(samples.size());
	for (const auto &s : samples) {
		if (!s.depth) { continue; }
		if (s.isHaploid()) {
			const TStrongArray<HPPhredInt, Haploid> a({s[first(ac)], s[second(ac)]});
			storage.emplace_back(a, s.depth);
		} else {
			const TStrongArray<HPPhredInt, Diploid> a({s[homoFirst(ac)], s[het(ac)], s[homoSecond(ac)]});
			storage.emplace_back(a, s.depth);
		}
	}
	return storage;
}

TVCFEntry calculateGenotypeLikelihoods(size_t NumRef, size_t NumAlt, bool IsDiploid) {
	using coretools::HPPhredInt;
	using coretools::LogProbability;
	constexpr double logError      = -2.995732273553991;
	constexpr double logErrorCompl = -0.05129329438755058;
	const uint16_t depth           = NumRef + NumAlt;
	const auto P0                  = HPPhredInt(LogProbability(NumRef * logErrorCompl + NumAlt * logError));
	const auto P2                  = HPPhredInt(LogProbability(NumRef * logError + NumAlt * logErrorCompl));
	if (IsDiploid) {
		const auto P1 = HPPhredInt(LogProbability((NumRef + NumAlt) * lnOneHalf));
		const coretools::TStrongArray<HPPhredInt, Diploid> a({P0, P1, P2});
		return {a, depth};
	} else {
		const coretools::TStrongArray<HPPhredInt, Haploid> a({P0, P2});
		return {a, depth};
	}
}

TVCFEntry simulate(BiallelicGenotype genotype, size_t avgDepth) {
	// simulate depth
	const auto depth = randomGenerator().getPoissonRandom(avgDepth);

	// simulate number of reference and alternative alleles
	const auto numRef = impl::simulateNumReads(genotype, depth);
	const auto numAlt = (int)depth - numRef;

	// get genotype likelihoods
	return calculateGenotypeLikelihoods(numRef, numAlt, isDiploid(genotype));
}

std::pair<Base, Base> findMajorMinorAllele(coretools::TStrongArray<size_t, Base, 4> AlleleCounts, Base RefAllele) {
	// major allele = allele with the highest counts
	const auto majorAllele =
	    randomGenerator()
	        .sampleIndexOfMaxima<coretools::TStrongArray<size_t, Base, 4>, Base, coretools::index(Base::max)>(
	            AlleleCounts);

	if (majorAllele == RefAllele) {
		// choose minor allele: can be any allele (except major)
		// note: multiple alleles might have same counts
		std::array<Base, 4> minorAlleles;
		size_t minorAllelesSize = 0;
		size_t secondMax        = 0;
		for (auto b = Base::min; b < Base::max; ++b) {
			if (b == majorAllele) { continue; } // exclude major allele
			if (AlleleCounts[b] > secondMax) {  // found new max
				secondMax            = AlleleCounts[b];
				minorAllelesSize     = 1;
				minorAlleles.front() = b;
			} else if (AlleleCounts[b] == secondMax) { // found allele that is equally good
				minorAlleles[minorAllelesSize] = b;
				++minorAllelesSize;
			}
		}
		// randomly sample minor allele, if there are multiple
		auto minorAllele = minorAlleles[randomGenerator().sample(minorAllelesSize)];
		return std::make_pair(majorAllele, minorAllele);
	} else {
		// major allele is not refAllele -> minor allele must be ref-allele!
		// quick check if this is valid (should always be true for bi-allelic simulators)
		for (auto b = Base::min; b < Base::max; ++b) {
			if (b != majorAllele && b != RefAllele && AlleleCounts[b] > AlleleCounts[RefAllele]) {
				DEVERROR(": reference allele (", RefAllele, ") is not major (", majorAllele, ") nor minor (",
				         AlleleCounts[b], ") allele!");
			}
		}
		return std::make_pair(majorAllele, RefAllele);
	}
}

} // namespace genometools::VCF
