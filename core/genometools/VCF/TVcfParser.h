/*
 * TVcfParser.h
 *
 *  Created on: Jun 15, 2011
 *      Author: wegmannd
 */

#ifndef TVCFPARSER_H_
#define TVCFPARSER_H_

#include <map>
#include <vector>
#include <string>

#include "coretools/Strings/fillContainer.h"
#include "coretools/Strings/stringProperties.h"
#include "coretools/Types/probability.h"
#include "genometools/Genotypes/BiallelicGenotype.h"

namespace genometools {

// TODO: use header info to check entries

enum class VCF_TYPE { UNKNOWN, INTEGER, FLOAT, FLAG, CHAR, STRING };

class TVcfColumnNumbers {
public:
	int Chr, Pos, Id, Ref, Alt, Qual, Filter, Info, Format, FirstInd;
	TVcfColumnNumbers() {
		FirstInd = 999999;
		Chr      = -1;
		Pos      = -1;
		Id       = -1;
		Ref      = -1;
		Alt      = -1;
		Qual     = -1;
		Filter   = -1;
		Info     = -1;
		Format   = -1;
	};

	void set(std::string_view tag, int i) {
		using coretools::str::stringContains;
		using coretools::str::toString;
		if (stringContains(tag, "CHROM"))
			Chr = i;
		else if (stringContains(tag, "POS"))
			Pos = i;
		else if (stringContains(tag, "ID"))
			Id = i;
		else if (stringContains(tag, "REF"))
			Ref = i;
		else if (stringContains(tag, "ALT"))
			Alt = i;
		else if (stringContains(tag, "QUAL"))
			Qual = i;
		else if (stringContains(tag, "FILTER"))
			Filter = i;
		else if (stringContains(tag, "INFO"))
			Info = i;
		else if (stringContains(tag, "FORMAT")) {
			Format   = i;
			// next is first individual!
			FirstInd = i + 1;
		}
	};
	void check() const {
		if (Chr < 0) UERROR("Error when reading vcf header: column 'CHROM' is missing!");
		if (Pos < 0) UERROR("Error when reading vcf header: column 'POS' is missing!");
		if (Id < 0) UERROR("Error when reading vcf header: column 'ID' is missing!");
		if (Ref < 0) UERROR("Error when reading vcf header: column 'REF' is missing!");
		if (Alt < 0) UERROR("Error when reading vcf header: column 'ALT' is missing!");
		if (Qual < 0) UERROR("Error when reading vcf header: column 'QUAL' is missing!");
		if (Filter < 0) UERROR("Error when reading vcf header: column 'FILTER' is missing!");
		if (Info < 0) UERROR("Error when reading vcf header: column 'INFO' is missing!");
		if (Format < 0) UERROR("Error when reading vcf header: column 'FORMAT' is missing!");
	};
};
//---------------------------------------------------------------------------------------------------------
class TVcfHeaderLine {
public:
	std::string _id;
	int number = -1;
	std::string numberString;
	VCF_TYPE type=VCF_TYPE::UNKNOWN;
	std::string typeString;
	std::string desc;

	TVcfHeaderLine() = default;
	TVcfHeaderLine(std::string_view Line);
	TVcfHeaderLine(std::string_view ID, std::string_view Number, VCF_TYPE Type, std::string_view Desc);
	void update(std::string_view Number, VCF_TYPE Type, std::string_view Desc);
	std::string getString() const;
};
//---------------------------------------------------------------------------------------------------------
class TVcfSample {
private:
public:
	std::vector<std::string> data;
	std::pair<int, int> genotype;
	bool missing         = true;
	bool hasGenotype     = false;
	bool unknownGenotype = false;
	bool isHaploid       = false;

	void readData(std::string_view s) {coretools::str::fillContainerFromString(s, data, ':');};
	void addData(std::string_view d) { data.emplace_back(d); };
	void updateData(int pos, std::string_view d) { data.at(pos) = d; };
	void setGenotype(int firstAllele, int secondAllele);
	void setGenotype(int haploidAllele);
	void setMissingGenotype();
	bool parse(std::string_view s, int genotypeCol);
	bool checkGenotype(int max) const;
	const std::string& getCol(int col) const { return data[col]; };
	void write(std::ostream &out, unsigned int numFields) const;
};
//---------------------------------------------------------------------------------------------------------

class TVcfLine {
public:
	long lineNumber = -1;
	std::vector<std::string> data; // used to store read data
	bool positionParsed = false;
	bool variantParsed = false;
	bool idParsed = false;
	bool filterParsed = false;
	bool qualityParsed = false;
	bool infoParsed = false;
	bool formatParsed = false;
	bool samplesParsed = false;

	uint64_t pos = -1;
	std::string chr;
	double variantQuality = -1;
	bool variantQualityMissing = true;

	std::vector<std::string> variants; // entry at 0 is reference
	std::map<std::string, std::vector<std::string>, std::less<>> info;
	std::vector<std::string> formatOrdered;
	std::map<std::string, int, std::less<>> format;
	std::vector<TVcfSample> samples;

	std::string id, qual, filter;

	// ID, FILTER and QUAL: these fields are currently NOT PARSED -> TODO
	std::vector<char> bases; // entry at 0 is ref

	TVcfLine() = default;
	TVcfLine(std::string_view line, unsigned int numCols, long LineNumber);
	void update(std::string_view line, unsigned int numCols, long LineNumber);
	bool variantExists(std::string_view var) const;
	void addVariant(std::string_view var);
	void writeVariant(std::ostream &out) const;


};

class TVcfParser {
private:
	static coretools::PhredInt getPhredScore(std::string_view phredString) {
		if (phredString == "inf")
			return coretools::PhredInt::lowest();
		else { return coretools::PhredInt(phredString); }
	};

public:
	TVcfColumnNumbers cols;
	std::map<std::string, TVcfHeaderLine, std::less<>> info;
	std::map<std::string, TVcfHeaderLine, std::less<>> format;
	std::vector<std::string> samples;

	// parsers
	void parsePosition(TVcfLine &line);
	void parseVariant(TVcfLine &line);
	void parseQuality(TVcfLine &line);
	void parseFormat(TVcfLine &line);
	void parseInfo(TVcfLine &line);
	void parseSamples(TVcfLine &line);

	// other functions
	int getFormatCol(std::string_view tag, const TVcfLine &line) const;
	int getFormatCol(const TVcfLine &line, std::string_view tag) const { return getFormatCol(tag, line); };
	bool formatColExists(std::string_view tag, const TVcfLine &line) const;
	int addFormatCol(std::string_view tag, TVcfLine &line) const;
	//	void setColNumbers(TVcfColumnNumbers* Cols){cols=Cols;};
	void addInfo(std::string_view Line);
	void updateInfo(std::string_view ID, std::string_view Number, VCF_TYPE Type, std::string_view Desc);
	void addFormat(std::string_view Line);
	void addSample(std::string_view Name);
	void updateInfo(TVcfLine &line, std::string_view Id, std::string_view Data) const;
	void updatePL(TVcfLine &tempLine, std::string_view Data, unsigned int sample) const;
	void addToInfo(TVcfLine &line, std::string_view Id, std::string_view Data) const;
	int getSampleNum(std::string_view Name) const;
	std::string getSampleName(unsigned int sample) const;
	int getNumSamples() const;
	bool checkSampleNum(const TVcfLine &line, unsigned int sample) const;

	// get variant info
	const std::string& getChr(const TVcfLine &line) const;
	uint64_t getPos(const TVcfLine &line) const;
	int getNumAlleles(const TVcfLine &line) const;
	const std::string& getRefAllele(const TVcfLine &line) const;
	const std::string& getFirstAltAllele(const TVcfLine &line) const;
	const std::string& getAllele(const TVcfLine &line, int num) const;
	bool variantQualityIsMissing(const TVcfLine &line) const;
	double variantQuality(const TVcfLine &line) const;

	// modify samples
	void addInfoToSample(TVcfLine &line, unsigned int sample, std::string_view tag, std::string_view Data) const ;
	void setSampleMissing(TVcfLine &line, unsigned int sample) const;
	void setSampleHasUndefinedGenotype(TVcfLine &line, unsigned int sample) const;
	void updateField(TVcfLine &line, std::string_view tag, std::string_view Data, unsigned int sample) const;

	// retrieve sample info
	bool sampleIsHaploid(const TVcfLine &line, unsigned int sample) const;
	bool sampleIsDiploid(const TVcfLine &line, unsigned int sample) const;
	bool sampleIsHomoRef(const TVcfLine &line, unsigned int sample) const;
	bool sampleIsHeteroRefNonref(const TVcfLine &line, unsigned int sample) const;
	const std::string& getFirstAlleleOfSample(const TVcfLine &line, unsigned int sample) const;
	const std::string& getSecondAlleleOfSample(const TVcfLine &line, unsigned int sample) const;
	BiallelicGenotype sampleBiallelicGenotype(const TVcfLine &line, unsigned int sample) const;
	bool sampleIsMissing(const TVcfLine &line, unsigned int sample) const;
	bool sampleHasUndefinedGenotype(const TVcfLine &line, unsigned int s) const;
	float sampleGenotypeQuality(const TVcfLine &line, unsigned int sample) const;

	// fill genotype likelihoods
	template<typename Type> void setMissing(Type &SampleLikelihoods, const TVcfLine &line, unsigned int s) const {
		if (line.samples[s].isHaploid) {
			SampleLikelihoods.setMissingHaploid();
		} else {
			SampleLikelihoods.setMissingDiploid();
		}
	};

	template<typename Type> void fillGenotypeLikelihoods(Type &SampleLikelihoods, const TVcfLine &line, unsigned int s) const {
		typedef decltype(std::declval<Type>().operator[](BiallelicGenotype::haploidFirst)) TypePhred;
		if (s >= line.samples.size()) UERROR("Sample ", s, " does not exists!");
		if (line.samples[s].missing) {
			setMissing(SampleLikelihoods, line, s);
		} else {
			const auto cPL = getFormatCol(line, "PL");
			if (cPL < 0) {
				const int cGL = getFormatCol(line, "GL");
				if (cGL < 0) {
					// neither PL nor GL tag: set missing (i.e. to highest)
					setMissing(SampleLikelihoods, line, s);
				} else {
					// GL field exists
					std::vector<std::string> phreddie;
					coretools::str::fillContainerFromString(line.samples[s].data[cGL], phreddie, ',');

					// diploid or haploid?
					if (line.samples[s].isHaploid) { // haploid: only two are given
						SampleLikelihoods.setHaploid(TypePhred(coretools::Log10Probability(phreddie[0])),
						                             TypePhred(coretools::Log10Probability(phreddie[1])));
					} else { // diploid
						SampleLikelihoods.setDiploid(TypePhred(coretools::Log10Probability(phreddie[0])),
						                             TypePhred(coretools::Log10Probability(phreddie[1])),
						                             TypePhred(coretools::Log10Probability(phreddie[2])));
					}
				}
			} else {
				// PL field exists
				std::vector<std::string> phreddie;
				coretools::str::fillContainerFromString(line.samples[s].data[cPL], phreddie, ',');

				// diploid or haploid?
				if (line.samples[s].isHaploid) { // haploid: only two are given
					SampleLikelihoods.setHaploid(TypePhred(getPhredScore(phreddie[0])),
					                             TypePhred(getPhredScore(phreddie[1])));
				} else { // diploid
					SampleLikelihoods.setDiploid(TypePhred(getPhredScore(phreddie[0])),
					                             TypePhred(getPhredScore(phreddie[1])),
					                             TypePhred(getPhredScore(phreddie[2])));
				}
			}
		}
	};

	const std::string & sampleContentAt(const TVcfLine &line, std::string_view tag, unsigned int sample) const;
	std::string sampleContentAtNoCheckForMissingSample(const TVcfLine &line, std::string_view tag, unsigned int sample) const;
	static int phred(double x) noexcept { return (double)-10.0 * log10(x); };
	static float dePhred(double x) {
		if (x < 0.) { UERROR("Phred quality scores should not be negative! Please check your vcf-file."); }
		return pow(10.0, -x / 10.0);
	};

	// output
	void writeColumnDescriptionHeader(std::ostream &out) const;
	void writeInfoHeader(std::ostream &out) const;
	void writeFormatHeader(std::ostream &out) const;
	void writeLine(const TVcfLine &line, std::ostream &out) const;
};

}; // end namespace genometools

#endif /* TVCFPARSER_H_ */
