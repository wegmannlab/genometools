/*
 * TPopulationLikelihoodStorage.h
 *
 *  Created on: Apr 30, 2019
 *      Author: wegmannd
 */

#ifndef POPULATIONTOOLS_TPOPULATIONLIKELIHOODLOCUS_H_
#define POPULATIONTOOLS_TPOPULATIONLIKELIHOODLOCUS_H_

#include <algorithm>
#include <vector>


namespace genometools {

//------------------------------------------------
// TPopulationLikehoodLocus
// class used when reading line by line
//------------------------------------------------

template<typename Type> class TPopulationLikehoodLocus {
private:
	std::vector<Type> _samples;
public:
	TPopulationLikehoodLocus() = default;

	TPopulationLikehoodLocus(size_t NumSamples) {
		_samples.resize(NumSamples);
	};

	void resize(size_t NumSamples) {
		_samples.resize(NumSamples);
	};

	Type &operator[](size_t index) noexcept { return _samples[index]; };
	const Type &operator[](size_t index) const noexcept { return _samples[index]; };

	Type *samples() noexcept { return _samples.data(); };

	size_t numSamples() const noexcept { return _samples.size(); };

	size_t numSamplesWithData() const {
		return std::count_if(_samples.begin(), _samples.end(), [](const auto &s){return !s.isMissing();});
	};

	bool hasData() const {
		return std::find_if(_samples.begin(), _samples.end(), [](const auto &s){return !s.isMissing();}) != _samples.end();
	};

	void fillAsMissingHaploid() {
		for (auto& s: _samples) s.setMissingHaploid();
	};

	void fillAsMissingDiploid() {
		for (auto& s: _samples) s.setMissingDiploid();
	};

	void print(size_t index) const { _samples[index].print(); };
};

//------------------------------------------------
// TPopulationLikehoodRegion
// class used when reading line by line
//------------------------------------------------
template<typename Type> class TPopulationLikehoodWindow {
private:
	using Locus = TPopulationLikehoodLocus<Type>;
	std::vector<Locus> _loci; // allows simple access to subsets based on populations
	size_t _numSamples = 0;

public:
	TPopulationLikehoodWindow() = default;

	TPopulationLikehoodWindow(size_t NumLoci, size_t NumSamples) {
		resize(NumLoci, NumSamples);
	};

	TPopulationLikehoodWindow(size_t NumLoci) {
		resize(NumLoci);
	};

	void clear() {
		_loci.clear();
		_numSamples  = 0;
	};

	void resize(size_t NumLoci) {
		_loci.resize(NumLoci, Locus(_numSamples));
	};

	void resize(size_t NumLoci, size_t NumSamples) {
		if (NumSamples == _numSamples) resize(NumLoci);
		else {
			_numSamples = NumSamples;
			_loci.resize(NumLoci, Locus(_numSamples));
			for (auto &l : _loci) l.resize(NumSamples);
		}
	};

	const TPopulationLikehoodLocus<Type> &operator[](size_t locus) const noexcept { return _loci[locus]; };
	TPopulationLikehoodLocus<Type> &operator[](size_t locus) noexcept { return _loci[locus]; };

	size_t numSamples() const { return _numSamples; };

	size_t numLoci() const { return _loci.size(); };



	size_t numLociwithData() const {
		return std::count_if(_loci.begin(), _loci.end(), [](const auto &l){return l.hasData();});
	};

	bool hasData() const {
		return std::find_if(_loci.begin(), _loci.end(), [](const auto &l){return l.hasData();}) != _loci.end();
	};

	void fillAsMissingHaploid() {
		for (auto& l: _loci) l.fillAsMissingHaploid();
	};

	void fillAsMissingDiploid() {
		for (auto& l: _loci) l.fillAsMissingDiploid();
	};

	bool individualHasMissingData(size_t individual) const {
		return std::find_if(_loci.begin(), _loci.end(), [individual](const auto &l){return l[individual].isMissing();}) != _loci.end();
	};
};

}; // end namespace genometools

#endif /* POPULATIONTOOLS_TPOPULATIONLIKELIHOODLOCUS_H_ */
