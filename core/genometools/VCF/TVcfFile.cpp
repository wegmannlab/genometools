/*
 * TVcfFile.cpp
 *
 *  Created on: Aug 8, 2011
 *      Author: wegmannd
 */

#include "genometools/VCF/TVcfFile.h"
#include "coretools/Files/gzstream.h"

namespace genometools {

//--------------------------------------------------------------------

void TVcfFile_base::openStream(std::string_view filename) {
	if (coretools::str::readAfterLast(filename, '.') == "gz") {
		openStream(filename, true);
	} else {
		openStream(filename, false);
	}
};

void TVcfFile_base::openStream(std::string_view Filename, bool zipped) {
	// open stream
	filename = Filename;
	if (zipped)
		myStream = new gz::igzstream(filename.c_str());
	else
		myStream = new std::ifstream(filename.c_str());
	if (!(*myStream) || myStream->fail() || !myStream->good()) UERROR("Failed to open file '", filename, "'!");
	inputStreamOpend = true;

	// first line contains fileformat version
	std::string temp;
	getline(*myStream, temp);
	currentLine = 1;

	if (coretools::str::stringContains(temp, "##fileformat")) {
		fileFormat = coretools::str::extractAfter(temp, '=');
		coretools::str::trimString(fileFormat);
		if (fileFormat != "VCFv4.0" && fileFormat != "VCFv4.1" && fileFormat != "VCFv4.2" && fileFormat != "VCFv4.3")
			UERROR("VCF file is not in 'VCFv4.0' format!");
	} else
		UERROR("Missing VCF file format specification on first line! Is '", Filename, "' a VCF file?");

	// parse header
	parseHeaderVCF_4_0();
};

void TVcfFile_base::openOutputStream(std::string_view Filename, bool zipped) {
	// open stream
	outputFilename = Filename;
	if (zipped)
		myOutStream = new gz::ogzstream(outputFilename.c_str());
	else
		myOutStream = new std::ofstream(outputFilename.c_str());
	if (!(*myOutStream)) UERROR("Failed to open file '", outputFilename, "'!");
	outputStreamOpend = true;
};

void TVcfFile_base::setOutStream(std::ostream &os) { myOutStream = &os; }

void TVcfFile_base::parseHeaderVCF_4_0() {
	// read the header of the vcf file and stop after that
	std::string temp, buf;
	bool headerRowRead = false;
	while (!myStream->eof() && myStream->peek() == '#') {
		++currentLine;
		getline(*myStream, temp);
		if (coretools::str::stringContains(temp, "#CHROM")) {
			if (headerRowRead) UERROR("Found more than one header row!");
			// analyze header: save which column contains the chromosome, position, refbase, altbases, info, format and
			// species
			coretools::str::trimString(temp);
			int i = 0;
			while (!temp.empty()) {
				buf = coretools::str::extractBeforeWhiteSpace(temp);
				coretools::str::trimString(buf);
				temp.erase(0, 1);
				if (i < parser.cols.FirstInd)
					parser.cols.set(buf, i);
				else
					parser.addSample(buf);
				++i;
			}
			numCols       = i;
			headerRowRead = true;
		} else {
			// parse all other header lines
			if (temp.find("##FORMAT") == 0)
				parser.addFormat(temp);
			else if (temp.find("##INFO") == 0)
				parser.addInfo(temp);
			else
				unknownHeader.push_back(temp);
		}
	}
	// check cols
	parser.cols.check();
	if (parser.samples.size() < 1) UERROR("VCF file contains no samples!");
	currentLine = 0;
}

void TVcfFile_base::addNewHeaderLine(std::string headerLine) { unknownHeader.push_back(std::move(headerLine)); }

/*
void TVcfFile::addFilter(my_string filter){
    filters.push_back(TVcfFilter(filter, &currentLine));
    applyFilters=true;
}

void TVcfFile::filterSamples(){
    if(!samplesParsed) throw TException("Unable to filter samples: samples not parsed yet!");
    if(applyFilters){
        for(vector<TVcfFilter>::iterator itF=filters.begin(); itF!=filters.end(); ++itF){
            for(vector<TVcfSample>::iterator it=samples.begin(); it!=samples.end(); ++it)
                it->filter(&(*itF));
        }
    }
}

void TVcfFile::printFilters(){
    if(applyFilters){
        cout << " - Using the following filters:" << endl;
        for(vector<TVcfFilter>::iterator itF=filters.begin(); itF!=filters.end(); ++itF)
            itF->print();
    }
}
*/

int TVcfFile_base::fieldContentAsInt(std::string_view tag, const TVcfLine *line, unsigned int sample) const {
	return coretools::str::fromString<int>(parser.sampleContentAt(*line, tag, sample));
}

int TVcfFile_base::depthAsIntNoCheckForMissingSample(std::string_view tag, const TVcfLine *line, unsigned int sample) const {
	return coretools::str::fromString<int>(parser.sampleContentAtNoCheckForMissingSample(*line, tag, sample));
}

void TVcfFile_base::setSampleMissing(TVcfLine *line, unsigned int sample) const { parser.setSampleMissing(*line, sample); }

void TVcfFile_base::setSampleHasUndefinedGenotype(TVcfLine *line, unsigned int sample) const {
	parser.setSampleHasUndefinedGenotype(*line, sample);
}

void TVcfFile_base::updateField(TVcfLine *line, std::string_view tag, std::string_view Data, unsigned int sample) const {
	parser.updateField(*line, tag, Data, sample);
};

void TVcfFile_base::writeHeaderVCF_4_0() const {
	*myOutStream << "##fileformat=" << fileFormat << std::endl;
	parser.writeFormatHeader(*myOutStream);
	parser.writeInfoHeader(*myOutStream);
	// write unknown header columns
	for (auto i = unknownHeader.begin(); i != unknownHeader.end(); ++i) {
		*myOutStream << *i << std::endl;
	}
	// add column description
	parser.writeColumnDescriptionHeader(*myOutStream);
}

bool TVcfFile_base::readLine() {
	std::string temp;
	do {
		if (myStream->eof()) {
			eof = true;
			return false;
		}
		++currentLine;
		getline(*myStream, temp);
	} while (temp.empty());
	tempLine.update(temp, numCols, currentLine);

	// what to parse?
	for (auto it = usedParsers.begin(); it != usedParsers.end(); ++it) {
		(parser.*(*it))(tempLine);
	}
	return true;
}

//--------------------------------------------------------------------
TVcfFileSingleLine::TVcfFileSingleLine(std::string_view filename, bool zipped) {
	openStream(filename, zipped);
	written = true;
	eof     = false;
}

TVcfFileSingleLine::~TVcfFileSingleLine() {
	if (!written) {
		if (automaticallyWriteVcf) parser.writeLine(tempLine, *myOutStream);
	}
}

void TVcfFileSingleLine::writeLine() {
	parser.writeLine(tempLine, *myOutStream);
	written = true;
}

bool TVcfFileSingleLine::next() {
	// write old Line
	if (!written) {
		if (automaticallyWriteVcf) writeLine();
	}
	if (!readLine())
		return false;
	else {
		written = false;
		return true;
	}
}

void TVcfFileSingleLine::updateInfo(std::string_view Id, std::string_view Data) {
	if (written) UERROR("Can not update line, no line read!");
	parser.updateInfo(tempLine, Id, Data);
}
void TVcfFileSingleLine::updatePL(std::string_view Data, unsigned int sample) { parser.updatePL(tempLine, Data, sample); }
void TVcfFileSingleLine::addToInfo(std::string_view Id, std::string_view Data) {
	if (written) UERROR("Can not update line, no line read!");
	parser.addToInfo(tempLine, Id, Data);
}
std::string TVcfFileSingleLine::fieldContentAsString(std::string_view tag, unsigned int sample) {
	return TVcfFile_base::fieldContentAsString(tag, &tempLine, sample);
}
int TVcfFileSingleLine::fieldContentAsInt(std::string_view tag, unsigned int sample) {
	return TVcfFile_base::fieldContentAsInt(tag, &tempLine, sample);
}

int TVcfFileSingleLine::depthAsIntNoCheckForMissingSample(std::string_view tag, unsigned int sample) {
	return TVcfFile_base::depthAsIntNoCheckForMissingSample(tag, &tempLine, sample);
}

bool TVcfFileSingleLine::isBialleleicSNP() {
	if (parser.getNumAlleles(tempLine) == 2) {
		std::string_view ref = parser.getAllele(tempLine, 0);
		std::string_view alt = parser.getAllele(tempLine, 1);
		return (ref == "A" || ref == "C" || ref == "G" || ref == "T") &&
		       (alt == "A" || alt == "C" || alt == "G" || alt == "T");
	}
	return false;
};


void TVcfFileSingleLine::setSampleMissing(unsigned int sample) { TVcfFile_base::setSampleMissing(&tempLine, sample); }

void TVcfFileSingleLine::setSampleHasUndefinedGenotype(unsigned int sample) {
	TVcfFile_base::setSampleHasUndefinedGenotype(&tempLine, sample);
}

void TVcfFileSingleLine::updateField(std::string tag, std::string &Data, unsigned int sample) {
	TVcfFile_base::updateField(&tempLine, tag, Data, sample);
}

Genotype TVcfFileSingleLine::sampleGenotype(unsigned int num) const {
	using namespace genometools;
	return genotype(char2base(parser.getFirstAlleleOfSample(tempLine, num)[0]),
	                char2base(parser.getSecondAlleleOfSample(tempLine, num)[0]));
};

// int TVcfFileSingleLine::sampleDepth(unsigned int sample){
double TVcfFileSingleLine::sampleDepth(unsigned int sample) const {
	// return 0 if sample is missing
	if (parser.sampleIsMissing(tempLine, sample)) return 0;
	// check if depth is given
	if (parser.formatColExists("DP", tempLine))
		return coretools::str::fromString<double>(parser.sampleContentAt(tempLine, "DP", sample));
	else
		return -1;
}

}; // end namespace genometools
