/*
 * TVCFFields.h
 *
 *  Created on: Nov 18, 2018
 *      Author: phaentu
 */

#ifndef TVCFFIELDS_H_
#define TVCFFIELDS_H_

#include <vector>
#include <string>

#include "coretools/Files/gzstream.h"
#include "coretools/Main/TError.h"

namespace genometools {

//------------------------------------------------------
// TVCFField
//------------------------------------------------------
class TVCFField {
public:
	std::string tag;
	std::string description;
	bool accepted = false;
	bool used = false;

	TVCFField(std::string Tag, std::string Description)
		: tag(std::move(Tag)), description(std::move(Description)){};

	void writeHeader(std::string_view type, gz::ogzstream &vcf) {
		vcf << "##" << type << "=<ID=" << tag << ',' << description << ">\n";
	};
};

//------------------------------------------------------
// TVCFFieldVector
//------------------------------------------------------
class TVCFFieldVector {
private:
	std::string _type;
	std::vector<TVCFField> availableFields;
	std::vector<TVCFField *> usedFields;

public:
	TVCFFieldVector(std::string Type, std::initializer_list<std::pair<std::string, std::string>> fields = {}) :_type(std::move(Type)) {
		for (auto& f: fields) add(f.first, f.second);
	};

	const std::string &type() const { return _type; };

	void add(std::string tag, std::string description) { availableFields.emplace_back(std::move(tag), std::move(description)); };

	TVCFField *getFieldPointer(std::string_view tag) {
		for (auto it = availableFields.begin(); it != availableFields.end(); ++it) {
			if (tag == it->tag) return &(*it);
		}
		UERROR("VCF ", _type, " field '", tag, "' is unknown!");
	};

	bool fieldExists(std::string_view tag) const {
		for (auto it = availableFields.begin(); it != availableFields.end(); ++it) {
			if (it->tag == tag) return true;
		}
		return false;
	};

	void acceptField(std::string_view tag) { getFieldPointer(tag)->accepted = true; };

	bool useField(std::string_view tag) {
		TVCFField *pt = getFieldPointer(tag);
		if (pt->accepted) {
			if (!pt->used) {
				usedFields.push_back(pt);
				pt->used = true;
			}
			return true;
		}
		return false;
	};

	void clearUsed() {
		usedFields.clear();
		for (auto it = availableFields.begin(); it != availableFields.end(); ++it)
			it->used = false;
	};

	size_t numUsed() const { return usedFields.size(); };

	std::string getListOfUsedFields(std::string_view delim) const {
		std::string out;
		for (TVCFField *it : usedFields) {
			if (out.length() > 0) out += delim;
			out += it->tag;
		}
		return out;
	};

	void fillVectorWithTagsOfUsedFields(std::vector<std::string> &vec) const {
		vec.clear();
		for (auto it = usedFields.begin(); it != usedFields.end(); ++it)
			vec.push_back((*it)->tag);
	};

	void writeVCFHeader(gz::ogzstream &vcf) const {
		// info fields
		for (auto it = usedFields.begin(); it != usedFields.end(); ++it)
			(*it)->writeHeader(_type, vcf);
	};
};

}; // end namespace genometools

#endif /* TVCFFIELDS_H_ */
