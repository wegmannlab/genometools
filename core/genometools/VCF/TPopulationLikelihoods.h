//
// Created by caduffm on 3/10/22.
//

#ifndef ATLAS_TPOPULATIONLIKELIHOODS_H
#define ATLAS_TPOPULATIONLIKELIHOODS_H

#include "coretools/Main/TLog.h"
#include "coretools/Math/TNumericRange.h"
#include "coretools/Storage/TStorage.h"
#include "coretools/Files/TInputFile.h"

#include "coretools/Types/probability.h"
#include "genometools/TBed.h"
#include "genometools/GenomePositions/TGenomePosition.h"
#include "genometools/GenomePositions/TNamesPositions.h"
#include "genometools/Genotypes/TFrequencies.h"
#include "genometools/TSampleLikelihoods.h"
#include "genometools/VCF/TPopulation.h"
#include "genometools/VCF/TPopulationLikelihoodLocus.h"
#include "genometools/VCF/TVcfFile.h"

namespace genometools {

//-------------------------------------------------
// TPopulationLikelihoodReader
//-------------------------------------------------

class TPopulationLikelihoodReader {
protected:
	// about VCF
	bool _initialized = false;
	TVcfFileSingleLine _vcfFile;

	// filters
	size_t _limitLines = 0;
	coretools::TNumericRange<size_t> _depthFilter;
	coretools::Probability _maxMissing{1.0};
	double _minMAF                     = 0.0;
	double _epsF                       = 1e-07;
	bool _doEM                         = false;
	size_t _minVariantQuality          = 0;
	bool _filterOnChr                  = false;
	std::vector<std::string> _chrsToKeep;
	bool _limitChr = false;
	std::string _chrUpAndIncluding;

	// BED file for windows
	TBed _bedFile;
	TBed::const_iterator _curBedWindow;
	uint32_t _curRefId      = 0;
	bool _limitToSitesInBed = false;

	// counters
	coretools::TTimer _timer;
	bool _vcfParsingStarted          = false;
	size_t _progressFrequency        = 0;
	size_t _lineCounter              = 0;
	size_t _notInBedFile             = 0;
	size_t _notBialleleicCounter     = 0;
	size_t _missingSNPCounter        = 0;
	size_t _lowFreqSNPCounter        = 0;
	size_t _lowVariantQualityCounter = 0;
	size_t _noPLCounter              = 0;
	size_t _numAcceptedLoci          = 0;
	size_t _notOnChrCounter          = 0;

	// tmp variables used for reading
	TGenotypeFrequencies _genoFrequencies;
	std::string _curChr;

	void _resetCounters();

	void _readWindowSettings();
	void _readDepthSettings();
	void _readMissingSettings();
	void _readMAFSettings(bool saveAlleleFreq);
	void _readVariantQualitySettings();
	void _readChrSettings();

	void _printProgressFrequencyFiltering() const;

	int _filterOnDepth(TPopulationSamples &samples);

	virtual bool _readNextLineFromVCF();
	void _removeParsedChr();
	template<typename Type> bool _filterSite(Type *data, TPopulationSamples &samples) {
		// skip sites with != 2 alleles
		if (_vcfFile.getNumAlleles() != 2) {
			_notBialleleicCounter++;
			return false;
		}

		// keep chromosomes
		if (_filterOnChr && std::find(_chrsToKeep.begin(), _chrsToKeep.end(), _vcfFile.chr()) == _chrsToKeep.end()) {
			_notOnChrCounter++;
			return false;
		}

		// skip sites with too low variant quality
		if (_minVariantQuality > 0 &&
		    (_vcfFile.variantQualityIsMissing() || _vcfFile.variantQuality() < (double)_minVariantQuality)) {
			_lowVariantQualityCounter++;
			return false;
		}

		// check if GL or PL is given
		if (!_vcfFile.formatColExists("GL") && !_vcfFile.formatColExists("PL")) {
			++_noPLCounter;
			return false;
		}

		// missingness filter: if >= _maxMissing of individuals per locus are missing, remove locus
		uint32_t numMissing = _filterOnDepth(samples);
		if (numMissing >= _maxMissing * samples.numSamples()) {
			_missingSNPCounter++;
			return false;
		}

		// store data
		// TODO: find way not to strore data if not needed (e.g. F2 task without MAF filter)
		for (uint32_t s = 0; s < samples.numSamples(); ++s) {
			unsigned int vcfIndex = samples.sampleIndexInVCF(s);
			_vcfFile.fillGenotypeLikelihoods(data[s], vcfIndex);
		}

		// filter in MAF
		if (_doEM) {
			_genoFrequencies.estimate<true>(data, samples.numSamples(), _epsF);
			if (_genoFrequencies.MAF() < _minMAF) {
				_lowFreqSNPCounter++;
				return false;
			}
		}

		// all fine!
		return true;
	}
	bool _updateChromosomeInfo();
	bool _jumpToNextChromosome();

public:
	TPopulationLikelihoodReader() = default;
	TPopulationLikelihoodReader(bool saveAlleleFreq);
	virtual ~TPopulationLikelihoodReader() = default;

	virtual void initialize(bool saveAlleleFreq);
	void doEstimateGenotypeFrequencies() { _doEM = true; };

	void openVCF(std::string_view vcfFilename);
	void concludeFilters() const;
	std::vector<std::string> &getSampleVCFNames() { return _vcfFile.parser.samples; };
	long numAcceptedLoci() const { return _numAcceptedLoci; };
	double variantQuality() const { return _vcfFile.variantQuality(); };
};

//-------------------------------------------------
// TPopulationLikelihoodReaderLocus
//-------------------------------------------------

class TPopulationLikelihoodReaderLocus : public TPopulationLikelihoodReader {
private:
	// true allele frequencies
	coretools::TInputFile _trueFreqFile;
	double _trueAlleleFrequency = -1.0;
	bool _storeTrueFreq         = false;

	bool _readNextLineFromVCF() override;
	std::tuple<std::string, size_t, coretools::Probability> _readLineFreqFile();

public:
	TPopulationLikelihoodReaderLocus() = default;
	TPopulationLikelihoodReaderLocus(bool saveAlleleFreq);

	void initialize(bool saveAlleleFreq) override;
	void doSaveTrueAlleleFrequencies() { _storeTrueFreq = true; }

	std::string chr() const { return _curChr; };
	long position() const { return _vcfFile.position(); };
	std::string chrPos() const { return _curChr + ":" + coretools::str::toString(position()); };
	long positionZeroBased() const { return _vcfFile.positionZeroBased(); };
	Base refAllele() const { return char2base(_vcfFile.getRefAllele()[0]); };
	Base altAllele() const { return char2base(_vcfFile.getFirstAltAllele()[0]); };
	std::vector<BiallelicGenotype> biallelicGenotypes(TPopulationSamples &samples) const;
	BiallelicGenotype biallelicGenotype(TPopulationSamples &samples, uint32_t s) const;
	Genotype genotype(TPopulationSamples &samples, uint32_t s) const;
	double depth(TPopulationSamples &samples, uint32_t s);
	bool sampleIsMissing(TPopulationSamples &samples, uint32_t s);

	bool readDataFromVCF(TPopulationSamples &samples) {
		// wrapper around readDataFromVCF if you don't need to store GTL
		TPopulationLikehoodLocus<TSampleLikelihoods<coretools::HPPhredInt>> data;
		return readDataFromVCF(data, samples);
	}
	template<typename Type> bool readDataFromVCF(TPopulationLikehoodLocus<Type> &data, TPopulationSamples &samples) {
		data.resize(samples.numSamples());
		return readDataFromVCF(data.samples(), samples);
	};
	template<typename Type> bool readDataFromVCF(Type *data, TPopulationSamples &samples) {
		using namespace coretools::instances;
		// set time at beginning
		if (!_vcfParsingStarted) {
			_vcfParsingStarted = true;
			_timer.start();

			// start bed
			if (_limitToSitesInBed) { _curBedWindow = _bedFile.begin(); }
		}

		// read next
		while (_readNextLineFromVCF()) { // new line in vcf-file (= new locus)
			// update chr
			if (_curChr != _vcfFile.chr()) {
				if (!_updateChromosomeInfo()) { return false; }

				// jump bed to this chromosome
				if (_limitToSitesInBed) {
					// jump to next chromosome until we are on a chromsome with BED windows
					while (_bedFile.empty(_curChr)) {
						if (!_jumpToNextChromosome()) { return false; }
					}

					// get ref id of this chromosome
					_curRefId = _bedFile.refID(_curChr);

					// start windows on this chromosome
					_curBedWindow = _bedFile.begin(_curRefId);
				}
			}

			// check if site is used
			if (_limitToSitesInBed) {
				// stop parsing if we reached end of bed file
				if (_curBedWindow == _bedFile.end()) {
					logfile().list("Reached end of last window (BED file).");
					return false;
				}

				// check if VCF is ahead or behind BED window
				TGenomePosition pos(_curRefId, _vcfFile.positionZeroBased());

				// jump to next window if position is past current window
				while (*_curBedWindow < pos) {
					++_curBedWindow;
					if (_curBedWindow == _bedFile.end()) {
						logfile().list("Reached end of last window (BED file).");
						return false;
					}
				}

				// continue to next pos in VCF is window is ahead
				if (pos < *_curBedWindow) {
					++_notInBedFile;
					continue;
				}

				// else accept current position
				if (!_curBedWindow->within(pos)) { DEVERROR("something went wrong!"); }
			}

			// filter
			if (_filterSite(data, samples)) {
				// SNP is accepted!
				++_numAcceptedLoci;
				return true;
			}
		}

		// return false at end of file
		logfile().list("Reached end of VCF file.");

		if (_limitChr && !_chrsToKeep.empty()) {
			_removeParsedChr();
			std::string chrs = coretools::str::concatenateString(_chrsToKeep, ",");
			logfile().warning("Did not encounter the following chromosomes defined with parameter 'chr': ", chrs, ".");
		}

		return false;
	};

	void openTrueAlleleFrequenciesFile(std::string_view Filename);
	TGenotypeFrequencies *genotypeFrequencies() { return &_genoFrequencies; };
	double alleleFrequency() const { return _genoFrequencies.alleleFrequency(); };
	double trueAlleleFrequency() const { return _trueAlleleFrequency; };
	void writePosition(coretools::TOutputFile &out);
	double depth(TPopulationSamples &samples, uint32_t s) const;
};

//-------------------------------------------------
// TPopulationLikelihoodReaderWindow
//-------------------------------------------------

class TPopulationLikelihoodReaderWindow : public TPopulationLikelihoodReader {
private:
	bool _readNextLocusAndUpdateChromosome();

public:
	TPopulationLikelihoodReaderWindow() = default;
	TPopulationLikelihoodReaderWindow(bool saveAlleleFreq);
	~TPopulationLikelihoodReaderWindow() = default;

	template<typename Type> bool readDataFromVCF(TPopulationLikehoodWindow<Type> &window, TPopulationSamples &samples) {
		// ASSUMPTION: all chromosomes are present in VCF with at least one position!
		// only works if BED file is open
		if (!_limitToSitesInBed) { UERROR("Can not read data from VCF into window: no windows defined!"); }

		// initialize if at beginning of VCF
		if (!_vcfParsingStarted) {
			_vcfParsingStarted = true;
			_timer.start();

			// read first locus on first chromosome with windows
			_readNextLocusAndUpdateChromosome();
		} else {
			// advance window
			++_curBedWindow;
			if (_curBedWindow == _bedFile.end()) return false;
		}

		// resize window
		window.resize(_curBedWindow->size(), samples.numSamples());

		// 1) make sure we are at right window and place in VCF
		//----------------------------------------------------
		// if VCF is ahead of window on same chromosome, advance VCF
		while (!_vcfFile.eof && _curRefId == _curBedWindow->refID() &&
		       _vcfFile.positionZeroBased() < _curBedWindow->from().position()) {
			_readNextLocusAndUpdateChromosome();
		}

		// if VCF is at end, is on next chromosome or past window, return empty
		if (_vcfFile.eof || _curRefId != _curBedWindow->refID() ||
		    _vcfFile.positionZeroBased() >= _curBedWindow->to().position()) {
			window.fillAsMissingDiploid();
			return true;
		}

		// 2) fill window!
		//---------------
		uint32_t index = 0;
		while (_vcfFile.positionZeroBased() < _curBedWindow->to().position()) {
			uint32_t curIndex = _vcfFile.positionZeroBased() - _curBedWindow->from().position();

			// fill empty until cur position
			for (; index < curIndex; ++index) { window[index].fillAsMissingDiploid(); }

			// fill cur position
			if (_filterSite(window[index].samples(), samples)) {
				// SNP is accepted!
				++_numAcceptedLoci;
			} else {
				window[index].fillAsMissingDiploid();
			}
			++index;

			// read next
			if (!_readNextLocusAndUpdateChromosome()) {
				// end of file: fill empty until end
				for (; index < window.numLoci(); ++index) { window[index].fillAsMissingDiploid(); }
				break;
			}

			// if VCF is on next chromosome, fill empty until end
			if (_curRefId != _curBedWindow->refID()) {
				for (; index < window.numLoci(); ++index) { window[index].fillAsMissingDiploid(); }
				break;
			}
		}

		// all fine!
		return true;
	};

	void writeWindow(coretools::TOutputFile &out);
};

//-------------------------------------------------
// TPopulationLikelihoods
//-------------------------------------------------

template<typename Type, typename TypePos = TPositionsRaw> class TPopulationLikelihoods {
private:
	// data on individuals
	TPopulationSamples _samples;

	// data on loci: ptr to TPositionsRaw or one of its derived classes
	TypePos *_positions = nullptr;
	bool _ownsPositions = true;

	// store genotype likelihoods
	coretools::TMultiDimensionalStorage<Type, 2> _storage;

	// store allele frequencies
	std::vector<double> _alleleFrequencies;
	std::vector<double> _trueAlleleFrequencies;
	bool _saveAlleleFrequencies     = false;
	bool _saveTrueAlleleFrequencies = false;

	size_t _guessNumLoci() {
		// guess on how many loci to expect (may speed up reading data)
		using namespace coretools::instances;
		size_t guessNumLoci = 1000000;
		if (parameters().exists("guessNumLoci")) {
			guessNumLoci = coretools::instances::parameters().get<size_t>("guessNumLoci");
			logfile().list("Argument 'guessNumLoci': Will initialize the memory for storing GTL to " +
			               coretools::str::toString(guessNumLoci) + " loci.");
		}
		return guessNumLoci;
	}

	void _matchSamples(TPopulationLikelihoodReaderLocus &Reader) {
		if (_samples.hasSamples()) {
			_samples.fillVCFOrder(Reader.getSampleVCFNames());
		} else {
			_samples.readSamplesFromVCFNames(Reader.getSampleVCFNames());
		}
	}

	void _storeAlleleFrequencies(const TPopulationLikelihoodReaderLocus &Reader) {
		if (_saveAlleleFrequencies) { _alleleFrequencies.emplace_back(Reader.alleleFrequency()); }
		if (_saveTrueAlleleFrequencies) { _trueAlleleFrequencies.emplace_back(Reader.trueAlleleFrequency()); }
	}

	void _finalizeStorage() {
		// finalize and set dimension names (loci and individual names)
		_storage.finalizeFillData();
		_positions->finalizeFilling();
		_storage.setDimensionName(std::make_shared<TNamesPositions>(_positions), 0);
		_storage.setDimensionName(std::make_shared<coretools::TNamesStrings>(_samples.sampleNames()), 1);
	}

	void _readDataFromVCF() {
		using namespace coretools::instances;
		// create VCF reader
		TPopulationLikelihoodReaderLocus reader(_saveAlleleFrequencies);
		if (_saveTrueAlleleFrequencies) { reader.doSaveTrueAlleleFrequencies(); }

		// open vcf file
		std::string filename = parameters().get("vcf");
		reader.openVCF(filename);
		_matchSamples(reader);

		// prepare storage
		_storage.prepareFillData(_guessNumLoci(), {_samples.numSamples()});
		auto ptr = _storage.addMemoryBlock(_samples.numSamples());

		// run through VCF file
		logfile().startIndent("Parsing VCF file:");
		while (reader.readDataFromVCF(ptr, _samples)) {
			// store chromosome and position and allele frequency
			_positions->add(reader.position(), reader.chr());
			_storeAlleleFrequencies(reader);
			// update storage for next
			ptr = _storage.addMemoryBlock(_samples.numSamples());
		}
		// remove last, empty container
		_storage.pop_back(_samples.numSamples());
		_finalizeStorage();

		// report
		logfile().endIndent();
		reader.concludeFilters();
		if (_positions->size() == 0) { UERROR("No usable loci in VCF file '", filename, "'!"); }
	};

public:
	TPopulationLikelihoods() {
		_positions     = new TypePos;
		_ownsPositions = true;
	}
	TPopulationLikelihoods(TypePos *Positions) {
		_positions     = Positions;
		_ownsPositions = false;
	}
	~TPopulationLikelihoods() {
		if (_ownsPositions) { delete _positions; }
	}

	void clear() { _storage.clear(); };

	void doSaveAlleleFrequencies() { _saveAlleleFrequencies = true; };
	void doSaveTrueAlleleFrequencies() { _saveTrueAlleleFrequencies = true; };

	void setSamples(const TPopulationSamples &PopSamples) { _samples = PopSamples; }

	void readData() {
		// check if we limit samples
		using namespace coretools::instances;
		if (parameters().exists("samples") && _samples.numSamples() == 0) { // only if not filled before
			_samples.readSamples(parameters().get<std::string>("samples"));
		}

		// read Data
		_readDataFromVCF();
	};

	TypePos *positions() const { return _positions; };
	size_t numSamples() const { return _samples.numSamples(); };
	size_t numLoci() const { return _positions->size(); };
	const std::vector<double> &alleleFrequencies() const { return _alleleFrequencies; };
	const std::vector<double> &trueAlleleFrequencies() const { return _trueAlleleFrequencies; }
	void clearAlleleFrequencies() { _alleleFrequencies.clear(); }
	void clearTrueAlleleFrequencies() { _trueAlleleFrequencies.clear(); }
	const coretools::TMultiDimensionalStorage<Type, 2> &getStorage() const { return _storage; }
	void setStorage(coretools::TMultiDimensionalStorage<Type, 2> newStorage) { _storage = newStorage; }
};

}; // end namespace genometools

#endif // ATLAS_TPOPULATIONLIKELIHOODS_H
