#ifndef VCF_TVCFWRITER_H_
#define VCF_TVCFWRITER_H_

#include <string>

#include "coretools/Containers/TView.h"
#include "coretools/Files/TOutputFile.h"
#include "coretools/Types/probability.h"

#include "genometools/GLF/GLF.h"
#include "genometools/VCF/VCF.h"
#include "genometools/GenomePositions/TChromosomes.h"

namespace genometools{

class TVCFWriter {
private:
	coretools::TOutputFile _vcf;

	bool _usePhred = false;
	std::string _format;

	void _writeLikelihood(coretools::HPPhredInt likGlf);
	void _writeHeader(std::string_view Source, coretools::TConstView<std::string> SampleNames, const TChromosomes &Chrs);
	coretools::HPPhredInt _writeGenotypeAndQuality(coretools::TConstView<coretools::HPPhredInt> GTL);
	void _writeCell(size_t Depth, coretools::TConstView<coretools::HPPhredInt> GTL);

	void _writeFixed(std::string_view ChrName, size_t Position, std::string_view ID, std::string_view RefAllele,
				   std::string_view AltAllele, coretools::PhredInt VariantQuality, std::string_view Filter);

	void _writeFixed(std::string_view ChrName, size_t Position, std::string_view ID, Base RefAllele, Base AltAllele,
					 coretools::PhredInt VariantQuality, std::string_view Filter);

public:
	TVCFWriter(std::string_view Filename, std::string_view Source, coretools::TConstView<std::string> SampleNames,
			   const TChromosomes &Chrs, bool UsePhred);

	TVCFWriter(coretools::TWriter *Writer, std::string_view Source, coretools::TConstView<std::string> SampleNames,
	           const TChromosomes &Chrs, bool UsePhred);

	// just pass the line
	void writeSite(std::string_view Site) { _vcf.writeln(Site); }

	// line without data
	template<typename BaseType>
	void writeSite(std::string_view ChrName, size_t Position, std::string_view ID, BaseType RefAllele,
				   BaseType AltAllele, coretools::PhredInt VariantQuality, std::string_view Filter,
				   std::string_view Info) {
		_writeFixed(ChrName, Position, ID, RefAllele, AltAllele, VariantQuality, Filter);
		_vcf.writeln(Info);
	}

	// line with data
	template<typename BaseType>
	void writeSite(std::string_view ChrName, size_t Position, std::string_view ID, BaseType RefAllele, BaseType AltAllele,
				   coretools::PhredInt VariantQuality, std::string_view Filter, std::string_view Info,
				   std::string_view Format, coretools::TConstView<std::string> Data) {
		_writeFixed(ChrName, Position, ID, RefAllele, AltAllele, VariantQuality, Filter);
		_vcf.write(Info, Format);
		for (const auto& d: Data) _vcf.write(d);
		_vcf.endln();
	}

	void writeSite(std::string_view ChrName, size_t Position, Base RefAllele, Base AltAllele,
				   coretools::PhredInt VariantQuality, coretools::TConstView<TVCFEntry> Data);

	void writeSite(std::string_view ChrName, size_t Position, Base RefAllele, Base AltAllele,
	               coretools::PhredInt VariantQuality, coretools::TConstView<TGLFEntry> Data);
};

} // end namespace genometools

#endif
