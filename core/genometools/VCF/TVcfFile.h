/*
 * TVcfFile.h
 *
 *  Created on: Aug 8, 2011
 *      Author: wegmannd
 */

#ifndef TVCFFILE_H_
#define TVCFFILE_H_

#include <vector>

#include "genometools/Genotypes/Base.h"
#include "genometools/Genotypes/BiallelicGenotype.h"
#include "genometools/Genotypes/Genotype.h"
#include "genometools/VCF/TVcfParser.h"

namespace genometools {

typedef void (TVcfParser::*pt2Function)(TVcfLine &);
//---------------------------------------------------------------------------------------------------------
class TVcfFile_base {
public:
	std::istream *myStream = nullptr;
	bool inputStreamOpend = false;
	std::ostream *myOutStream = nullptr;
	bool outputStreamOpend = false;
	std::string fileFormat;
	// TVcfColumnNumbers cols;
	TVcfParser parser;
	unsigned int numCols = -1;
	long currentLine = 0;
	std::vector<pt2Function> usedParsers;
	bool automaticallyWriteVcf = false;
	TVcfLine tempLine;
	bool eof = false;
	double totalFileSize = -1;
	std::string filename, outputFilename;

	// vector<TVcfFilter> filters;
	// bool applyFilters;

	std::vector<std::string> unknownHeader;

	TVcfFile_base() = default;
	TVcfFile_base(std::string Filename, bool zipped) : filename(std::move(Filename)) {
		openStream(filename, zipped);
	};
	virtual ~TVcfFile_base() {
		if (inputStreamOpend) delete myStream;
		if (outputStreamOpend) delete myOutStream;
	};
	void enableAutomaticWriting() {
		if (!outputStreamOpend) UERROR("Can not automatically write VCF: no output stream has been opened!");
		automaticallyWriteVcf = true;
	};

	void openStream(std::string_view filename);
	void openStream(std::string_view filename, bool zipped);
	void openOutputStream(std::string_view filename, bool zipped);
	void setOutStream(std::ostream &is);

	// which parsers to use?
	void enablePositionParsing() { usedParsers.push_back(&TVcfParser::parsePosition); };
	void enableVariantQualityParsing() { usedParsers.push_back(&TVcfParser::parseQuality); };
	void enableVariantParsing() { usedParsers.push_back(&TVcfParser::parseVariant); };
	void enableInfoParsing() { usedParsers.push_back(&TVcfParser::parseInfo); };
	void enableFormatParsing() { usedParsers.push_back(&TVcfParser::parseFormat); };
	void enableSampleParsing() { usedParsers.push_back(&TVcfParser::parseSamples); };

	// retrieve info
	int sampleNumber(std::string_view Name) const { return parser.getSampleNum(Name); }
	int numSamples() const { return parser.getNumSamples(); }
	std::string sampleName(unsigned int num) const { return parser.getSampleName(num); }
	bool sampleIsMissing(const TVcfLine *line, unsigned int sample) const { return parser.sampleIsMissing(*line, sample); };
	bool sampleHasUnknownGenotype(const TVcfLine *line, unsigned int sample) const {return parser.sampleHasUndefinedGenotype(*line, sample);};
	virtual std::string fieldContentAsString(std::string_view tag, const TVcfLine *line, unsigned int sample) const {return parser.sampleContentAt(*line, tag, sample);};
	virtual int fieldContentAsInt(std::string_view tag, const TVcfLine *line, unsigned int sample) const ;
	virtual int depthAsIntNoCheckForMissingSample(std::string_view tag, const TVcfLine *line, unsigned int sample) const;

	// modify
	void setSampleMissing(TVcfLine *line, unsigned int sample) const;
	void setSampleHasUndefinedGenotype(TVcfLine *line, unsigned int sample) const ;
	void updateField(TVcfLine *line, std::string_view tag, std::string_view Data, unsigned int sample) const;

	// void addFilter(my_string filter);
	// void filterSamples();
	// void printFilters();

	void parseHeaderVCF_4_0();
	void writeHeaderVCF_4_0() const;
	void addNewHeaderLine(std::string headerLine);
	bool readLine();
	void addFormat(std::string Line) { parser.addFormat(Line); };
};

class TVcfFileSingleLine : public TVcfFile_base {
public:
	bool written;

	TVcfFileSingleLine() { written = true; };
	TVcfFileSingleLine(std::string_view filename, bool zipped);
	virtual ~TVcfFileSingleLine();
	void writeLine();
	bool next();
	// call specific parsers
	void parseInfo() { parser.parseSamples(tempLine); };
	void parseFormat() { parser.parseFormat(tempLine); };
	void parseSamples() { parser.parseSamples(tempLine); };

	// other stuff
	TVcfLine *pointerToVcfLine() { return &tempLine; };
	void updateInfo(std::string_view Id, std::string_view Data);
	void addToInfo(std::string_view Id, std::string_view Data);
	void updatePL(std::string_view Data, unsigned int sample);
	using TVcfFile_base::depthAsIntNoCheckForMissingSample;
	using TVcfFile_base::fieldContentAsInt;
	using TVcfFile_base::fieldContentAsString;
	std::string fieldContentAsString(std::string_view tag, unsigned int sample);
	int fieldContentAsInt(std::string_view tag, unsigned int sample);
	int depthAsIntNoCheckForMissingSample(std::string_view tag, unsigned int sample);

	template<typename T> void fillGenotypeLikelihoods(T &SampleLikelihoods, unsigned int &s) {
		parser.fillGenotypeLikelihoods(SampleLikelihoods, tempLine, s);
	};

	// variant info
	uint64_t position() const { return parser.getPos(tempLine); };
	uint64_t positionZeroBased() const { return position() - 1; };
	std::string chr() const { return parser.getChr(tempLine); };
	bool variantQualityIsMissing() const { return parser.variantQualityIsMissing(tempLine); }
	double variantQuality() const { return parser.variantQuality(tempLine); }
	int getNumAlleles() const { return parser.getNumAlleles(tempLine); }
	std::string getRefAllele() const { return parser.getRefAllele(tempLine); };
	std::string getFirstAltAllele() const { return parser.getFirstAltAllele(tempLine); };
	std::string getAllele(int num) const { return parser.getAllele(tempLine, num); };
	bool isBialleleicSNP();

	// sample info
	void setSampleMissing(unsigned int sample);
	void setSampleHasUndefinedGenotype(unsigned int sample);
	void updateField(std::string tag, std::string &Data, unsigned int sample);

	bool sampleIsMissing(unsigned int sample) const { return TVcfFile_base::sampleIsMissing(&tempLine, sample); };
	bool sampleHasUndefinedGenotype(unsigned int sample) const {
		return TVcfFile_base::sampleIsMissing(&tempLine, sample);
	};
	bool sampleIsHaploid(unsigned int sample) const { return parser.sampleIsHaploid(tempLine, sample); };

	;
	bool sampleIsDiploid(unsigned int sample) const { return parser.sampleIsDiploid(tempLine, sample); };
	;
	bool sampleIsHomoRef(unsigned int sample) const { return parser.sampleIsHomoRef(tempLine, sample); };

	bool sampleIsHeteroRefNonref(unsigned int sample) const {
		return parser.sampleIsHeteroRefNonref(tempLine, sample);
	};

	Base getFirstAlleleOfSample(unsigned int num) const {
		return char2base(parser.getFirstAlleleOfSample(tempLine, num)[0]);
	};

	Base getSecondAlleleOfSample(unsigned int num) const {
		return char2base(parser.getSecondAlleleOfSample(tempLine, num)[0]);
	};
	BiallelicGenotype sampleBiallelicGenotype(unsigned int num) const {
		return parser.sampleBiallelicGenotype(tempLine, num);
	};
	;
	Genotype sampleGenotype(unsigned int num) const;
	float sampleGenotypeQuality(unsigned int sample) const {
	return parser.sampleGenotypeQuality(tempLine, sample);
}
;
	double sampleDepth(unsigned int sample) const;
	// int sampleDepth(unsigned int sample);
	bool formatColExists(std::string_view tag) const { return parser.formatColExists(tag, tempLine); };
	const std::string& getSampleContentAt(std::string_view tag, unsigned int sample) {
		return parser.sampleContentAt(tempLine, tag, sample);
	}
};

}; // end namespace genometools

#endif /* TVCFFILE_H_ */
