/*
 * TGlfMultireader.cpp
 *
 *  Created on: Feb 11, 2020
 *      Author: wegmannd
 */

#include "TVCFWriter.h"

#include <ctime>

#include "coretools/Containers/TView.h"
#include "coretools/Files/TWriter.h"
#include "coretools/Main/TRandomGenerator.h"
#include "coretools/Types/probability.h"
#include "genometools/GLF/GLF.h"
#include "genometools/Genotypes/Ploidy.h"
#include "genometools/VCF/VCF.h"

namespace genometools {
using coretools::instances::randomGenerator;
using coretools::TConstView;

namespace impl {
template<typename Container> size_t totalDepth(const Container &samples) noexcept {
	return std::accumulate(samples.begin(), samples.end(), size_t{},
						   [](auto tot, const auto &s) { return tot + s.depth; });
}

coretools::HPPhredInt getSecondHighestGTL(size_t IndexBest, TConstView<coretools::HPPhredInt> GTL) {
	if (GTL.size() == 2) {         // haploid
		return GTL[1 - IndexBest]; // the other one
	} else {
		constexpr std::array<std::array<size_t, 2>, 3> a = {{{1, 2}, {0, 2}, {0, 1}}}; // the two other ones
		return std::min(GTL[a[IndexBest][0]], GTL[a[IndexBest][1]]);
	}
}

template<typename Entry>
void writeDepth(TConstView<Entry> Data, coretools::TOutputFile& Vcf) {
	const auto depth = totalDepth(Data);
	Vcf.writeNoDelim("DP=", depth).writeDelim();
}


} // namespace impl

//----------------------------------------------------
// TVcfWriter
//----------------------------------------------------
TVCFWriter::TVCFWriter(std::string_view Filename, std::string_view Source, TConstView<std::string> SampleNames,
					   const TChromosomes &Chrs, bool UsePhred)
	: TVCFWriter(coretools::makeWriter(Filename, "w"), Source, SampleNames, Chrs, UsePhred) {}

TVCFWriter::TVCFWriter(coretools::TWriter *Writer, std::string_view Source, TConstView<std::string> SampleNames,
					   const TChromosomes &Chrs, bool UsePhred) : _usePhred(UsePhred) {
	_format = "GT:GQ:DP:";
	if (UsePhred) {
		_format += "PL";
	} else {
		_format += "GL";
	}

	_vcf.open(Writer, "\t");
	_writeHeader(Source, SampleNames, Chrs);
}

void TVCFWriter::_writeHeader(std::string_view Source, TConstView<std::string> SampleNames, const TChromosomes &Chrs) {
	_vcf.writeNoDelim("##fileformat=", VCFversion()).endln();

	const std::time_t time    = std::time(nullptr);
	const struct tm *timeinfo = localtime(&time);
	char buffer[9];
	strftime(buffer, 9, "%Y%m%d", timeinfo);
	_vcf.writeNoDelim("##fileDate=", buffer).endln();

	_vcf.writeNoDelim("##source=", Source).endln();

	// make sure the header matches the format used in writeSiteToVCF
	_vcf.writeln("##INFO=<ID=DP,Number=1,Type=Integer,Description=\"Total Depth\">");

	_vcf.writeln("##FORMAT=<ID=GT,Number=1,Type=String,Description=\"Genotype\">");
	_vcf.writeln("##FORMAT=<ID=GQ,Number=1,Type=Integer,Description=\"Genotype quality\">");
	_vcf.writeln("##FORMAT=<ID=DP,Number=1,Type=Integer,Description=\"Read Depth\">");
	if (_usePhred)
		_vcf.writeln("##FORMAT=<ID=PL,Number=G,Type=Integer,Description=\"Phred-scaled normalized genotype likelihoods\">");
	else
		_vcf.writeln("##FORMAT=<ID=GL,Number=G,Type=Float,Description=\"Normalized genotype likelihoods\">");

	for (const auto& chr: Chrs) {
		if (chr.inUse()) {
			_vcf.writeNoDelim("##contig=<ID=", chr.name(), ",length=", chr.length(), ">").endln();
		}
	}

	// also write header with sample names
	_vcf.write("#CHROM", "POS", "ID", "REF", "ALT", "QUAL", "FILTER", "INFO", "FORMAT");
	for (const std::string &name : SampleNames) _vcf.write(name);
	_vcf.endln();
}

void TVCFWriter::_writeLikelihood(coretools::HPPhredInt likGlf) {
	if (_usePhred) {
		_vcf.writeNoDelim(coretools::PhredInt(likGlf));
	}
	else {
		if (likGlf == 0)
			_vcf.writeNoDelim(0); // otherwise, it is formated as -0
		else
			_vcf.writeNoDelim(coretools::Log10Probability(likGlf));
	}
}

void TVCFWriter::_writeFixed(std::string_view ChrName, size_t Position, std::string_view ID, std::string_view RefAllele,
								 std::string_view AltAllele, coretools::PhredInt VariantQuality, std::string_view Filter) {
	_vcf.write(ChrName, Position + 1, ID, RefAllele, AltAllele, VariantQuality, Filter);
}

void TVCFWriter::_writeFixed(std::string_view ChrName, size_t Position, std::string_view ID, Base RefAllele,
							 Base AltAllele, coretools::PhredInt VariantQuality, std::string_view Filter) {
	using coretools::index;
	constexpr std::array<std::string_view, 5> bases = {"A", "C", "G", "T", "N"};
	_writeFixed(ChrName, Position, ID, bases[index(RefAllele)], bases[index(AltAllele)], VariantQuality, Filter);
}

void TVCFWriter::writeSite(std::string_view ChrName, size_t Position, Base RefAllele, Base AltAllele,
						   coretools::PhredInt VariantQuality, TConstView<TGLFEntry> Data) {
	_writeFixed(ChrName, Position, ".", RefAllele, AltAllele, VariantQuality, ".");
	impl::writeDepth(Data, _vcf);
	_vcf.write(_format);

	for (const auto &d : Data) {
		if (d.isHaploid()) {
			_writeCell(d.depth, {d[RefAllele], d[AltAllele]});
		} else {
			const auto refHom = genotype(RefAllele, RefAllele);
			const auto het    = genotype(RefAllele, AltAllele);
			const auto altHom = genotype(AltAllele, AltAllele);
			_writeCell(d.depth, {d[refHom], d[het], d[altHom]});
		}
	}
	_vcf.endln();
}

void TVCFWriter::writeSite(std::string_view ChrName, size_t Position, Base RefAllele, Base AltAllele,
						   coretools::PhredInt VariantQuality, TConstView<TVCFEntry> Data) {
	_writeFixed(ChrName, Position, ".", RefAllele, AltAllele, VariantQuality, ".");
	impl::writeDepth(Data, _vcf);
	_vcf.write(_format);

	for (const auto &d : Data) {
		if (d.isHaploid()) {
			_writeCell(d.depth, {d[Haploid::first], d[Haploid::second]});
		} else {
			_writeCell(d.depth, {d[Diploid::homoFirst], d[Diploid::het], d[Diploid::homoSecond]});
		}
	}
	_vcf.endln();
}

coretools::HPPhredInt TVCFWriter::_writeGenotypeAndQuality(TConstView<coretools::HPPhredInt> GTL) {
	assert(GTL.size() == 2 || GTL.size() == 3);
	const auto genotypeStrings =
		(GTL.size() == 2) ? std::array<std::string, 3>{"0", "1"} : std::array<std::string, 3>{"0/0", "0/1", "1/1"};

	const auto min     = std::min_element(GTL.cbegin(), GTL.cend());
	const auto minQual = *min;
	const auto in      = std::distance(GTL.cbegin(), min);

	// get all genotypes with minQual (=MLE)
	std::array<size_t, 3> mleGenotypes;
	size_t mleSize = 0;
	for (size_t i = 0; i < GTL.size(); ++i) {
		if (GTL[i] == minQual) {
			mleGenotypes[mleSize] = i;
			++mleSize;
		}
	}

	// write genotype quality
	if (mleSize > 1) {
		const auto mleGeno = mleGenotypes[randomGenerator().sample(mleSize)];
		_vcf.writeNoDelim(genotypeStrings[mleGeno], ":0:");
	} else {
		auto slq = impl::getSecondHighestGTL(in, GTL);
		slq.scale(minQual);
		_vcf.writeNoDelim(genotypeStrings[mleGenotypes.front()], ':', coretools::PhredInt(slq), ':');
	}

	return minQual;
}

void TVCFWriter::_writeCell(size_t Depth, TConstView<coretools::HPPhredInt> GTL) {
	assert(GTL.size() == 2 || GTL.size() == 3);
	if (Depth == 0) {
		if (GTL.size() == 3) { // diploid
			_vcf.write("./.:.:.:.");
		}
		else { // haploid
			_vcf.write(".:.:.:.");
		}
		return;
	}

	const auto minQual = _writeGenotypeAndQuality(GTL);

	// write depth
	_vcf.writeNoDelim(Depth, ':');

	// write likelihoods
	_writeLikelihood(coretools::HPPhredInt(GTL[0] - minQual));
	for (size_t g = 1; g < GTL.size(); g++) {
		_vcf.writeNoDelim(',');
		_writeLikelihood(coretools::HPPhredInt(GTL[g] - minQual));
	}
	_vcf.writeDelim();
}


} // namespace genometools
