//
// Created by caduffm on 3/10/22.
//

#ifndef ATLAS_TPOPULATION_H
#define ATLAS_TPOPULATION_H

#include <cstdint>
#include <vector>
#include <string>


//------------------------------------------------
// TPopulation
//------------------------------------------------

namespace genometools {

class TPopulation {
private:
	std::string _name;
	std::vector<std::string> _samples;
	size_t _firstSampleIndex = 0;

public:
	TPopulation(std::string Name): _name(std::move(Name)) {};

	const std::string& name() const { return _name; };

	bool operator==(std::string_view Name) const { return Name == _name; };

	size_t numSamples() const { return _samples.size(); };

	void addSample(std::string_view Sample);
	void addSamples(const std::vector<std::string> &Samples);
	bool sampleExists(std::string_view Sample) const;

	void setFirstSampleIndex(size_t Index) { _firstSampleIndex = Index; };

	size_t firstSampleIndex() const { return _firstSampleIndex; };

	bool sampleIndexExists(size_t Index) const;
	size_t sampleIndex(std::string_view Sample) const;
	const std::string& sampleName(size_t Index) const;
	const std::vector<std::string> &sampleNames() const;
	void addSampleNamesToVector(std::vector<std::string> &vec) const;
	void report() const;

	// the following functions accept arrays spanning ALL samples and perform calculations on samples in population
	size_t numSamplesMissing(bool *sampleMissing) const;

	size_t numSamplesWithData(bool *sampleMissing) const { return numSamples() - numSamplesMissing(sampleMissing); };
};

//------------------------------------------------
// TPopulationSamples
//------------------------------------------------
class TPopulationSamples {
private:
	// populations
	std::vector<TPopulation> _populations;
	size_t _numSamples = 0;

	// VCF index: maps index in VCF to internal index, which is ordered by population
	struct vcfInfo {
		bool used;
		size_t index;

		vcfInfo() {
			used  = false;
			index = 0;
		}
	};
	std::vector<vcfInfo> _vcfIndex;
	std::vector<size_t> _indexToVCFIndex;
	std::vector<size_t> _indexToPopulationIndex;

	void _fillIndexToPopulationIndex();
	void _finalizeReadingSamples();

public:
	TPopulationSamples() = default;
	TPopulationSamples(std::string_view Filename);
	TPopulationSamples(const std::vector<std::string> &SampleNames);
	TPopulationSamples(const std::vector<std::string> &PopulationNames,
	                   const std::vector<std::vector<std::string>> &SampleNames);

	bool hasSamples() const { return _numSamples > 0; };

	size_t numPopulations() const { return _populations.size(); };
	bool populationExists(std::string_view name) const;
	const std::string& getPopulationName(size_t index) const;
	size_t populationIndex(std::string_view name) const;
	size_t populationIndex(size_t SampleIndex) const { return _indexToPopulationIndex[SampleIndex]; };
	size_t numSamplesInPop(size_t population) const { return _populations[population].numSamples(); };

	size_t numSamples() const { return _numSamples; };
	bool sampleExists(std::string_view name) const;
	size_t sampleIndex(std::string_view name) const;
	const std::string& sampleName(size_t SampleIndex) const;
	std::vector<std::string> sampleNames() const;

	void addSampleNamesToVector(std::vector<std::string> &vec) const;

	void readSamples(std::string_view Filename);
	void readSamplesFromVCFNames(std::vector<std::string> &vcfSampleNames);
	void report();
	void fillVCFOrder(std::vector<std::string> &vcfSampleNames);

	// bool sampleIsUsed(std::string_view  name);
	// size_t getOrderedSampleIndex(std::string_view  name);
	size_t startIndex(int population) { return _populations[population].firstSampleIndex(); };
	size_t sampleIndexInVCF(size_t index);

	uint8_t *getPointerToDataInPop(uint8_t *data, size_t population) const;
	size_t numSamplesMissingInPop(bool *sampleMissing, size_t population) const;
	size_t numSamplesWithDataInPop(bool *sampleMissing, size_t population) const;
};


}; // end namespace genometools

#endif // ATLAS_TPOPULATION_H
