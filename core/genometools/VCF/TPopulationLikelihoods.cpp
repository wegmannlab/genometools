//
// Created by caduffm on 3/10/22.
//

#include "genometools/VCF/TPopulationLikelihoods.h"
#include "coretools/Files/TInputFile.h"
#include "coretools/Files/TOutputFile.h"
#include "coretools/Types/probability.h"

namespace genometools {

using coretools::str::toString;
using namespace coretools::instances;

//-------------------------------------------------
// TPopulationLikelihoodReader
//-------------------------------------------------

TPopulationLikelihoodReader::TPopulationLikelihoodReader(bool saveAlleleFreq) { initialize(saveAlleleFreq); }

void TPopulationLikelihoodReader::_readWindowSettings() {
	//  do we limit the lines to read?
	_limitLines = parameters().get("limitLines", 0);
	if (_limitLines > 0) {
		logfile().list("Will only read the first ", _limitLines, " lines of the VCF file. (parameter 'limitLines')");
	}

	// limit to sites in bed file?
	if (parameters().exists("regions")) {
		std::string filename = parameters().get("regions");
		logfile().listFlush("Limiting analysis to regions defined in BED file '", filename,
		                    "' (parameter regions) ...");
		_bedFile.parse(filename);
		logfile().done();
		logfile().conclude("Read ", _bedFile.size(), " of cumulative length ", _bedFile.length(), " bp on ",
		                   _bedFile.NChrWindows(), " chromosomes.");
		_limitToSitesInBed = true;
		_curBedWindow = _bedFile.begin();
	}

	// report to logfile
	if (_limitLines > 0 && _limitToSitesInBed) {
		logfile().warning(
		    "Conflicting parameters 'limitLines' and 'regions': Will only consider regions within the first ",
		    _limitLines, " lines of the VCF file.");
	} else if (_limitLines == 0 && !_limitToSitesInBed) {
		logfile().list("Will parse entire VCF. (use 'limitLines' or 'regions' to change)");
	}
}

void TPopulationLikelihoodReader::_readDepthSettings() {
	// do we set a depth filter?
	if (parameters().exists("filterDepth")) {
		parameters().fill("filterDepth", _depthFilter);
		logfile().list("Will set samples with a sequencing depth outside ", _depthFilter,
		               " as missing. (parameters 'filterDepth')");
	} else {
		_depthFilter.set("[1,]"); // depth = 0 is missing anyways
		logfile().list("Will keep sites regardless of depth. (use 'filterDepth' to filter)");
	}
}

void TPopulationLikelihoodReader::_readMissingSettings() {
	_maxMissing = parameters().get<coretools::Probability>("maxMissing", coretools::P(1.0));
	if (_maxMissing < 1.0) {
		logfile().list("Will filter out sites with a missing data fraction > ", _maxMissing,
		               ". (parameter 'maxMissing')");
	} else {
		logfile().list("Will keep sites regardless of missingness. (use 'maxMissing' to filter)");
	}
}

void TPopulationLikelihoodReader::_readMAFSettings(bool saveAlleleFreq) {
	// parameters to set a filter on the minor allele frequency?
	_minMAF = parameters().get("minMAF", 0.0);
	if (_minMAF < 0.0 || _minMAF >= 0.5) UERROR("MAF filter must be within [0.0,0.5)!");
	if (_minMAF > 0.0 || saveAlleleFreq) {
		_doEM = true;
		_epsF = parameters().get("epsF", 1e-07);
	}

	// report to logfile
	if (_minMAF > 0.0) {
		logfile().list("Will filter on a minor allele frequency of ", _minMAF, ". (parameter 'minMAF')");
	} else {
		logfile().list("Will keep sites regardless of their minor allele frequency. (use 'minMAF' to filter)");
	}
}

void TPopulationLikelihoodReader::_readVariantQualitySettings() {
	// filter on variant quality?
	_minVariantQuality = parameters().get("minVarQual", 0);
	if (_minVariantQuality > 0) {
		logfile().list("Will only keep sites with variant quality >= " + toString(_minVariantQuality) +
		               ". (parameter 'minVarQual')");
	} else {
		logfile().list("Will keep sites regardless of their variant quality. (use 'minVarQual' to filter)");
	}
}

void TPopulationLikelihoodReader::_readChrSettings() {
	if (parameters().exists("chr")) {
		std::string argument = parameters().get<std::string>("chr");
		if (coretools::str::stringContains(argument, ".txt")) { // specified as a file name
			logfile().listFlush("Reading chromosomes from file '" + argument + "' ...");
			for (coretools::TInputFile file(argument, coretools::FileType::NoHeader); !file.empty(); file.popFront()) {
				_chrsToKeep.emplace_back(file.get(0));
			}
			logfile().done();
		} else { // specified as a vector on command line
			logfile().list("Reading chromosomes from command line.");
			coretools::str::fillContainerFromString(argument, _chrsToKeep, ',');
		}

		// write to logfile
		logfile().startIndent("Will keep the following chromosomes:");
		for (const auto &it : _chrsToKeep) logfile().list(it);
		logfile().endIndent();

		_filterOnChr = true;
	} else if (parameters().exists("limitChr")) {
		std::string limitName = parameters().get<std::string>("limitChr");
		logfile().list("Will limit analysis to all chromosomes up to and including " + limitName +
		               ". (parameter 'limitChr')");
		_chrUpAndIncluding = limitName;
		_limitChr          = true;
	} else {
		logfile().list("Will keep all chromosomes. (use 'chr' or 'limitChr' to filter)");
	}
}

void TPopulationLikelihoodReader::initialize(bool saveAlleleFreq) {
	_readWindowSettings();
	_readDepthSettings();
	_readMissingSettings();
	_readMAFSettings(saveAlleleFreq);
	_readVariantQualitySettings();
	_readChrSettings();

	// set progress frequency
	_progressFrequency = parameters().get("reportFreq", 10000);
	_initialized       = true;
}

void TPopulationLikelihoodReader::_resetCounters() {
	_vcfParsingStarted        = false;
	_lineCounter              = 0;
	_notInBedFile             = 0;
	_notBialleleicCounter     = 0;
	_missingSNPCounter        = 0;
	_lowFreqSNPCounter        = 0;
	_lowVariantQualityCounter = 0;
	_noPLCounter              = 0;
	_numAcceptedLoci          = 0;
	_notOnChrCounter          = 0;
	_curChr                   = "";
}

void TPopulationLikelihoodReader::openVCF(std::string_view vcfFilename) {
	if (!_initialized) { DEVERROR("Can not open VCF: TPopulationLikelihoodReader was never initialized!"); }

	// open input stream
	logfile().startIndent("Reading vcf from file '", vcfFilename, "'.");
	_vcfFile.openStream(vcfFilename);

	// enable parsers
	_vcfFile.enablePositionParsing();
	_vcfFile.enableVariantParsing();
	_vcfFile.enableVariantQualityParsing();
	_vcfFile.enableFormatParsing();
	_vcfFile.enableSampleParsing();

	// reset counters
	_resetCounters();
	logfile().endIndent();
}

int TPopulationLikelihoodReader::_filterOnDepth(TPopulationSamples &samples) {
	int numMissing = 0;
	for (uint32_t s = 0; s < samples.numSamples(); ++s) {
		int vcfIndex = samples.sampleIndexInVCF(s);

		// depth filter: if a locus has < minDepth reads, flag locus as missing (set all genotype likelihoods = 1)
		if (_depthFilter.outside(_vcfFile.sampleDepth(vcfIndex))) {
			_vcfFile.setSampleMissing(vcfIndex);
			++numMissing;
		}
	}

	return numMissing;
}

bool TPopulationLikelihoodReader::_readNextLineFromVCF() {
	++_lineCounter;

	// print progress
	if (_lineCounter % _progressFrequency == 0) { _printProgressFrequencyFiltering(); }

	// limit lines
	if (_limitLines > 0 && _lineCounter > _limitLines) {
		logfile().list("Reached limit of " + toString(_limitLines) + " lines.");
		return false;
	}

	// read next line
	if (!_vcfFile.next()) return false;

	// all fine!
	return true;
}

void TPopulationLikelihoodReader::_removeParsedChr() {
	auto it = std::find(_chrsToKeep.begin(), _chrsToKeep.end(), _curChr);
	if (it != _chrsToKeep.end()) { _chrsToKeep.erase(it); }
}

bool TPopulationLikelihoodReader::_updateChromosomeInfo() {
	// function called when VCF is on new chromosome
	// remove previous chromosome from vector _chrsToKeep (we stop when vector is empty)
	_removeParsedChr();
	if (_filterOnChr && _chrsToKeep.empty()) {
		logfile().list("Parsed all chromosomes that were defined with parameter 'chr'.");
		return false;
	}

	if (_limitChr && _curChr == _chrUpAndIncluding && !_curChr.empty()) {
		// read up and including that chromosome
		logfile().list("Parsed all chromosomes up and including", _chrUpAndIncluding, " (parameter 'limitChr')");
		return false;
	}

	// update curChr
	_curChr = _vcfFile.chr();
	return true;
}

bool TPopulationLikelihoodReader::_jumpToNextChromosome() {
	while (!_vcfFile.eof && _vcfFile.chr() == _curChr) {
		_vcfFile.next();
		++_lineCounter;
		++_notInBedFile;
	}

	if (_vcfFile.eof) {
		_removeParsedChr();
		return false;
	} else {
		return _updateChromosomeInfo();
	}
}

void TPopulationLikelihoodReader::_printProgressFrequencyFiltering() const {
	logfile().list("Parsed " + toString(_lineCounter - 1) + " lines, retained " + toString(_numAcceptedLoci) +
	               " loci in " + _timer.formattedTime() + ".");
}

void TPopulationLikelihoodReader::concludeFilters() const {
	_printProgressFrequencyFiltering();
	if (_notInBedFile > 0)
		logfile().conclude(toString(_notInBedFile) + " loci were not in considered regions (BED file).");
	if (_notBialleleicCounter > 0) logfile().conclude(toString(_notBialleleicCounter) + " loci were not bi-allelic.");
	if (_lowVariantQualityCounter > 0)
		logfile().conclude(toString(_lowVariantQualityCounter) + " loci had variant quality < " +
		                   toString(_minVariantQuality) + ".");
	if (_noPLCounter > 0) logfile().conclude(toString(_noPLCounter) + " loci had no PL or GL field.");
	if (_missingSNPCounter > 0)
		logfile().conclude(toString(_missingSNPCounter) + " loci had > " + toString(_maxMissing * 100) +
		                   "% of missing samples.");
	if (_lowFreqSNPCounter > 0)
		logfile().conclude(toString(_lowFreqSNPCounter) + " loci had MAF < " + toString(_minMAF) + ".");
	if (_notOnChrCounter > 0)
		logfile().conclude(toString(_notOnChrCounter) + " loci were on other chromosomes than specified.");
}

//-----------------------------------
// TPopulationLikelihoodReaderLocus
//-----------------------------------

TPopulationLikelihoodReaderLocus::TPopulationLikelihoodReaderLocus(bool saveAlleleFreq)
    : TPopulationLikelihoodReader(saveAlleleFreq) {}

void TPopulationLikelihoodReaderLocus::initialize(bool saveAlleleFreq) {
	TPopulationLikelihoodReader::initialize(saveAlleleFreq);

	// open true allele freq file
	if (parameters().exists("trueAlleleFreq")) {
		openTrueAlleleFrequenciesFile(parameters().get<std::string>("trueAlleleFreq"));
	}
}

void TPopulationLikelihoodReaderLocus::openTrueAlleleFrequenciesFile(std::string_view Filename) {
	logfile().listFlush("Reading true allele frequencies from file '", Filename, "' ...");
	_trueFreqFile.open(Filename, coretools::FileType::NoHeader); // expect exactly three columns
	if (_trueFreqFile.numCols() != 3) UERROR("File '", Filename, "' was expected to have exactly three columns!");
	logfile().done();
}

std::tuple<std::string, size_t, coretools::Probability> TPopulationLikelihoodReaderLocus::_readLineFreqFile() {
	const auto chr  = _trueFreqFile.get<std::string>(0);
	const auto pos  = _trueFreqFile.get<size_t>(1);
	const auto freq = _trueFreqFile.get<coretools::Probability>(2);
	_trueFreqFile.popFront();
	return {chr, pos, freq};
}

bool TPopulationLikelihoodReaderLocus::_readNextLineFromVCF() {
	if (!TPopulationLikelihoodReader::_readNextLineFromVCF()) return false;

	// read true allele frequency
	// file is assumed to have exact same dimension: always read next line!
	if (_storeTrueFreq) {
		auto [chr, pos, freq] = _readLineFreqFile();

		// check if positions match (allele file is 0-based)
		while (pos < _vcfFile.position() - 1) { std::tie(chr, pos, freq) = _readLineFreqFile(); }
		if (pos > _vcfFile.position() - 1)
			UERROR("current vcf pos=", _vcfFile.position(), " is not equal to current trueAlleleFreq position=", pos);
	}
	return true;
}

void TPopulationLikelihoodReaderLocus::writePosition(coretools::TOutputFile &out) {
	out.write(_vcfFile.chr(), _vcfFile.position(), _vcfFile.getRefAllele(), _vcfFile.getFirstAltAllele());
}

std::vector<BiallelicGenotype>
TPopulationLikelihoodReaderLocus::biallelicGenotypes(TPopulationSamples &samples) const {
	std::vector<BiallelicGenotype> vec(samples.numSamples());
	for (uint32_t s = 0; s < samples.numSamples(); ++s) {
		uint32_t vcfIndex = samples.sampleIndexInVCF(s);
		vec[s]            = _vcfFile.sampleBiallelicGenotype(vcfIndex);
	}
	return vec;
}

BiallelicGenotype TPopulationLikelihoodReaderLocus::biallelicGenotype(TPopulationSamples &samples,
                                                                                   uint32_t s) const {
	uint32_t vcfIndex = samples.sampleIndexInVCF(s);
	return _vcfFile.sampleBiallelicGenotype(vcfIndex);
}

Genotype TPopulationLikelihoodReaderLocus::genotype(TPopulationSamples &samples, uint32_t s) const {
	uint32_t vcfIndex = samples.sampleIndexInVCF(s);
	return _vcfFile.sampleGenotype(vcfIndex);
}

double TPopulationLikelihoodReaderLocus::depth(TPopulationSamples &samples, uint32_t s) {
	uint32_t vcfIndex = samples.sampleIndexInVCF(s);
	return _vcfFile.sampleDepth(vcfIndex);
}

bool TPopulationLikelihoodReaderLocus::sampleIsMissing(TPopulationSamples &samples, uint32_t s) {
	uint32_t vcfIndex = samples.sampleIndexInVCF(s);
	return _vcfFile.sampleIsMissing(vcfIndex);
}

//-----------------------------------
// TPopulationLikelihoodReaderWindow
//-----------------------------------

TPopulationLikelihoodReaderWindow::TPopulationLikelihoodReaderWindow(bool saveAlleleFreq)
    : TPopulationLikelihoodReader(saveAlleleFreq) {}

bool TPopulationLikelihoodReaderWindow::_readNextLocusAndUpdateChromosome() {
	// read next locus
	if (!_readNextLineFromVCF()) return false; // reached end of VCF

	// update chr
	if (_curChr != _vcfFile.chr()) {
		if (!_updateChromosomeInfo()) { return false; }

		// jump to next chromosome until we are on a chromsome with BED regions
		while (_bedFile.empty(_curChr)) {
			if (!_jumpToNextChromosome()) { return false; }
		}

		// get ref id of this chromosome
		_curRefId = _bedFile.refID(_curChr);
	}

	return true;
}

void TPopulationLikelihoodReaderWindow::writeWindow(coretools::TOutputFile &out) {
	out.write(_bedFile.name(_curBedWindow->refID()),
			  _curBedWindow->from().position(),
			  _curBedWindow->to().position());
};

}; // end namespace genometools
