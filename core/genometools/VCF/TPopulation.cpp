//
// Created by caduffm on 3/10/22.
//

#include "genometools/VCF/TPopulation.h"
#include "genometools/DefaultColumnNames.h"

#include <algorithm>
#include <set>

#include "coretools/Main/TError.h"
#include "coretools/Main/TLog.h"
#include "coretools/Files/TInputFile.h"

namespace genometools {

//------------------------------------------------
// TPopulation
//------------------------------------------------
void TPopulation::addSample(std::string_view Sample) {
	// check for duplicates
	if (std::find(_samples.begin(), _samples.end(), Sample) != _samples.end()) {
		UERROR("Duplicate sample '", Sample, "' in population '", _name, "'!");
	}
	_samples.emplace_back(Sample);
};

void TPopulation::addSamples(const std::vector<std::string> &Samples){
	// check for duplicates
	std::set<std::string> s(Samples.begin(), Samples.end());
	if (s.size() != Samples.size()){
		UERROR("Duplicate samples in population '", _name, "'!");
	}
	_samples.insert(_samples.end(), Samples.begin(), Samples.end());
};

bool TPopulation::sampleExists(std::string_view Sample) const {
	return std::find(_samples.begin(), _samples.end(), Sample) != _samples.end();
};

bool TPopulation::sampleIndexExists(size_t Index) const {
	return Index >= _firstSampleIndex && Index < _firstSampleIndex + _samples.size();
};

size_t TPopulation::sampleIndex(std::string_view Sample) const {
	for (size_t i = 0; i < _samples.size(); ++i) {
		if (_samples[i] == Sample) { return _firstSampleIndex + i; }
	}
	DEVERROR("sample '", Sample, "' does not exist!");
};

const std::string &TPopulation::sampleName(size_t Index) const { return _samples[Index - _firstSampleIndex]; };

const std::vector<std::string> &TPopulation::sampleNames() const { return _samples; };

void TPopulation::addSampleNamesToVector(std::vector<std::string> &vec) const {
	vec.insert(vec.end(), _samples.begin(), _samples.end());
};

void TPopulation::report() const {
	for (auto &s : _samples) { coretools::instances::logfile().list(s); }
};

// the following functions accept arrays spanning ALL samples and perform calculations on samples in population
size_t TPopulation::numSamplesMissing(bool *sampleMissing) const {
	size_t numMissing = 0;
	for (size_t s = 0; s < numSamples(); ++s) {
		if (sampleMissing[_firstSampleIndex + s]) numMissing++;
	}
	return numMissing;
};

////////////////////////////////////////////////////////////////////////////////////////////////
// TPopulationSamples                                                                         //
////////////////////////////////////////////////////////////////////////////////////////////////

TPopulationSamples::TPopulationSamples(std::string_view Filename) {
	readSamples(Filename);
};

TPopulationSamples::TPopulationSamples(const std::vector<std::string> &SampleNames) {
	// single population
	if (SampleNames.empty()) { DEVERROR("Vector of sample names is empty!"); }

	_populations.emplace_back("Population");
	_populations.back().addSamples(SampleNames);

	_finalizeReadingSamples();
}

TPopulationSamples::TPopulationSamples(const std::vector<std::string> &PopulationNames,
                                       const std::vector<std::vector<std::string>> &SampleNames) {
	// multiple populations
	if (PopulationNames.empty()) { DEVERROR("Vector of population names is empty!"); }
	if (PopulationNames.size() != SampleNames.size()) {
		DEVERROR("Size of vectors do not match (", PopulationNames.size(), " vs ", SampleNames.size(), ")!");
	}
	for (size_t i = 0; i < PopulationNames.size(); i++) {
		_populations.emplace_back(PopulationNames[i]);
		_populations.back().addSamples(SampleNames[i]);
	}

	_finalizeReadingSamples();
}

bool TPopulationSamples::populationExists(std::string_view name) const {
	return std::find(_populations.begin(), _populations.end(), name) != _populations.end();
};

const std::string& TPopulationSamples::getPopulationName(size_t index) const {
	if (index >= _populations.size()) { UERROR("No population with index ", index, "!"); }
	return _populations[index].name();
};

size_t TPopulationSamples::populationIndex(std::string_view name) const {
	for (size_t p = 0; p < _populations.size(); ++p) {
		if (_populations[p] == name) { return p; }
	}
	UERROR("No population with name ", name, "!");
};

void TPopulationSamples::_finalizeReadingSamples(){
	_numSamples = 0;
	_indexToPopulationIndex.clear();
	for (auto &p : _populations) {
		p.setFirstSampleIndex(_numSamples);
		_numSamples += p.numSamples();
	}
	_fillIndexToPopulationIndex();

	report();
}

bool TPopulationSamples::sampleExists(std::string_view name) const {
	for (auto &p : _populations) {
		if (p.sampleExists(name)) { return true; }
	}
	return false;
};

size_t TPopulationSamples::sampleIndex(std::string_view name) const {
	for (auto &p : _populations) {
		if (p.sampleExists(name)) { return p.sampleIndex(name); }
	}
	DEVERROR("Sample '", name, "' does not exist!");
};

const std::string& TPopulationSamples::sampleName(size_t index) const {
	if (index >= _numSamples) { DEVERROR("index >= _numSamples!"); }
	for (auto &p : _populations) {
		if (p.sampleIndexExists(index)) { return p.sampleName(index); }
	}
	DEVERROR("index not found!");
};

std::vector<std::string> TPopulationSamples::sampleNames() const {
	std::vector<std::string> sampleNames;
	for (auto &p : _populations) {
		sampleNames.insert(sampleNames.end(), p.sampleNames().begin(), p.sampleNames().end());
	}
	return sampleNames;
}

void TPopulationSamples::addSampleNamesToVector(std::vector<std::string> &vec) const {
	for (auto &p : _populations) { p.addSampleNamesToVector(vec); }
};

void TPopulationSamples::readSamples(std::string_view Filename) {
	coretools::instances::logfile().listFlush("Reading samples from file '", Filename, "' ...");

	// open file
    coretools::TInputFile in(Filename, coretools::FileType::Header);

    // read file and add sites
	bool hasPopulationName = false;
	
    std::array<size_t, 3> idx;
    idx[0] = in.indexOfFirstMatch(defaultColumnNames_sample);	
	if(in.hasOneOfTheseIndeces(defaultColumnNames_population)){
		idx[1] = in.indexOfFirstMatch(defaultColumnNames_population);
		hasPopulationName = true;
	} else {
		_populations.emplace_back(defaultColumnNames_population[0]);
	}

    // read all sites in subset
    for (; !in.empty(); in.popFront()) {    
		// add sample to correct population
		if (hasPopulationName) {
			const std::string pop = in.get<std::string>(idx[1]);
			auto it = std::find(_populations.begin(), _populations.end(), pop);
			if (it == _populations.end()) {
				it = _populations.emplace(_populations.end(), pop);
			}
			it->addSample(in.get(idx[0]));
		} else {
			_populations[0].addSample(in.get(idx[0]));
		}
	}

	// close file
	coretools::instances::logfile().done();

	_finalizeReadingSamples();
};

void TPopulationSamples::readSamplesFromVCFNames(std::vector<std::string> &vcfSampleNames) {
	// create a single populations
	_populations.emplace_back("Population");

	// add samples to that population
	for (size_t s = 0; s < vcfSampleNames.size(); ++s) { _populations[0].addSample(vcfSampleNames[s]); }
	_numSamples = _populations[0].numSamples();
	_fillIndexToPopulationIndex();

	fillVCFOrder(vcfSampleNames);
};

void TPopulationSamples::report() {
	coretools::instances::logfile().startIndent("Will consider the following populations:");
	for (auto &p : _populations) {
		coretools::instances::logfile().startIndent("Population '", p.name(), "':");
		p.report();
		coretools::instances::logfile().endIndent();
	}
	coretools::instances::logfile().endIndent();
};

void TPopulationSamples::fillVCFOrder(std::vector<std::string> &vcfSampleNames) {
	_vcfIndex.resize(vcfSampleNames.size());
	_indexToVCFIndex.resize(vcfSampleNames.size());

	std::vector<bool> sampleInVCF(_numSamples);

	for (size_t i = 0; i < vcfSampleNames.size(); ++i) {
		// check if sample is used (i.e. exists in one of the populations)
		if (sampleExists(vcfSampleNames[i])) {
			if (_vcfIndex[i].used) { UERROR("Duplicate sample name '", vcfSampleNames[i], "' in VCf header!"); }

			size_t index          = sampleIndex(vcfSampleNames[i]);
			_indexToVCFIndex[index] = i;
			sampleInVCF[index]      = true;

			_vcfIndex[i].used  = true;
			_vcfIndex[i].index = index;
		}
	}

	// check if we lack samples
	for (size_t s = 0; s < _numSamples; ++s) {
		if (!sampleInVCF[s]) UERROR("Sample '", sampleName(s), "' missing in VCF!");
	}
};

void TPopulationSamples::_fillIndexToPopulationIndex() {
	_indexToPopulationIndex.resize(_numSamples);
	for (size_t p = 0; p < _populations.size(); ++p) {
		for (size_t y = 0; y < _populations[p].numSamples(); ++y) {
			_indexToPopulationIndex[_populations[p].firstSampleIndex() + y] = p;
		}
	}
};

size_t TPopulationSamples::sampleIndexInVCF(size_t index) { return _indexToVCFIndex[index]; };

uint8_t *TPopulationSamples::getPointerToDataInPop(uint8_t *data, size_t population) const {
	return &data[3 * _populations[population].firstSampleIndex()];
};

size_t TPopulationSamples::numSamplesMissingInPop(bool *sampleMissing, size_t population) const {
	return _populations[population].numSamplesMissing(sampleMissing);
};

size_t TPopulationSamples::numSamplesWithDataInPop(bool *sampleMissing, size_t population) const {
	return _populations[population].numSamplesWithData(sampleMissing);
};

} // namespace genometools
