/*
 * TVcfParser.cpp
 *
 *  Created on: Jun 15, 2011
 *      Author: wegmannd
 */

#include "genometools/VCF/TVcfParser.h"
#include <algorithm>
#include <iterator>

namespace genometools {

namespace impl {

VCF_TYPE getTypefromString(std::string_view s) {
	if (s == "Integer") return VCF_TYPE::INTEGER;
	if (s == "Float") return VCF_TYPE::FLOAT;
	if (s == "Flag") return VCF_TYPE::FLAG;
	if (s == "Character") return VCF_TYPE::CHAR;
	if (s == "String") return VCF_TYPE::STRING;

	return VCF_TYPE::UNKNOWN;
};

std::string getStringfromType(VCF_TYPE type) {
	if (type == VCF_TYPE::INTEGER) return "Integer";
	if (type == VCF_TYPE::FLOAT) return "Float";
	if (type == VCF_TYPE::FLAG) return "Flag";
	if (type == VCF_TYPE::CHAR) return "Character";
	if (type == VCF_TYPE::STRING) return "String";

	return "?";
};
} // namespace impl

//--------------------------------------------------------------------
TVcfHeaderLine::TVcfHeaderLine(std::string_view str) {
	std::string Line(str);
	int pp           = Line.find_first_of('<');
	Line.erase(0, pp + 1);
	Line = coretools::str::extractBefore(Line, '>');
	while (!Line.empty()) {
		std::string temp = coretools::str::extractBefore(Line, ',');
		if (temp.find("=\"") > 0) {
			// contains quotes. Remove quotes and check for an even occurrence
			int numq = 0;
			pp       = temp.find_first_of('"');
			while (pp >= 0) {
				++numq;
				temp.erase(pp, 1);
				pp = temp.find_first_of('"');
			}
			if (numq == 1 || numq == 3) temp = temp + coretools::str::extractBefore(Line, '"');
			Line.erase(0, 1);
		}

		// get tag
		std::string tag = coretools::str::extractBefore(temp, '=');
		temp.erase(0, 1);

		// check tag
		if (tag == "ID")
			_id = temp;
		else if (tag == "Number") {
			numberString = temp;
			if (temp == ".") number = 99999;
			// else number=temp.toInt();
			else
				number = 88888;
		} else if (tag == "Type") {
			typeString = temp;
			type       = impl::getTypefromString(typeString);
			if (type == VCF_TYPE::UNKNOWN) UERROR("Error when parsing vcf header, unknown 'Type' in line '", Line, "'!");
		} else if (tag == "Description")
			desc = temp;
		else if (tag == "##INFO")
			continue; // HACK to fix an error in a previous version of Atlas.
		else
			UERROR("Error when parsing vcf header, unknown tag '", tag, "' in line '", Line, "'!");
	}

	// throw error
	if (type != VCF_TYPE::FLAG && number < 1) UERROR("Error when parsing vcf header, unknown 'Number' in line '", Line, "'!");
	if (_id.empty() || number == -1 || type == VCF_TYPE::UNKNOWN || desc.empty()) {
		std::string errorMessage = "Error when parsing vcf header, missing tag in line '" + Line + "':";
		if (_id.empty()) errorMessage += " id is empty!";
		if (number < 1) errorMessage += " number is not a number!";
		if (type == VCF_TYPE::UNKNOWN) errorMessage += " unknown type!";
		if (desc.empty()) errorMessage += " description is empty!";
		UERROR(errorMessage);
	}
};

TVcfHeaderLine::TVcfHeaderLine(std::string_view ID, std::string_view Number, VCF_TYPE Type, std::string_view Desc) {
	_id          = ID;
	numberString = Number;
	if (numberString == ".")
		number = 99999;
	else
		number = coretools::str::fromString<int>(Number);
	type       = Type;
	typeString = impl::getStringfromType(type);
	desc       = Desc;
	if (type != VCF_TYPE::FLAG && number < 1) UERROR("Error when creating new vcf header line, unknown 'Number' entry '", Number, "'!");
};

void TVcfHeaderLine::update(std::string_view Number, VCF_TYPE Type, std::string_view Desc) {
	numberString = Number;
	if (numberString == ".")
		number = 99999;
	else
		number = coretools::str::fromString<int>(Number);
	type       = Type;
	typeString = impl::getStringfromType(type);
	desc       = Desc;
	if (type != VCF_TYPE::FLAG && number < 1) UERROR("Error when creating new vcf header line, unknown 'Number' entry '", Number, "'!");
};


std::string TVcfHeaderLine::getString() const {
	return "<ID=" + _id + ",Number=" + numberString + ",Type=" + typeString + ",Description=\"" + desc + "\">";
};
//--------------------------------------------------------------------
/*
TVcfFilter::TVcfFilter(std::string filter, long* CurrentLine){
    currentLine=CurrentLine;
    //has the format tag<val or tag>val or tag.sub<val or tag.sub>val
    if(filter.contains('>')) larger=true;
    else if(filter.contains('<')) larger=false;
    else throw "Filter '" + filter + "' is missing a '>' or '<' sign!";

    //get value
    std::string temp;
    if(larger) temp=filter.extract_after('>');
    else temp=filter.extract_after('<');
    if(!temp.isNumber()) throw "In filter '" + filter + "', the value '"+temp+"' is not a number!";
    val=temp.toDouble();

    //get field
    if(larger) temp=filter.extract_before('>');
    else temp=filter.extract_before('<');
    if(temp.contains('.')){
        sub=true;
        tag=temp.extract_before('.');
        subTag=temp.extract_after('.');
    } else {
        sub=false;
        tag=temp;
    }
};

bool TVcfFilter::pass(TVcfFormat* format, std::vector<std::string>* data){
    std::string buf=data->at(format->getCol(tag));
    float d;
    if(sub){
        //parse buf. Format is sub=data,sub2=data2 ...
        std::string temp;
        bool found=false;
        while(!buf.empty()){
            temp=buf.extract_sub_str(',');
            if(temp.contains('=') && temp.extract_before('=')==subTag){
                found=true;
                break;
            }
        }
        if(!found) throw "Sub-tag '"+subTag+"' missing in VCF file on line " +(std::string) *currentLine + "!";
        d=temp.extract_after('=').toDouble();
    } else d=buf.toDouble();

    if(larger && d>val) return true;
    else if (!larger && d<val) return true;
    return false;
};

void TVcfFilter::print(){
    if(sub) cout << "    -> Skipping calls where entry " << subTag << " in column " << tag;
    else cout << "    -> Skipping calls where column " << tag;
    if(larger) cout << " > ";
    else cout << " < ";
    cout << val << endl;
};
*/
//--------------------------------------------------------------------
/*
void TVcfSample::filter(TVcfFilter* filter){
    if(!missing){
        if(!filter->pass(format, &data))
            missing=false;
    }
};
*/
//--------------------------------------------------------------------
// TVcfSample
//--------------------------------------------------------------------


void TVcfSample::setGenotype(int firstAllele, int secondAllele) {
	genotype.first  = firstAllele;
	genotype.second = secondAllele;
	missing         = false;
	hasGenotype     = true;
	isHaploid       = false;
};

void TVcfSample::setGenotype(int haploidAllele) {
	genotype.first  = haploidAllele;
	genotype.second = 0;
	missing         = false;
	hasGenotype     = true;
	isHaploid       = true;
};

void TVcfSample::setMissingGenotype() {
	genotype.first  = 0;
	genotype.second = 0;
	missing         = true;
	hasGenotype     = false;
};

bool TVcfSample::parse(std::string_view  s, int genotypeCol) {
	// parse into std::vector (split by ':')
	readData(s);

	// get genotype
	if (genotypeCol < 0) {
		setMissingGenotype();
	} else {
		std::string gt = getCol(genotypeCol);
		// check if data is missing: GT is either "." or "./." or ".|."
		if (gt == ".") {
			setMissingGenotype();
			isHaploid = true;
		} else if (gt == "./." || gt == ".|.") {
			setMissingGenotype();
			isHaploid = false;
		} else if (gt.length() == 1) {
			setGenotype(coretools::str::fromString<int>(gt));
		} else if (gt.length() == 3 && (gt[1] == '/' || gt[1] == '|')) {
			setGenotype(gt[0] - '0', gt[2] - '0'); // turn into int by removing char of 0
		} else {
			setMissingGenotype();
			isHaploid = false;
			// return false;
		}
	}

	return true;
};

bool TVcfSample::checkGenotype(int max) const {
	if (genotype.first < 0 || genotype.second < 0 || genotype.first > max || genotype.second > max) return false;
	return true;
};

void TVcfSample::write(std::ostream &out, unsigned int numFields) const {
	if (missing) {
		out << "\t./.";
		for (size_t i = 1; i < numFields; ++i) out << ":.";
	} else {
		out << "\t";
		out << data.front();
		for (auto it = data.begin() + 1; it != data.end(); ++it) {
			out << ":";
			out << *it;
		}
	}
};

//--------------------------------------------------------------------
// TVcfLine
//--------------------------------------------------------------------
TVcfLine::TVcfLine(std::string_view line, unsigned int numCols, long LineNumber) { update(line, numCols, LineNumber); };

void TVcfLine::update(std::string_view line, unsigned int numCols, long LineNumber) {
	positionParsed = false;
	variantParsed  = false;
	idParsed       = false;
	filterParsed   = false;
	qualityParsed  = false;
	infoParsed     = false;
	formatParsed   = false;
	samplesParsed  = false;
	lineNumber     = LineNumber;
	pos            = -1;
	chr.clear();
	variants.clear();
	info.clear();
	formatOrdered.clear();
	format.clear();
	samples.clear();
	id.clear();
	qual.clear();
	filter.clear();

	// now read new data
	coretools::str::fillContainerFromStringWhiteSpace(line, data);
	if (data.size() != numCols)
		UERROR("Wrong number of columns (", data.size(), " instead of ", numCols, ") in VCF file on line ", lineNumber,
		       "!");
};

bool TVcfLine::variantExists(std::string_view var) const {
	return std::find(variants.begin(), variants.end(), var) != variants.end();
};

void TVcfLine::addVariant(std::string_view var) {
	assert(!variantExists(var));
	variants.emplace_back(var);
};

void TVcfLine::writeVariant(std::ostream &out) const {
	auto it = variants.begin();
	out << *it << "\t";
	if (variants.size() > 1) {
		++it;
		out << *it;
		++it;
		while (it != variants.end()) {
			out << "," << *it;
			++it;
		}
	} else
		out << ".";
};
//--------------------------------------------------------------------
void TVcfParser::parsePosition(TVcfLine &line) {
	// remove 'chr' and turn into int, if possible
	/*int x=line.data[cols.Chr].find("chr");
	  if(x==0) line.data[cols.Chr].remove(x, 3);
	  int chr_int=line.data[cols.Chr].toInt();
	  if(chr_int>0) line.data[cols.Chr]=chr_int; */
	// if(line.chr<=0) throw "Unknown chromosome '" + line.data[cols.Chr] + "' in VCF file on line "
	// +toString(line.lineNumber) + "!";

	// just use string
	line.chr = line.data[cols.Chr];
	line.pos = coretools::str::fromString<uint64_t>(line.data[cols.Pos]);
	assert(line.pos > 0);
	line.positionParsed = true;
};

void TVcfParser::parseVariant(TVcfLine &line) {
	//		std::cout << "parsing " << line.data[cols.Pos] << std::endl;
	// parse reference bases
	line.variants.push_back(line.data[cols.Ref]);

	// alternative bases can be a comma separated list
	std::string buf;
	if (line.data[cols.Alt] != ".") { // only if there are alternative bases
		while (!line.data[cols.Alt].empty()) {
			buf = coretools::str::extractBefore(line.data[cols.Alt], ',');
			line.data[cols.Alt].erase(0, 1);
			if (buf == "<NON_REF>") buf = "X";
			line.addVariant(buf);
		}
	}
	line.variantParsed = true;
};

void TVcfParser::parseQuality(TVcfLine &line) {
	if (line.data[cols.Qual] == ".") {
		line.variantQualityMissing = true;
		line.variantQuality        = -1.0;
	} else {
		line.variantQualityMissing = false;
		line.variantQuality        = coretools::str::fromString<double>(line.data[cols.Qual]);
	}
	line.qualityParsed = true;
};

void TVcfParser::addInfo(std::string_view Line) {
	TVcfHeaderLine l(Line);
	info[l._id] = l;
};
void TVcfParser::updateInfo(std::string_view ID, std::string_view Number, VCF_TYPE Type, std::string_view Desc) {
	// check if id exists
	auto it = info.find(ID);
	if (it == info.end()) {
		// add new info
		TVcfHeaderLine l(ID, Number, Type, Desc);
		info[l._id] = l;
	} else {
		// update
		it->second.update(Number, Type, Desc);
	}
};

void TVcfParser::addFormat(std::string_view Line) {
	TVcfHeaderLine l(Line);
	format[l._id] = l;
};

void TVcfParser::addSample(std::string_view Name) {
	samples.emplace_back(Name);
};

void TVcfParser::updateInfo(TVcfLine &line, std::string_view Id, std::string_view Data) const {
	// update or add?
	auto it = line.info.find(Id);
	if (it == line.info.end()) {
		// add new tag
		if (info.find(Id) == info.end()) UERROR("Can not modify info, unknown info entry '", Id, "'!");
		line.info[std::string{Id}] = std::vector<std::string>();
		it            = line.info.find(Id);
		it->second.emplace_back(Data);
	} else {
		// replace what is there
		it->second.clear();
		it->second.emplace_back(Data);
	}
};

void TVcfParser::updatePL(TVcfLine &line, std::string_view Data, unsigned int sample) const {
	// update or add?
	int col = getFormatCol(line, "PL");
	if (col < 0) UERROR("Did not find 'PL' info field!");
	line.samples[sample].data[col] = Data;
};

void TVcfParser::addToInfo(TVcfLine &line, std::string_view Id, std::string_view Data) const {
	// update or add?
	auto it = line.info.find(Id);
	if (it == line.info.end()) {
		// add new tag
		if (info.find(Id) == info.end()) UERROR("Can not modify info, unknown info entry '", Id, "'!");
		line.info[std::string{Id}] = std::vector<std::string>();
		it            = line.info.find(Id);
		it->second.emplace_back(Data);
	} else {
		// add data, if not yet there
		bool exists = false;
		for (auto i = it->second.begin(); i != it->second.end(); ++i) {
			if ((*i) == Data) {
				exists = true;
				break;
			}
		}
		if (!exists) it->second.emplace_back(Data);
	}
};

/*
std::array<PhredIntProbability, 3> TVcfParser::GenotypeLikelihoodsAsPhredScore(TVcfLine & line, unsigned int & s){
    if(s >= line.samples.size()) throw "Sample " + coretools::str::toString(s) + " does not exists!";
    if(line.samples[s].missing){
        return { 0.0, 0.0, 0.0 };
    } else {
        int col = getFormatCol(line, "PL");
        if(col < 0){
            col = getFormatCol(line, "GL");
            if(col < 0){
                //neither PL nor GL tag: set missing
                return { 0.0, 0.0, 0.0 };
            } else {
                //GL field exists
                std::vector<std::string> phreddie;
                fillContainerFromString(line.samples[s].data[col], phreddie, ',');

                //diploid or haploid?
                if(line.samples[s].isHaploid){ //haploid: only two are given
                    return { getPhredScoreFromGL(phreddie[0]), getPhredScoreFromGL(phreddie[1]), T::lowest() };
                } else { //diploid
                    return { getPhredScoreFromGL(phreddie[0]), getPhredScoreFromGL(phreddie[1]),
getPhredScoreFromGL(phreddie[2]) };
                }
            }
        } else {
            //PL field exists
            std::vector<std::string> phreddie;
            fillContainerFromString(line.samples[s].data[col], phreddie, ',');

            //diploid or haploid?
            if(line.samples[s].isHaploid){
                //haploid: only two are given: set heterozygous to lowest
                return { getPhredScore(phreddie[0]), getPhredScore(phreddie[1]), PhredIntProbability::lowest() };

            } else {
                //diploid
                return {getPhredScore(phreddie[0]), getPhredScore(phreddie[1]), getPhredScore(phreddie[2]) };
            }
        }
    }
};

std::array<coretools::Log10Probability, 3> TVcfParser::Log10GenotypeLikelihoods(TVcfLine & line, unsigned int & s){
    if(s >= line.samples.size()) throw "Sample " + toString(s) + " does not exists!";
    if(line.samples[s].missing){
        return {0.0, 0.0, 0.0};
    } else {
        int col = getFormatCol(line, "GL");
        if(col < 0){
            col = getFormatCol(line, "PL");
            if(col < 0){
                //neither PL nor GL tag: set missing
                return {0.0, 0.0, 0.0};
            } else {
                //PL field exists
                std::vector<std::string> phreddie;
                fillContainerFromString(line.samples[s].data[col], phreddie, ',');

                //diploid or haploid?
                std::array<coretools::Log10Probability, 3> gtl;
                uint8_t tmp;
                if(line.samples[s].isHaploid){
                    //haploid: only two are given
                    savePhredScore(phreddie[0], tmp);
                    gtl[0] = tmp / -10.0;
                    savePhredScore(phreddie[1], tmp);
                    gtl[1] = tmp / -10.0;
                    gtl[2] = -99999.9; //set heterozygous to a a very small value
                } else {
                    //diploid
                    savePhredScore(phreddie[0], tmp);
                    gtl[0] = tmp / -10.0;
                    savePhredScore(phreddie[1], tmp);
                    gtl[1] = tmp / -10.0;
                    savePhredScore(phreddie[2], tmp);
                    gtl[2] = tmp / -10.0;
                }
                return gtl;
            }
        } else {
            //GL field exists: just convert to double
            std::vector<std::string> phreddie;
            fillContainerFromString(line.samples[s].data[col], phreddie, ',');

            //diploid or haploid?
            if(line.samples[s].isHaploid){
                //haploid: only two are given
                return { readGL(phreddie[0]), readGL(phreddie[1]), -99999.9 };
            } else {
                //diploid
                return { readGL(phreddie[0]), readGL(phreddie[1]), readGL(phreddie[2]) };
            }
        }
    }
};
*/

const std::string&  TVcfParser::sampleContentAt(const TVcfLine &line, std::string_view tag, unsigned int sample) const {
	assert(checkSampleNum(line, sample));
	int col = getFormatCol(tag, line);
	if (col < 0) UERROR("Column '", tag, "' is missing at position ", line.pos, " on ", line.chr, "!");
	return line.samples[sample].data[col];
};

std::string TVcfParser::sampleContentAtNoCheckForMissingSample(const TVcfLine &line, std::string_view tag, unsigned int sample) const {
	assert(checkSampleNum(line, sample));
	int col = getFormatCol(tag, line);
	if (col < 0)
		return "0"; // throw "Column '"+tag+"' is missing at position " + toString(line.pos) + " on " + line.chr + "!";
	return line.samples[sample].data[col];
};

int TVcfParser::getSampleNum(std::string_view Name) const {
	auto it = std::find(samples.begin(), samples.end(), Name);
	if (it == samples.end()) UERROR("Sample '", Name, "' is missing in the vcf file!");

	return std::distance(samples.begin(), it);
};

std::string TVcfParser::getSampleName(unsigned int sample) const {
	if (sample > samples.size()) UERROR("Sample ", sample, " does not exists!");
	return samples[sample];
};

int TVcfParser::getNumSamples() const { return samples.size(); };

//--------------------------------------------------------------------
// get variant info
//--------------------------------------------------------------------
const std::string&  TVcfParser::getChr(const TVcfLine &line) const {
	assert(line.positionParsed);
	return line.chr;
};

uint64_t TVcfParser::getPos(const TVcfLine &line) const {
	assert(line.positionParsed);
	return line.pos;
};

int TVcfParser::getNumAlleles(const TVcfLine &line) const {
	assert(line.variantParsed);
	return line.variants.size();
};

const std::string& TVcfParser::getRefAllele(const TVcfLine &line) const {
	assert(line.variantParsed);
	return line.variants[0];
};

const std::string& TVcfParser::getFirstAltAllele(const TVcfLine &line) const {
	assert(line.variantParsed);
	return line.variants[1];
};

const std::string& TVcfParser::getAllele(const TVcfLine &line, int num) const {
	assert(line.variantParsed);
	return line.variants[num];
};

bool TVcfParser::variantQualityIsMissing(const TVcfLine &line) const {
	assert(line.qualityParsed);
	return line.variantQualityMissing;
};

double TVcfParser::variantQuality(const TVcfLine &line) const {
	assert(line.qualityParsed);
	assert(line.variantQualityMissing);
	return line.variantQuality;
};
//--------------------------------------------------------------------

bool TVcfParser::checkSampleNum(const TVcfLine &line, unsigned int sample) const {
	return sample < line.samples.size() && !line.samples[sample].missing;
};

void TVcfParser::addInfoToSample(TVcfLine &line, unsigned int sample, std::string_view tag, std::string_view Data) const {
	if (sample >= line.samples.size()) UERROR("Sample ", sample, " does not exists!");
	if (!line.samples[sample].missing) {
		// find position in format string
		const int col = addFormatCol(tag, line);
		line.samples[sample].updateData(col, Data);
	}
};

void TVcfParser::setSampleMissing(TVcfLine &line, unsigned int sample) const {
	if (sample >= line.samples.size()) UERROR("Sample ", sample, " does not exists!");
	line.samples[sample].missing = true;
};
void TVcfParser::setSampleHasUndefinedGenotype(TVcfLine &line, unsigned int sample) const {
	if (sample >= line.samples.size()) UERROR("Sample ", sample, " does not exists!");
	line.samples[sample].unknownGenotype = true;
};

void TVcfParser::updateField(TVcfLine &line, std::string_view tag, std::string_view Data, unsigned int sample) const {
	assert(checkSampleNum(line, sample));
	int col = getFormatCol(tag, line);
	if (col < 0) UERROR("Column '", tag, "' is missing at position ", line.pos, " on ", line.chr, "!");
	line.samples[sample].data[col] = Data;
};

bool TVcfParser::sampleIsHaploid(const TVcfLine &line, unsigned int sample) const {
	if (sample >= line.samples.size()) UERROR("Sample ", sample, " does not exists!");
	return line.samples[sample].isHaploid;
};

bool TVcfParser::sampleIsDiploid(const TVcfLine &line, unsigned int sample) const {
	assert(checkSampleNum(line, sample));
	return !line.samples[sample].isHaploid;
};

bool TVcfParser::sampleIsHomoRef(const TVcfLine &line, unsigned int sample) const {
	assert(checkSampleNum(line, sample));
	if (line.samples[sample].genotype.first == 0 && line.samples.at(sample).genotype.second == 0) return true;
	return false;
};

bool TVcfParser::sampleIsHeteroRefNonref(const TVcfLine &line, unsigned int sample) const {
	assert(checkSampleNum(line, sample));
	if (line.samples[sample].genotype.first == 0 && line.samples.at(sample).genotype.second != 0) return true;
	if (line.samples[sample].genotype.first != 0 && line.samples.at(sample).genotype.second == 0) return true;
	return false;
};

const std::string& TVcfParser::getFirstAlleleOfSample(const TVcfLine &line, unsigned int sample) const {
	return line.variants[line.samples[sample].genotype.first];
};

const std::string& TVcfParser::getSecondAlleleOfSample(const TVcfLine &line, unsigned int sample) const {
	return line.variants[line.samples[sample].genotype.second];
};

BiallelicGenotype TVcfParser::sampleBiallelicGenotype(const TVcfLine &line, unsigned int sample) const {
	using BG = BiallelicGenotype;
	// return missing if non-biallelic
	if (line.samples[sample].isHaploid) {
		if (line.samples[sample].missing || line.variants.size() > 2) { return BG::missingHaploid; }
		if (line.samples[sample].genotype.first == 0) {
			return BG::haploidFirst;
		} else {
			return BG::haploidSecond;
		}
	} else {
		if (line.samples[sample].missing || line.variants.size() > 2) { return BG::missingDiploid; }
		auto tmp = line.samples[sample].genotype.first + line.samples[sample].genotype.second;
		if (tmp == 0) {
			return BG::homoFirst;
		} else if (tmp == 1) {
			return BG::het;
		} else {
			return BG::homoSecond;
		}
	}
};

bool TVcfParser::sampleIsMissing(const TVcfLine &line, unsigned int s) const {
	if (s >= line.samples.size()) { UERROR("Sample ", s, " does not exists!"); }
	return line.samples[s].missing;
};

bool TVcfParser::sampleHasUndefinedGenotype(const TVcfLine &line, unsigned int s) const {
	if (s >= line.samples.size()) UERROR("Sample ", s, " does not exists!");
	return line.samples[s].unknownGenotype;
};

float TVcfParser::sampleGenotypeQuality(const TVcfLine &line, unsigned int sample) const {
	//	std::cerr << "check quality of sample " << sample << " and position " << line.pos << ": " << std::flush;
	assert(checkSampleNum(line, sample));
	int col = getFormatCol(line, "GQ");
	//	std::cerr << " col=" << col << std::flush;
	if (col < 0) col = getFormatCol(line, "RGQ");
	if (col < 0) UERROR("Column 'GQ' is missing at position ", line.pos, " on ", line.chr, "!");
	//	std::cerr << " qual=" << line.samples[sample].data[col] << std::endl;
	return coretools::str::fromString<double>(line.samples[sample].data[col]);
};

void TVcfParser::parseFormat(TVcfLine &line) {
	std::string buf;
	int i = 0;
	while (!line.data[cols.Format].empty()) {
		buf = coretools::str::extractBefore(line.data[cols.Format], ':');
		coretools::str::trimString(buf);
		line.data[cols.Format].erase(0, 1);

		line.format.emplace(buf, i);
		line.formatOrdered.push_back(buf);
		++i;
	}
	line.formatParsed = true;
};

int TVcfParser::getFormatCol(std::string_view tag, const TVcfLine &line) const {
	auto it = line.format.find(tag);
	if (it == line.format.end()) return -1;
	return it->second;
};

bool TVcfParser::formatColExists(std::string_view tag, const TVcfLine &line) const {
	auto it = line.format.find(tag);
	if (it == line.format.end()) return false;
	return true;
};

int TVcfParser::addFormatCol(std::string_view tag, TVcfLine &line) const {
	// col exists?
	int col = getFormatCol(tag, line);
	if (col < 0) {
		// col does not exists -> add!
		col = line.format.size();
		line.format.emplace(tag, col);
		line.formatOrdered.emplace_back(tag);
		// add emtpy string to all samples
		for (auto it = line.samples.begin(); it != line.samples.end(); ++it) {
			if (!it->missing) (*it).addData("");
		}
	}
	return col;
};

void TVcfParser::parseInfo(TVcfLine &line) {
	std::string buf, temp;
	std::map<std::string, std::vector<std::string>>::iterator it;
	while (!line.data[cols.Info].empty()) {
		buf = coretools::str::extractBefore(line.data[cols.Info], ';');
		coretools::str::trimString(buf);
		line.data[cols.Info].erase(0, 1);

		temp = coretools::str::extractBefore(buf, '=');
		buf.erase(0, 1);

		line.info[temp] = std::vector<std::string>();
		it              = line.info.find(temp);
		while (!buf.empty()) {
			it->second.push_back(coretools::str::extractBefore(buf, ','));
			buf.erase(0, 1);
		}
	}
	line.infoParsed = true;
};

void TVcfParser::parseSamples(TVcfLine &line) {
	// make sure variant and format are parsed
	if (!line.variantParsed) parseVariant(line);
	if (!line.formatParsed) parseFormat(line);

	// parse all samples
	int gtCol = getFormatCol("GT", line);

	for (size_t i = cols.FirstInd; i < cols.FirstInd + samples.size(); ++i) {
		line.samples.emplace_back();
		if (!line.samples.back().parse(line.data[i], gtCol)) {
			UERROR("Unknown genotype '", line.samples.end()->getCol(gtCol), "' in VCF file on line ", line.lineNumber,
				   "!");
		}
	}
	line.samplesParsed = true;
};

void TVcfParser::writeColumnDescriptionHeader(std::ostream &out) const {
	out << "#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT";
	// samples
	for (auto it = samples.begin(); it != samples.end(); ++it) { out << "\t" << *it; }
	out << std::endl;
};
void TVcfParser::writeInfoHeader(std::ostream &out) const {
	for (auto it = info.begin(); it != info.end(); ++it) {
		out << "##INFO=" << it->second.getString() << std::endl;
	}
};
void TVcfParser::writeFormatHeader(std::ostream &out) const {
	for (auto it = format.begin(); it != format.end(); ++it) {
		out << "##FORMAT=" << it->second.getString() << std::endl;
	}
};

void TVcfParser::writeLine(const TVcfLine &line, std::ostream &out) const {
	// position
	if (line.positionParsed)
		out << line.chr << "\t" << line.pos;
	else
		out << line.data[cols.Chr] << "\t" << line.data[cols.Pos];

	// id
	if (line.idParsed)
		out << "\tERROR"; // we do not yet parse id
	else
		out << "\t" << line.data[cols.Id];

	// variant
	if (line.variantParsed) {
		out << "\t";
		line.writeVariant(out);
	} else
		out << "\t" << line.data[cols.Ref] << "\t" << line.data[cols.Alt];

	// qual
	if (line.qualityParsed)
		out << "\t" << line.variantQuality;
	else
		out << "\t" << line.data[cols.Qual];

	// filter
	if (line.filterParsed)
		out << "\tERROR"; // we do not yet parse it
	else
		out << "\t" << line.data[cols.Filter];

	// info
	if (line.infoParsed) {
		out << "\t";
		bool first = true;
		for (auto it = line.info.begin(); it != line.info.end();
		     ++it) {
			if (first)
				first = false;
			else
				out << ";";
			out << it->first;
			if (it->second.size() > 0) {
				out << "=" << it->second[0];
				for (auto i = it->second.begin() + 1; i != it->second.end(); ++i) { out << "," << *i; }
			}
		}
	} else
		out << "\t" << line.data[cols.Info];

	// format
	if (line.formatParsed) {
		auto it = line.formatOrdered.begin();
		out << "\t" << *it;
		++it;
		for (; it != line.formatOrdered.end(); ++it) out << ":" << *it;
	} else
		out << "\t" << line.data[cols.Format];

	// samples
	if (line.samplesParsed) {
		for (auto it = line.samples.begin(); it != line.samples.end(); ++it) {
			it->write(out, line.formatOrdered.size());
		}
	} else {
		for (size_t i = cols.FirstInd; i < cols.FirstInd + samples.size(); ++i) { out << "\t" << line.data[i]; }
	}

	out << std::endl;
};

}; // end namespace genometools
