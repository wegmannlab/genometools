#ifndef VCF_H_
#define VCF_H_

#include "genometools/GLF/GLF.h"
#include "genometools/Genotypes/AllelicCombination.h"
#include "genometools/Genotypes/Ploidy.h"
#include "genometools/Genotypes/TPloidEntry.h"

namespace genometools {

using TVCFEntry    = TPloidEntry<Haploid, Diploid>;
using TLikelihoods = TVCFEntry::DualArray;

constexpr std::string_view VCFversion() noexcept { return "VCFv4.2"; }

std::vector<TVCFEntry> fill(coretools::TConstView<TGLFEntry> samples, AllelicCombination ac);

TVCFEntry calculateGenotypeLikelihoods(size_t NumRef, size_t NumAlt, bool IsDiploid);
TVCFEntry simulate(BiallelicGenotype genotype, size_t avgDepth);
std::pair<Base, Base> findMajorMinorAllele(coretools::TStrongArray<size_t, Base, 4> AlleleCounts, Base RefAllele);
}

#endif
