/*
 * TFasta.h
 *
 *  Created on: Nov 18, 2022
 *      Author: Andreas
 */

#ifndef GENOMETOOLS_TFASTA_H_
#define GENOMETOOLS_TFASTA_H_

#include <string>
#include <vector>

#include "coretools/Containers/TView.h"
#include "coretools/Files/TLineReader.h"
#include "coretools/Main/TError.h"

#include "genometools/GenomePositions/TGenomePosition.h"
#include "genometools/GenomePositions/TGenomeWindow.h"
#include "genometools/Genotypes/Base.h"

namespace genometools {

class TFastaReader {
	mutable std::string _chrName;
	mutable size_t _refID         = size_t(-1);
	mutable bool _endOfChromosome = false;
	mutable coretools::TLineReader _reader;
	mutable std::vector<Base> _bases;

	void _readUntil(size_t n) const {
		_bases.reserve(n + 1);
		while (_bases.size() <= n && !_reader.empty() && !_endOfChromosome) {
			_reader.popFront();
			if (_reader.front().front() == '>') {
				_endOfChromosome = true;
				break;
			}
			for (auto c : _reader.front()) { _bases.push_back(char2base(c)); }
		}
		if (_bases.size() <= n) { _bases.resize(n + 1, Base::N); }
	}

	void _jumpChromosome(size_t refID) const {
		if (refID == _refID) return;
		_endOfChromosome = false;
		if (refID < _refID) {
			_reader.setPosition(0);
			if (_reader.front().front() != '>')
				UERROR("In FASTA file: ", _reader.name(), ", wrong file format, needs to start with leading '>'");
			_refID = 0;
		} else { // refID > _refID
			if (_reader.front().front() == '>')++_refID;
		}

		while (_refID < refID && !_reader.empty()) {
			_reader.popFront();
			if (!_reader.empty() && _reader.front().front() == '>') ++_refID;
		}

		if (_refID != refID || _reader.front().front() != '>'){
			UERROR("In FASTA file: ", _reader.name(), ", wrong file format, cannot find refID: ", refID, " !");
		}
		_chrName = _reader.front().substr(1);
		_bases.clear();
	}

public:
	TFastaReader() = default;

	TFastaReader(std::string_view Filename) : _reader(Filename) {}

	void open(std::string_view Filename) { _reader.open(Filename); }

	void close() { _reader.close(); }

	bool isOpen() const noexcept { return _reader.isOpen(); }

	operator bool() const noexcept { return isOpen(); }

	Base operator[](TGenomePosition pos) const {
		_jumpChromosome(pos.refID());
		_readUntil(pos.position());
		return _bases[pos.position()];
	}

	Base operator()(size_t refID, size_t pos) const {
		_jumpChromosome(refID);
		_readUntil(pos);
		return _bases[pos];
	}

	coretools::TView<Base> view(TGenomeWindow window) const {
		_jumpChromosome(window.refID());
		_readUntil(window.to().position() - 1);
		auto v = coretools::TView<Base>(_bases);
		return v.subview(window.from().position(), window.size());
	}

	coretools::TView<Base> view(size_t refId, size_t from, size_t count) const {
		_jumpChromosome(refId);
		_readUntil(from + count);
		auto v = coretools::TView<Base>(_bases);
		return v.subview(from, count);
	}

	const std::string &chrName(size_t refId) const noexcept {
		_jumpChromosome(refId);
		return _chrName;
	}
};
} // namespace genometools

#endif
