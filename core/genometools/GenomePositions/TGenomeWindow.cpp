#include "genometools/GenomePositions/TGenomeWindow.h"
#include "genometools/GenomePositions/TChromosomes.h"
#include "genometools/GenomePositions/TGenomePosition.h"

namespace genometools {

void TGenomeWindow::clear() {
	_from.clear();
	_to = _from;
}

void TGenomeWindow::move(size_t RefID, size_t From, size_t Length) {
	_from.move(RefID, From);
	_to.move(RefID, From + Length);
}

void TGenomeWindow::move(const TGenomePosition &From, size_t Length) {
	_from = From;
	_to   = _from + Length;
}

void TGenomeWindow::move(const TGenomePosition &From, const TGenomePosition &To) {
	if (From.refID() != To.refID()) { DEVERROR("Chromosomes From (", From, ") and To (", To, ") do not match!"); }
	if (To < From) { DEVERROR("To (", To, ") < From(", From, ")!"); }
	_from = From;
	_to   = To;
}

void TGenomeWindow::move(const TGenomeWindow &other) {
	_from = other.from();
	_to   = other.to();
}

TGenomeWindow TGenomeWindow::operator+(size_t length) const { return TGenomeWindow(_from + length, _to + length); }

TGenomeWindow TGenomeWindow::operator-(size_t length) const {
	if (length > _to.position()) {
		return TGenomeWindow(_from.refID(), 0, 1);
	}
	return TGenomeWindow(_from - length, _to - length);
}

size_t TGenomeWindow::operator-(const TGenomeWindow &other) const { return _from - other.from(); }

size_t TGenomeWindow::operator-(const TGenomePosition &other) const { return _from - other; }

bool TGenomeWindow::within(const TGenomePosition &other) const {
	// checks if other position is entirely within
	const auto oPos = other.position();
	return other.sameChr(_from) && fromOnChr() <= oPos && toOnChr() > oPos;
}

bool TGenomeWindow::contains(const TGenomeWindow &other) const {
	// checks if other window is entirely within
	return sameChr(other) && fromOnChr() <= other.fromOnChr() && toOnChr() >= other.toOnChr();
}

bool TGenomeWindow::overlaps(const TGenomeWindow &other) const {
	// check if other window overlaps
	return sameChr(other) && ((fromOnChr() <= other.fromOnChr() && toOnChr() > other.fromOnChr()) ||
							  (other.fromOnChr() <= fromOnChr() && other.toOnChr() > fromOnChr()));
}

bool TGenomeWindow::overlapsOrExtends(const TGenomeWindow &other) const {
	// check if other window overlaps, or are consecutive (no gap)
	return sameChr(other) && ((fromOnChr() <= other.fromOnChr() && toOnChr() >= other.fromOnChr()) ||
							  (other.fromOnChr() <= fromOnChr() && other.toOnChr() >= fromOnChr()));
}

bool TGenomeWindow::mergeWith(const TGenomeWindow &other) {
	if (!overlapsOrExtends(other)) { return false; }

	if (_from > other.from()) { _from = other.from(); }
	if (_to < other.to()) { _to = other.to(); }
	return true;
}

void TGenomeWindow::operator+=(size_t length) {
	_from += length;
	_to += length;
}

void TGenomeWindow::operator-=(size_t length) {
	if (length > _from.position()) {
		_from.move(_from.refID(), 0);
		if (length > _to.position()) {
			_to.move(_to.refID(), 1);
		} else {
			_to -= length;
		}
	} else {
		_from -= length;
		_to -= length;
	}
}

void TGenomeWindow::resize(size_t newLength) { _to = _from + newLength; }

std::string TGenomeWindow::asFormattedString(const TChromosomes &Chromosomes, std::string_view delim) const {
	return coretools::str::toString(Chromosomes[_from.refID()].name(), delim, _from.position() + 1, "-",
									_to.position() + 1);
}

TGenomeWindow merge(const TGenomeWindow &first, const TGenomeWindow &second) {
	if (!first.overlapsOrExtends(second)) { DEVERROR("windows do not overlap!"); }
	return TGenomeWindow(std::min(first.from(), second.from()), std::max(first.to(), second.to()));
}

} // namespace genometools
