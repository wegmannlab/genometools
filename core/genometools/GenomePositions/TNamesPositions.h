//
// Created by madleina on 09.03.22.
//

#ifndef TNAMESPOSITIONS_H
#define TNAMESPOSITIONS_H

#include "coretools/Storage/TNames.h"
#include "coretools/Strings/stringManipulations.h"
#include "genometools/GenomePositions/TDistances.h"

//--------------------------
// TNamesPositions
//--------------------------

namespace genometools {

class TNamesPositions : public coretools::TNamesEmpty {
	// name class that stores a pointer to positions
	// and provides functions to add and get those
protected:
	TPositionsRaw *_positions; // non-owning ptr
	bool _orderIsChunkPos;

	void _splitName(std::string Name, std::string &Chunk, size_t &Pos) {
		if (_orderIsChunkPos) {
			Chunk = coretools::str::split(Name, _delimNames);
			Pos  = coretools::str::fromString<size_t>(Name);
		} else {
			Pos  = coretools::str::fromString<size_t>(coretools::str::split(Name, _delimNames));
			Chunk = Name;
		}
	};

public:
	/*TNamesPositions() : TNamesEmpty() {
		_complexity            = 2;
		_title                 = {"-", "-"};
		_delimNames            = ':';  // by default, use : to paste Chr:Pos together
		_orderIsChunkPos        = true; // by default, the order is chr-pos
		_storesNonDefaultNames = true;
		_positions             = nullptr;
		};*/

	TNamesPositions(TPositionsRaw *Positions) : TNamesEmpty() {
		addPositions(Positions);
		_complexity            = 2;
		_title                 = {"-", "-"};
		_delimNames            = ':';  // by default, use : to paste Chr:Pos together
		_orderIsChunkPos        = true; // by default, the order is chr-pos
		_storesNonDefaultNames = true;
	};

	void setOrderChrPos(bool OrderIsChrPos) { _orderIsChunkPos = OrderIsChrPos; };

	void addPositions(TPositionsRaw *Positions) { _positions = Positions; };

	void addName(const std::vector<std::string> &Name) override {
		_checkSizeNameVec(Name, "TNamesPositions");
		// account for order Chr-Pos or Pos-Chr
		std::string pos;
		std::string chr;
		if (_orderIsChunkPos) {
			// Chr,Pos
			chr = Name[0];
			pos = Name[1];
		} else {
			// Pos,Chr
			pos = Name[0];
			chr = Name[1];
		}
		auto p = coretools::str::fromString<size_t, true>(pos);
		_positions->add(p, chr);
		_size++;
	};

	void addName(const std::vector<std::string> &Name, size_t) override { addName(Name); };

	void finalizeFilling() override { _positions->finalizeFilling(); };

	std::string operator[](size_t Index) const override {
		std::string delimNamesAsString = {_delimNames};
		if (_orderIsChunkPos) {
			return _positions->getChunkPositionAsString(Index, delimNamesAsString);
		} else {
			return _positions->getPositionChunkAsString(Index, delimNamesAsString);
		}
	};

	std::vector<std::string> getName(size_t Index) const override {
		if (_orderIsChunkPos) {
			return {_positions->getChunkName(Index), coretools::str::toString(_positions->getPosition(Index))};
		} else {
			return {coretools::str::toString(_positions->getPosition(Index)), _positions->getChunkName(Index)};
		}
	};

	bool exists(std::string_view Name) override {
		std::string chunk;
		size_t pos;
		_splitName(std::string{Name}, chunk, pos);
		return _positions->exists(pos, chunk);
	};

	bool exists(const std::vector<std::string> &Name) override {
		if (_orderIsChunkPos) {
			return _positions->exists(coretools::str::fromString<size_t>(Name[1]), Name[0]);
		} else {
			return _positions->exists(coretools::str::fromString<size_t>(Name[0]), Name[1]);
		}
	};

	bool operator==(const TNamesEmpty &Other) const override {
		if (!TNamesEmpty::operator==(Other)) { return false; }
		for (size_t i = 0; i < _size; i++) {
			if (operator[](i) != Other[i]) { return false; }
		}
		return true;
	}

	size_t getIndex(std::string_view Name) override {
		std::string chunk;
		size_t pos;
		_splitName(std::string{Name}, chunk, pos);
		return _positions->getIndex(pos, chunk);
	};

	size_t size() const override { return _positions->size(); };
};

}; // end namespace genometools

#endif // TNAMESPOSITIONS_H
