/*
 * TGenomePosition.cpp
 *
 *  Created on: Jun 10, 2020
 *      Author: phaentu
 */

#include "genometools/GenomePositions/TGenomePosition.h"

#include "genometools/GenomePositions/TChromosomes.h"

namespace genometools {

std::string TGenomePosition::asFormattedString(const TChromosomes & Chromosomes, std::string delim) const {
	return Chromosomes[_refID].name() + delim + coretools::str::toString(_position + 1);
}

}; // end namespace genometools
