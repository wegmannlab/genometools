#ifndef TGENOMEWINDOW_H_
#define TGENOMEWINDOW_H_

#include <string>

#include "genometools/GenomePositions/TGenomePosition.h"

namespace genometools {

//-----------------------------------------------------
// TGenomeWindow
//-----------------------------------------------------
class TGenomeWindow {
protected:
	TGenomePosition _from, _to; // NOTE: position _to is NOT included. Window is [from, to).

public:
	TGenomeWindow() = default;
	TGenomeWindow(size_t RefID, size_t From, size_t Length = 1) { move(RefID, From, Length); }
	TGenomeWindow(const TGenomePosition &From, const TGenomePosition &To) {move(From, To);}
	explicit TGenomeWindow(const TGenomePosition &From, size_t Length = 1) {move(From, Length);}

	void clear();

	size_t refID() const noexcept { return _from.refID(); };
	constexpr TGenomePosition from() const noexcept { return _from; };
	constexpr TGenomePosition to() const noexcept { return _to; };
	size_t fromOnChr() const noexcept { return _from.position(); };
	size_t toOnChr() const noexcept { return _to.position(); };
	size_t size() const noexcept { return _to.position() - _from.position(); };

	void move(size_t RefID, size_t Start, size_t Length);
	void move(const TGenomePosition &From, size_t Length);
	void move(const TGenomePosition &From, const TGenomePosition &To);
	void move(const TGenomeWindow &other);

	TGenomeWindow operator+(size_t length) const;
	TGenomeWindow operator-(size_t length) const;
	size_t operator-(const TGenomeWindow &other) const;
	size_t operator-(const TGenomePosition &other) const;

	bool sameChr(const TGenomeWindow &other) const { return refID() == other.refID(); };
	bool within(const TGenomePosition &other) const;
	bool contains(const TGenomeWindow &other) const;
	bool overlaps(const TGenomeWindow &other) const;
	bool overlapsOrExtends(const TGenomeWindow &other) const;
	bool mergeWith(const TGenomeWindow &other);

	// move / expand
	void operator+=(size_t length);
	void operator-=(size_t length);
	void resize(size_t newLength);

	operator std::string() const {
		//return internal string (i.e. 0-based)		
		return (std::string) _from + "-" + (std::string) _to;
	};

	std::string asFormattedString(const TChromosomes & Chromosomes, std::string_view delim = ":") const; // for users, i.e. 1-based
};

constexpr bool operator<(const TGenomeWindow &lhs, const TGenomeWindow &rhs) noexcept {
	return lhs.from() < rhs.from();
}
constexpr bool operator>(const TGenomeWindow &lhs, const TGenomeWindow &rhs) noexcept {
	return lhs.from() > rhs.from();
}
constexpr bool operator==(const TGenomeWindow &lhs, const TGenomeWindow &rhs) noexcept {
	return (lhs.to() == rhs.to()) && (lhs.from() == rhs.from());
}

// Window is smaller/larger if all of window smaller/larger than position
constexpr bool operator<(const TGenomeWindow &lhs, const TGenomePosition &rhs) noexcept {
	return lhs.to() <= rhs;
}
constexpr bool operator>(const TGenomeWindow &lhs, const TGenomePosition &rhs) noexcept {
	return lhs.from() > rhs;
}

constexpr bool operator<(const TGenomePosition &lhs, const TGenomeWindow &rhs) noexcept {
	return lhs < rhs.from();
}
constexpr bool operator>(const TGenomePosition &lhs, const TGenomeWindow &rhs) noexcept {
	return lhs >= rhs.to();
}

TGenomeWindow merge(const TGenomeWindow &lhs, const TGenomeWindow &rhs);

}

#endif
