/*
 * TChromosomes.cpp
 *
 *  Created on: Apr 25, 2019
 *      Author: linkv
 */

#include "genometools/GenomePositions/TChromosomes.h"

#include "coretools/Main/TLog.h"
#include "coretools/Main/TParameters.h"
#include "coretools/Files/TInputFile.h"
#include "coretools/Strings/fillContainer.h"
#include "coretools/Strings/stringProperties.h"

#include <filesystem>

namespace genometools {

using coretools::instances::logfile;
using coretools::instances::parameters;
using coretools::str::toString;

//---------------------------------------------------------
// TChromosome
//---------------------------------------------------------
TChromosome::TChromosome(uint32_t RefID, std::string_view Name, uint32_t Length) {
	_initialize(RefID, Name, Length, 2);
};

TChromosome::TChromosome(uint32_t RefID, std::string_view Name, uint32_t Length, uint8_t Ploidy) {
	_initialize(RefID, Name, Length, Ploidy);
};

void TChromosome::_initialize(uint32_t RefID, std::string_view Name, uint32_t Length, uint8_t Ploidy) {
	_name   = Name;
	_length = Length;
	_ploidy = Ploidy;
	_inUse  = true;

	// set TGenomePosition
	_from.move(RefID, 0);
	_to.move(RefID, _length); // end is not included
};

std::string TChromosome::compileSamHeader() const {
	std::string header = "@SQ\tSN:" + _name + "\tLN:" + toString(_length);

	if (!alternateLocus.empty()) { header += "\tAH:" + alternateLocus; }
	if (!alternativeNames.empty()) { header += "\tAN:" + alternativeNames; }
	if (!assemblyIdentifier.empty()) { header += "\tAS:" + assemblyIdentifier; }
	if (!description.empty()) { header += "\tDS:" + description; }
	if (!MD5.empty()) { header += "\tM5:" + MD5; }
	if (!species.empty()) { header += "\tSP:" + species; }
	if (!topology.empty()) { header += "\tTP:" + topology; }
	if (!uri.empty()) { header += "\tUR:" + uri; }

	return header + "\n";
};

//---------------------------------------------------------
// TChromosomes
//---------------------------------------------------------
void TChromosomes::clear() {
	_chromosomes.clear();
};

const TChromosome &TChromosomes::appendChromosome(const TChromosome& Other){
	_chromosomes.emplace_back(_chromosomes.size(), Other.name(), Other.length(), Other.ploidy());
	return _chromosomes.back();
}

const TChromosome &TChromosomes::appendChromosome(std::string_view name, uint32_t length) {
	_chromosomes.emplace_back(_chromosomes.size(), name, length);
	return _chromosomes.back();
};

const TChromosome &TChromosomes::appendChromosome(std::string_view name, uint32_t length, uint8_t ploidy) {
	_chromosomes.emplace_back(_chromosomes.size(), name, length, ploidy);
	return _chromosomes.back();
};

TChromosome &TChromosomes::_find(std::string_view chrName) {
	for (auto &c : _chromosomes) {
		if (c.name() == chrName) return c;
	}
	UERROR("Chromosome '", chrName, "' not found in BAM header!");
};

const TChromosome &TChromosomes::_find(std::string_view chrName) const {
	for (auto &c : _chromosomes) {
		if (c.name() == chrName) return c;
	}
	UERROR("Chromosome '", chrName, "' not found in BAM header!");
};

void TChromosomes::_setAllNotInUseAndDiploid() {
	for (auto &c : _chromosomes) {
		c.setInUse(false);
		c.setPloidy(2);
	}
};

void TChromosomes::limitAndSetPloidy() {
	limitChr();
	setPloidy();
};

bool TChromosomes::_readChrFromFile(std::string_view Filename) {
	logfile().list("Interpreting parameter 'chr' as a filename");
	coretools::TInputFile file(Filename, coretools::FileType::Header);

	if (!(file.numCols() == 1 || file.numCols() == 2)) {
		UERROR("File ", Filename, " must have one or two columns, not ", file.numCols(), " (arg 'chr')");
	}

	if (file.index("chr") != 0) UERROR("First column of chromosome file '", Filename, "' should be 'chr'!");

	std::vector<std::pair<std::string, size_t>> chrNamesAndPloidy;

	bool ploidySpecified = false;
	if (file.numCols() == 1) {
		logfile().list("Detected a single column in file ", Filename, ". Will assume all chromosomes are diploid");
		// only chr (no ploidy -> always diploid)
		for(; !file.empty(); file.popFront()) {
			chrNamesAndPloidy.emplace_back(file.get(0), 2);
		}

	} else if (file.numCols() == 2) {
		if (file.index("ploidy") != 1) UERROR("Second column of chromosome file '", Filename, "' should be 'ploidy'!");
		ploidySpecified = true;
		logfile().list("Detected two columns in file ", Filename, ". Will set ploidy according to second column");
		// chr and ploidy
		for(; !file.empty(); file.popFront()) {
			chrNamesAndPloidy.emplace_back(file.get(0), file.get<size_t>(1));
		}
	}

	_useSpecifiedChr(chrNamesAndPloidy);
	return ploidySpecified;
}

bool TChromosomes::_readChr(std::string_view String) {
	if(coretools::str::stringContains(String, ",") || !std::filesystem::exists(String)){
		// it is a comma-separated vector or a single chromosome name
		logfile().list("Interpreting parameter 'chr' as a comma-separated list");
		std::vector<std::string> vec;
		coretools::str::fillContainerFromString(String, vec, ',');
		_useSpecifiedChr(vec);
		return false; // ploidy was not specified
	} else {
		return _readChrFromFile(String);
	}
}

void TChromosomes::limitChr() {
	if (parameters().exists("chr")) {
		bool specifiedPloidy = _readChr(parameters().get("chr"));
		logfile().startIndent("Will limit analysis to the following chromosomes (parameter 'chr'):");
		writeUsedChromosomes(specifiedPloidy);
	}
	if (parameters().exists("limitChr")) {
		std::string limitName = parameters().get<std::string>("limitChr");
		logfile().list("Will limit analysis to all chromosomes up to and including " + limitName +
		               ". (parameter 'limitChr')");
		// issue a warning if both chr and limitChr were specified
		if (parameters().exists("chr")) {
			logfile().warning("Detected conflicting parameters 'chr' and 'limitChr'. Will limit analysis to all "
			                  "chromosomes in present 'chr' up to and including ",
			                  limitName, ".");
		}
		_limitChr(limitName);
	}
};

void TChromosomes::_useSpecifiedChr(std::string_view ChrName, size_t Ploidy) {
	// find chromosome
	TChromosome &c = _find(ChrName);
	c.setInUse(true);
	c.setPloidy(Ploidy);

	if (!(c.ploidy() == 1 || c.ploidy() == 2)) {
		UERROR("Unsupported ploidy '", c.ploidy(), "'! Currently only support haploid (1) and diploid (2).");
	}
}

void TChromosomes::_useSpecifiedChr(const std::vector<std::string> &ChrNames) {
	_setAllNotInUseAndDiploid();
	for (auto &it : ChrNames) { _useSpecifiedChr(it, 2); }
};

void TChromosomes::_useSpecifiedChr(const std::vector<std::pair<std::string, size_t>> &ChrNamesAndPloidy) {
	_setAllNotInUseAndDiploid();
	for (auto &it : ChrNamesAndPloidy) { _useSpecifiedChr(it.first, it.second); }
};

void TChromosomes::_limitChr(std::string_view Name) {
	// check if chr exists
	_find(Name);

	// do not change inUse of chromosomes prior to 'name'
	// but set inUse of all chromosomes after 'name' to false
	bool afterName = false;
	for (auto &it : _chromosomes) {
		if (afterName) { it.setInUse(false); }
		if (it.name() == Name) { afterName = true; }
	}
};

void TChromosomes::writeUsedChromosomes(bool WritePloidy) {
	for (auto &it : _chromosomes) {
		if (it.inUse()) {
			if (WritePloidy) {
				if (it.ploidy() == 1) {
					logfile().list(it.name() + ": haploid");
				} else {
					logfile().list(it.name() + ": diploid");
				}
			} else {
				logfile().list(it.name());
			}
		}
	}

	logfile().endIndent();
};

void _issueWarning_ChrPloidy() {
	if (parameters().exists("chr")) {
		logfile().warning("Detected conflicting parameters 'chr' and 'haploid': Will set all chromosomes listed in "
		                  "argument 'haploid' to haploid, even if 'chr' specified a different ploidy.");
	}
}

void TChromosomes::setPloidy() {
	if (parameters().exists("haploid")) {
		_issueWarning_ChrPloidy();

		std::vector<std::string> vec;
		coretools::str::fillContainerFromString(parameters().get<std::string>("haploid"), vec, ',');
		if (vec.size() == 1 && vec[0] == "all") {
			logfile().list("Assuming all chromosomes are haploid. (parameter 'haploid')");
			for (auto &c : _chromosomes) { c.setPloidy(1); }
		} else {
			_setToHaploid(vec);
		}
	}
};

void TChromosomes::_setToHaploid(const std::vector<std::string> &chrNames) {
	logfile().startIndent("Setting the following chromosomes to be haploid: (parameter 'haploid')");
	for (auto &it : chrNames) {
		TChromosome &c = _find(it);
		c.setPloidy(1);
		logfile().list(c.name());
	}
	logfile().endIndent();
};

// getters
uint32_t TChromosomes::referenceLength() const {
	uint32_t totLength = 0;
	for (auto &c : _chromosomes) {
		if (c.inUse()) totLength += c.length();
	}
	return totLength;
};

uint32_t TChromosomes::minLength() const {
	uint32_t minLen = _chromosomes[0].length();
	for (auto &c : _chromosomes) {
		if (c.length() < minLen) { minLen = c.length(); }
	}
	return minLen;
};

uint32_t TChromosomes::maxLength() const {
	uint32_t maxLen = _chromosomes[0].length();
	for (auto &c : _chromosomes) {
		if (c.length() > maxLen) { maxLen = c.length(); }
	}
	return maxLen;
};

bool TChromosomes::exists(std::string_view chrName) const {
	for (auto &c : _chromosomes) {
		if (c.name() == chrName) return true;
	}
	return false;
};

const TChromosome &TChromosomes::getChromosome(std::string_view chrName) const { return _find(chrName); };

uint32_t TChromosomes::refID(std::string_view chrName) const { return _find(chrName).refID(); };

uint32_t TChromosomes::length(uint32_t RefID) const { return _chromosomes[RefID].length(); };

std::string TChromosomes::name(uint32_t RefID) const { return _chromosomes[RefID].name(); };

bool TChromosomes::inUse(uint32_t RefID) const { return _chromosomes[RefID].inUse(); };

uint8_t TChromosomes::ploidy(uint32_t RefID) const { return _chromosomes[RefID].ploidy(); };

const TGenomePosition &TChromosomes::chrStart(uint32_t RefID) const { return _chromosomes[RefID].from(); };

const TGenomePosition &TChromosomes::chrEnd(uint32_t RefID) const { return _chromosomes[RefID].to(); };

std::string TChromosomes::compileSamHeader() const {
	std::string header;
	for (auto &c : _chromosomes) { header += c.compileSamHeader(); }
	return header;
};

}; // end namespace genometools
