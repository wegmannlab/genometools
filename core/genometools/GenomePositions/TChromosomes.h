/*
 * TChromosomes.h
 *
 *  Created on: Apr 25, 2019
 *      Author: linkv
 */

#ifndef TCHROMOSOMES_H_
#define TCHROMOSOMES_H_

#include <string>
#include <vector>

#include "genometools/GenomePositions/TGenomePosition.h"
#include "genometools/GenomePositions/TGenomeWindow.h"

namespace genometools {

//---------------------------------------------------------
// TChromosome
//---------------------------------------------------------
class TChromosome {
private:
	TGenomePosition _from, _to;
	std::string _name; // SN field
	uint32_t _length = 0; // LS field
	bool _inUse      = false;
	uint8_t _ploidy  = 2;

	void _initialize(uint32_t RefID, std::string_view Name, uint32_t Length, uint8_t Ploidy);

public:	
	// the following fields are not parsed.
	// TODO: make them private and add getters
	std::string alternateLocus;     // AH field
	std::string alternativeNames;   // AN field
	std::string assemblyIdentifier; // AS field
	std::string description;        // DS field
	std::string MD5;                // M5 field
	std::string species;            // SP field
	std::string topology;           // TP field;
	std::string uri;                // UR field;

	TChromosome(uint32_t RefID, std::string_view Name, uint32_t Length);
	TChromosome(uint32_t RefID, std::string_view Name, uint32_t Length, uint8_t Ploidy);

	//getters
	[[nodiscard]] const TGenomePosition& from() const noexcept { return _from; };
	[[nodiscard]] const TGenomePosition& to() const noexcept { return _to; };
	[[nodiscard]] uint32_t refID() const noexcept { return _from.refID(); };
	[[nodiscard]] const std::string& name() const noexcept { return _name; };
	[[nodiscard]] uint32_t length() const noexcept { return _length; };
	[[nodiscard]] bool inUse() const  noexcept { return _inUse; };
	[[nodiscard]] uint8_t ploidy() const noexcept { return _ploidy; };
	[[nodiscard]] bool isHaploid() const noexcept { return _ploidy == 1; };

	//setters
	void setInUse(bool Value) noexcept { _inUse = Value; };
	void setPloidy(uint8_t Value) noexcept { _ploidy = Value; };

	bool operator<(const TChromosome &other) { return _from < other._from; };

	std::string compileSamHeader() const;
};

inline bool operator<(const TGenomePosition &pos, const TChromosome &chr) { return pos < chr.from(); };
inline bool operator<(const TChromosome &chr, const TGenomePosition &pos) { return chr.to() < pos; };
inline bool operator<(const TChromosome &chr, const TGenomeWindow &win) { return chr.to() < win.from(); };
inline bool operator<(const TGenomeWindow &win, const TChromosome &chr) { return win.to() < chr.from(); };

//---------------------------------------------------------
// TChromosomes
//---------------------------------------------------------

class TChromosomes {
public:
	using iterator       = std::vector<TChromosome>::iterator;
	using const_iterator = std::vector<TChromosome>::const_iterator;

private:
	std::vector<TChromosome> _chromosomes;

	TChromosome &_find(std::string_view chrName);
	const TChromosome &_find(std::string_view chrName) const;
	void _setAllNotInUseAndDiploid();
	void _useSpecifiedChr(std::string_view ChrName, size_t Ploidy);
	void _useSpecifiedChr(const std::vector<std::string> &ChrNames);
	void _useSpecifiedChr(const std::vector<std::pair<std::string, size_t>> &ChrNamesAndPloidy);
	bool _readChr(std::string_view String);
	bool _readChrFromFile(std::string_view Filename);
	void _limitChr(std::string_view name);
	void _setToHaploid(const std::vector<std::string> &chrNames);

public:
	TChromosomes(){};

	void clear();
	const TChromosome &appendChromosome(const TChromosome& Other);
	const TChromosome &appendChromosome(std::string_view name, uint32_t length);
	const TChromosome &appendChromosome(std::string_view name, uint32_t length, uint8_t ploidy);

	void limitAndSetPloidy();
	void limitChr();
	void setPloidy();

	void writeUsedChromosomes(bool WritePloidy = false);

	// iterate
	iterator begin() noexcept { return _chromosomes.begin(); };
	const_iterator begin() const noexcept { return _chromosomes.begin(); };
	const_iterator cbegin() const noexcept { return _chromosomes.cbegin(); };

	iterator begin(uint32_t RefID) noexcept { return _chromosomes.begin() + RefID; };
	const_iterator begin(uint32_t RefID) const noexcept { return _chromosomes.begin() + RefID; };
	const_iterator cbegin(uint32_t RefID) const noexcept { return _chromosomes.cbegin() + RefID; };

	iterator end() noexcept { return _chromosomes.end(); };
	const_iterator end() const noexcept { return _chromosomes.end(); };
	const_iterator cend() const noexcept { return _chromosomes.cend(); };

	const TChromosome& front() const noexcept {return _chromosomes.front();}
	TChromosome& front() noexcept {return _chromosomes.front();}
	const TChromosome& back() const noexcept {return _chromosomes.back();}
	TChromosome& back() noexcept {return _chromosomes.back();}

	// getters
	uint32_t size() const { return _chromosomes.size(); };
	uint32_t referenceLength() const;
	uint32_t minLength() const;
	uint32_t maxLength() const;

	// getters by name
	bool exists(std::string_view name) const;
	const TChromosome &getChromosome(std::string_view chrName) const;
	uint32_t refID(std::string_view chrName) const;

	// getters by refID
	bool exists(uint32_t RefID) const { return RefID < size(); };
	TChromosome &operator[](uint32_t RefID) { return _chromosomes[RefID]; };
	const TChromosome &operator[](uint32_t RefID) const { return _chromosomes[RefID]; };
	const TChromosome &at(uint32_t RefID) { return _chromosomes[RefID]; };
	uint32_t length(uint32_t RefID) const;
	std::string name(uint32_t RefID) const;
	bool inUse(uint32_t RefID) const;
	uint8_t ploidy(uint32_t RefID) const;
	const TGenomePosition &chrStart(uint32_t RefID) const;
	const TGenomePosition &chrEnd(uint32_t RefID) const;

	std::string compileSamHeader() const;
};

}; // end namespace genometools

#endif /* TCHROMOSOMES_H_ */
