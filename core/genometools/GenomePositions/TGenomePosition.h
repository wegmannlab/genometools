#ifndef TGENOMEPOSITION_H_
#define TGENOMEPOSITION_H_

#include "coretools/Main/TError.h"

namespace genometools {

//-----------------------------------------------------
// TGenomePosition
//-----------------------------------------------------
// forward declarations;

class TChromosomes;

class TGenomePosition {
protected:
	size_t _refID    = 0;
	size_t _position = 0;
public:
	constexpr TGenomePosition() = default;
	constexpr TGenomePosition(size_t RefID, size_t Position) : _refID(RefID), _position(Position) {};

	constexpr void clear() {
		_refID    = 0;
		_position = 0;
	};

	constexpr size_t refID() const noexcept { return _refID; };
	constexpr size_t position() const noexcept { return _position; };

	constexpr void move(size_t RefID, size_t Position) noexcept {
		_refID    = RefID;
		_position = Position;
	};

	constexpr void move(const TGenomePosition &other) {
		*this = other;
	};

	constexpr void moveOnRef(size_t Position) noexcept {
		_position = Position;
	};

	constexpr TGenomePosition operator+(size_t length) const noexcept {
		return TGenomePosition(_refID, _position + length);
	};

	constexpr TGenomePosition operator-(size_t length) const noexcept {
		return length > _position ? TGenomePosition(_refID, 0) : TGenomePosition(_refID, _position - length);
	};

	constexpr size_t operator-(const TGenomePosition &other) const {
		if (!sameChr(other)) { DEVERROR("positions are not on same chromosome!"); }
		if (other._position > _position) return 0;
		return _position - other._position;
	};

	constexpr void operator+=(size_t length) noexcept {
		_position += length;
	};

	constexpr void operator-=(size_t length) noexcept {
		_position = length > _position ? 0 : _position - length;
	}

	constexpr void operator++() noexcept {
		++_position;
	};

	constexpr void operator--() noexcept {
	if (_position > 0) --_position;
	}

	constexpr bool sameChr(const TGenomePosition &other) const noexcept {
		return _refID == other.refID();
	};

	constexpr bool operator==(const TGenomePosition &other) const noexcept {
		return this->_refID == other._refID && this->_position == other._position;
	};

	constexpr bool operator<(const TGenomePosition &other) const noexcept {
		// on same chromosome: check position
		return this->_refID < other._refID || (this->_refID == other._refID && this->_position < other._position);
	}

	constexpr bool operator>(const TGenomePosition &other) const noexcept {
		// on same chromosome: check position
		return this->_refID > other._refID || (this->_refID == other._refID && this->_position > other._position);
	};

	constexpr bool operator>=(const TGenomePosition &other) const noexcept {
		if (*this == other) { return true; }
		return *this > other;
	};

	constexpr bool operator<=(const TGenomePosition &other) const noexcept {
		if (*this == other) { return true; }
		return *this < other;
	};

	operator std::string() const { 
		//return internal string (i.e. 0-based)
		return coretools::str::toString(_refID) + ":" + coretools::str::toString(_position);
	};
	
	std::string asFormattedString(const TChromosomes & Chromosomes, std::string delim = ":") const; // for users, i.e. 1-based
};

}; // end namespace genometools

#endif /* BAM_TGENOMEPOSITION_H_ */
