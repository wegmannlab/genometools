/*
 * TFastaWriter.h
 *
 *  Created on: Jan 23, 2023
 *      Author: phaentu
 */

#ifndef GENOMETOOLS_CORE_GENOMETOOLS_TFASTAWRITER_H_
#define GENOMETOOLS_CORE_GENOMETOOLS_TFASTAWRITER_H_

#include <string>

#include "coretools/Files/TOutputFile.h"
#include "genometools/Genotypes/Base.h"


namespace genometools {

class TFastaWriter{
private:
	uint32_t _lineLength;

	coretools::TOutputFile _fastaIndexFile;
	coretools::TOutputFile _fastaFile;

	//book keeping keeping for index
	std::string _curContigName;
	uint32_t _curBassOnLine{};
	uint32_t _curLength{};
	uint32_t _curNumLinesWritten{};
	uint64_t _curOffset{};

	//functions
	void _writeNewline(){
		_fastaFile.endln();
		_curLength += _curBassOnLine;
		++_curNumLinesWritten;
		_curBassOnLine = 0;
	}

	void _write(Base base){
		_fastaFile.writeNoDelim(base2char(base));
		++_curBassOnLine;

		//update book keeping
		if(_curBassOnLine == _lineLength){
			_writeNewline();
		}
	}

	void _writeIndexOfCurrentContig(){
		if(_fastaFile.isOpen() && _curOffset > 0){
			_fastaIndexFile.writeln(_curContigName, _curLength, _curOffset, _lineLength, _lineLength + 1);
		}
	}

public:
	TFastaWriter(int LineLength = 80) : _lineLength(LineLength){}
	TFastaWriter(std::string Filename, int LineLength = 80) : _lineLength(LineLength){
		//open files
		open(Filename);
	}

	~TFastaWriter(){
		close();
	}

	void open(std::string Filename){
		_fastaFile.open(Filename);
		_fastaIndexFile.open(Filename + ".fai", 5);
	}

	void close(){
		if(_fastaFile.isOpen()){
			if(_curBassOnLine > 0){
				_writeNewline();
			}
			_writeIndexOfCurrentContig();
			_fastaFile.close();
			_fastaIndexFile.close();
		}
	}

	void newContig(std::string_view ContigName){
		//write new line
		if(_curBassOnLine > 0){
			_writeNewline();
		}

		//write index of current contig
		_writeIndexOfCurrentContig();

		//write contig name to fasta file
		_fastaFile.writeNoDelim(">", ContigName).endln();

		//update book keeping
		//offset of new contig  = old offset + length of previous contig + new line characters + name of new contig (+2 for > and new line)
		_curContigName = ContigName;
		_curOffset += _curLength + _curNumLinesWritten + ContigName.size() + 2; // +2 for > and new line
		_curLength = 0;
		_curNumLinesWritten = 0;
	}

	void write(Base base){
		_fastaFile.writeNoDelim(base2char(base));
		++_curBassOnLine;

		//update book keeping
		if(_curBassOnLine == _lineLength){
			_writeNewline();
		}
	}

};


} // end namesapce genometools


#endif /* GENOMETOOLS_CORE_GENOMETOOLS_TFASTAWRITER_H_ */
