#ifndef TGLFREADER_H_
#define TGLFREADER_H_

#include "GLF.h"
#include "TGLFIndex.h"
#include "coretools/Files/TReader.h"
#include "genometools/GenomePositions/TGenomePosition.h"

namespace genometools {

class TGLFReader {
private:
	TGenomePosition _eof = TGenomePosition(-1, -1);
	bool _hasIndex       = false;

	// about site
	TGLFEntry _entry;
	TGenomePosition _position;
	int _recordType          = 99;
	uint8_t _RMS_mappingQual = 0;

	TGLFIndex _index;
	std::unique_ptr<coretools::TReader> _reader = std::make_unique<coretools::TNoReader>();

	bool _readChr();
	bool _readRecordType();
	void _readSNPRecord();
	void _writeIndex();

public:
	TGLFReader() = default;
	TGLFReader(const std::string &Filename) {
		open(Filename);
	}

	bool empty() const noexcept { return _position == _eof; };
	void popFront();
	const TGLFEntry &front() const noexcept { return _entry; };

	// get details
	const TChromosomes& chromosomes() const;
	const TChromosome& curChromosome() const;
	size_t lastRefID() const;
	uint32_t refID() const noexcept { return _position.refID(); };
	const TGLFIndex& index() const noexcept { return _index; };
	TGenomePosition position() const noexcept { return _position; };
	uint16_t depth() const noexcept { return _entry.depth; };

	const std::string& name() const {return _reader->name();}

	// open file and parse header
	void open(const std::string &Filename);
	void rewind();

	// parse file
	bool jumpToChr(uint32_t RefID, bool OnlyForward=true);
	bool jumpToNextChr();
	bool jumpToPositionOrBeyond(const TGenomePosition& Position, bool OnlyForward=true);
	bool readNextWindow(std::vector<TGLFLikelihoods> &GenoLikelihoods, TGenomeWindow Window);

	// printing
	void printChr();
	void printSite();
	void printToEnd();

	const TChromosome& curChr() const noexcept {return _index.chromosomes()[refID()];}
};

}
#endif
