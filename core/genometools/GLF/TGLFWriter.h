#ifndef TGLFWRITER_H_
#define TGLFWRITER_H_

#include <memory>
#include <string>

#include "coretools/Files/TWriter.h"
#include "genometools/Genotypes/Containers.h"

#include "TGLFIndex.h"

namespace genometools {

class TGLFWriter {
private:
	size_t _oldPos = 0;
	size_t _nSites = 0;
	std::string _header;

	TGLFIndex _index;
	std::unique_ptr<coretools::TWriter> _writer = std::make_unique<coretools::TNoWriter>();

	void _writeHeader(std::string_view Header);

public:
	TGLFWriter() = default;
	TGLFWriter(std::string_view Filename, const TChromosomes &Chrs) {
		open(Filename, Chrs, "");
	};
	~TGLFWriter(){ close(); };

	// open & close streams
	void open(std::string_view Filename, const TChromosomes &Chrs, std::string_view Header = "");
	void newChromosome(const TChromosome &chromosome);
	void writeSite(long pos, uint32_t depth, uint8_t RMS_mappingQual,
			   const TGenotypeLikelihoods &genotypeLikelihoods);
	void close();
};

}

#endif
