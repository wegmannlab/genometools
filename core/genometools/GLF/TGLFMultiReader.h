/*
 * TGlfMultiReader.h
 *
 *  Created on: Feb 11, 2020
 *      Author: wegmannd
 */

#ifndef GLF_TGLFMULTIREADER_H_
#define GLF_TGLFMULTIREADER_H_

#include <string>
#include <vector>

#include "TGLFReader.h"
#include "genometools/GLF/GLF.h"
#include "genometools/TAlleles.h"

#include "genometools/TFastaReader.h"

namespace genometools {


//--------------------------------------------
// TGlfVector
//--------------------------------------------
class TGLFVector{
private:
	std::vector<TGLFReader> _GLFs;
	std::vector<std::string> _sampleNames;
	bool _sampleNamesProvided;

	void _openFiles(const std::vector<std::string>& FileNames);

public:
	TGLFVector();

	size_t size() const noexcept { return _GLFs.size(); }

	auto begin() const noexcept { return _GLFs.begin(); }
	auto end() const noexcept { return _GLFs.end(); }

	auto begin() noexcept { return _GLFs.begin(); }
	auto end() noexcept { return _GLFs.end(); }

	const TGLFReader& operator[](size_t i) const noexcept {
		assert(i < size());
		return _GLFs[i];
	}
	TGLFReader& operator[](size_t i) noexcept {
		assert(i < size());
		return _GLFs[i];
	}
	const std::string& fileName(size_t i) const noexcept {
		assert(i < size());
		return _GLFs[i].name();
	}
	const std::string& sampleName(size_t i) const noexcept {
		assert(i < size());
		return _sampleNames[i];
	}
};

//----------------------------------------------------
// TGlfMultiReader
//----------------------------------------------------
class TGLFMultiReader {
private:
	TGLFVector _GLFs;
	std::vector<size_t> _ids;

	size_t _minSamplesWithData = 1;
	uint32_t _curRefId         = 0;
	uint32_t _minDepth         = 0;
	size_t _windowSize         = 100000;

	TGenomeWindow _curWindow;
	std::vector<std::vector<TGLFEntry>> _dataWindow;

	// reference
	TFastaReader _fastaReader;
	const TAlleles* _alleles = nullptr;

	void _openGLFs();
	int _getGLFIndexFromName(const std::string &name) const;
	void _prepareParsing();
	void _jumpToNextPosition();
	bool _moveToNextChromosome();

	void _readWindow();
	void _readWindowAlleles();

public:
	TGLFMultiReader();

	bool empty() const noexcept { return _ids.empty(); }
	void popFront();
	const std::vector<size_t> &front() const { return _ids; }

	const std::vector<TGLFEntry>& data(size_t iWindow) const noexcept {
		assert(iWindow < _dataWindow.size());
		return _dataWindow[iWindow];
	}

	void addReference(const std::string& FastaFile);
	const TChromosomes& chromosomes() const {
		return _GLFs[0].chromosomes();
	}

	const TChromosome& curChr() const noexcept {
		return chromosomes()[_curRefId];
	}

	void setMinSamplesWithData(size_t MinSamplesWithData) noexcept {
		_minSamplesWithData = MinSamplesWithData;
		_prepareParsing();
	};
	void setAlleles(const TAlleles& Alleles) noexcept {
		_alleles = &Alleles;
		_prepareParsing();
	}

	std::vector<std::string> sampleNames() const;

	// access data
	size_t numActiveSamples() const noexcept { return _GLFs.size(); }
	std::string curChrName() const { return curChr().name(); }
	const TGenomeWindow& curWindow() const { return _curWindow; };
	TGenomePosition position(size_t iWindow) const noexcept { return _curWindow.from() + iWindow; }
	bool hasRef() const noexcept {return _fastaReader.isOpen();}
	coretools::TView<Base> refView() const {
		assert(hasRef());
		return _fastaReader.view(_curWindow);
	}
};

}; // end namespace GLF

#endif /* GLF_TGLFMULTIREADER_H_ */
