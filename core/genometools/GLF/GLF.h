#ifndef GLF_H_
#define GLF_H_

#include "genometools/Genotypes/Base.h"
#include "genometools/Genotypes/Genotype.h"
#include "genometools/Genotypes/TPloidEntry.h"

namespace genometools {

using TGLFEntry       = TPloidEntry<Base, Genotype>;
using TGLFLikelihoods = TGLFEntry::DualArray;

constexpr std::string_view GLFversion() noexcept { return "GLF2"; }

}; // namespace GLF

#endif /* TGLF_H_ */
