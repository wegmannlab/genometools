#include "TBed.h"

#include <filesystem>

#include "coretools/Files/TLineReader.h"
#include "coretools/Files/TOutputFile.h"
#include "coretools/Strings/fromString.h"
#include "coretools/Strings/splitters.h"
#include "coretools/Strings/stringManipulations.h"

#include "genometools/GenomePositions/TChromosomes.h"

namespace genometools {
using namespace coretools::str;

void TBed::_addLine(std::string_view Line, std::string_view Sep, bool addMissing) {
	TSplitter<std::string_view, true> inner(Line, Sep);
	const auto chr = inner.front();
	inner.popFront();
	const auto from = fromString<size_t, true>(inner.front());
	inner.popFront();
	const auto to = fromString<size_t, true>(inner.front());
	if (to <= from) UERROR("From-position must be smaller than To position!");
	inner.popFront();
	if (!inner.empty()) UERROR("Unknown bed format '", Line, "'!");

	const auto rID = refID(chr);
	if (rID == _chromosomes.size()) {
		if (!addMissing) UERROR("Unknown chromosome '", chr, "'!");
		_chromosomes.emplace_back(chr);
	}
	_allWindows.emplace_back(rID, from, to - from);
}

void TBed::_merge() {
	auto copy = std::move(_allWindows);
	std::sort(copy.begin(), copy.end());

	_allWindows.clear();
	_allWindows.push_back(copy.front());

	for (const auto& w: copy) {
		auto& back = _allWindows.back();
		if (back.to() >= w.from()) { //overlap
			if (w.to().position() > back.to().position()) {
				back.resize(w.to().position() - back.from().position());
			}
		} else {
			_allWindows.push_back(w);
		}
	}

	// index + count
	_windows.assign(_chromosomes.size(), {end(), 0});
	_nChrWindows = 0;
	auto refID   = _allWindows.front().refID();
	auto data    = _allWindows.data();
	_length      = _allWindows.front().size();
	size_t n     = 1;
	for (size_t i = 1; i < _allWindows.size(); ++i) {
		_length += _allWindows[i].size();
		if (_allWindows[i].refID() != refID) {
			_windows[refID] = {data, n};
			data            = _allWindows.data() + i;
			n               = 1;
			refID           = _allWindows[i].refID();
			++_nChrWindows;
		} else {
			n++;
		}
	}
	_windows[refID] = {data, n};
	++_nChrWindows;
}

void TBed::parse(std::string_view Str, const TChromosomes &Chromosomes) {
	_chromosomes.assign(Chromosomes.size(), "");
	for (const auto &chr : Chromosomes) { _chromosomes[chr.refID()] = chr.name(); }
	parse(Str);
}

void TBed::parse(std::string_view Str, coretools::TConstView<std::string> Chromosomes) {
	_chromosomes.clear();
	_chromosomes.reserve(Chromosomes.size());
	for (const auto &chr : Chromosomes) { _chromosomes.push_back(chr); }
	parse(Str);
}

void TBed::parse(std::string_view Str) {
	_allWindows.clear();
	_windows.clear();
	if (Str.empty()) { return; }

	if (std::filesystem::exists(coretools::str::readBefore(Str, ",;", true))) {
		const auto files = fromString<std::vector<std::string>>(Str);
		for (const auto &file : files) {
			if (!std::filesystem::exists(file)) UERROR("File '", file, "' does not exist!");
			for (auto lReader = coretools::TLineReader(file); !lReader.empty(); lReader.popFront()) {
				_addLine(lReader.front(), coretools::str::whitespaces, true);
			}
		}
	} else {
		coretools::str::TSplitter outer(Str, ';');
		for (auto spl : outer) {
			try {
			_addLine(spl, ", \t", true);
			}
			catch(...) {
				UERROR("File '", spl, "'does not exist!");
			}
		}
	}
	if (_allWindows.empty()) {
		coretools::instances::logfile().warning("Empty BED-file!");
		return;
	}
	_merge();
}

void TBedWriter::write(std::string_view Filename) {
	_merge();
	coretools::TOutputFile out(Filename,3);
	for (const auto& w: _allWindows) {
		out.writeln(_chromosomes[w.refID()], w.fromOnChr(), w.toOnChr());
	}
}

void TBedWriter::_merge() {
	auto copy = std::move(_allWindows);
	std::sort(copy.begin(), copy.end());

	_allWindows.clear();
	_allWindows.push_back(copy.front());

	for (const auto& w: copy) {
		if (_allWindows.back().to() >= w.from()) { //overlap
			if (w.to().position() > _allWindows.back().to().position()) {
				_allWindows.back().resize(w.to().position() - _allWindows.back().from().position());
			}
		} else {
			_allWindows.push_back(w);
		}
	}
}

void TBedWriter::parse(const TChromosomes &Chromosomes) {
	_chromosomes.assign(Chromosomes.size(), "");
	for (const auto &chr : Chromosomes) { _chromosomes[chr.refID()] = chr.name(); }
}

}; // end namespace genometools
