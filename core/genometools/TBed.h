#ifndef TBED_H_
#define TBED_H_

#include <algorithm>

#include "coretools/Containers/TView.h"

#include "genometools/GenomePositions/TGenomePosition.h"
#include "genometools/GenomePositions/TGenomeWindow.h"

namespace genometools {
class TChromosomes;

class TBed {
	using Windows = coretools::TConstView<TGenomeWindow>;
	std::vector<std::string> _chromosomes;
	std::vector<TGenomeWindow> _allWindows;
	std::vector<Windows> _windows;

	size_t _length      = 0;
	size_t _nChrWindows = 0;

	void _addLine(std::string_view Line, std::string_view Sep, bool addMissing); 
	void _merge(); 

public:
	using iterator       = Windows::iterator;
	using const_iterator = Windows::const_iterator;

	TBed() = default;
	TBed(std::string_view Str) { parse(Str); }
	TBed(std::string_view Str, const TChromosomes &Chromosomes) {parse(Str, Chromosomes);}
	TBed(std::string_view Str, coretools::TConstView<std::string> Chromosomes) {parse(Str, Chromosomes);}

	void parse(std::string_view Str); 
	void parse(std::string_view Str, const TChromosomes &Chromosomes);
	void parse(std::string_view Str, coretools::TConstView<std::string> Chromosomes);

	size_t length() const noexcept { return _length; }
	size_t NChrWindows() const noexcept { return _nChrWindows; }
	size_t NChr() const noexcept { return _chromosomes.size(); }

	// Chromosome acces
	size_t refID(std::string_view Name) const noexcept {
		return std::distance(_chromosomes.cbegin(), std::find(_chromosomes.cbegin(), _chromosomes.cend(), Name));
	}
	size_t size() const noexcept { return _allWindows.size(); }
	bool empty() const noexcept { return _chromosomes.empty(); }
	const std::string &name(size_t RefID) const { return _chromosomes[RefID]; }

	// Window access
	const Windows &window(size_t RefID) const noexcept { return _windows[RefID]; }
	const Windows &window(std::string_view Name) const noexcept { return window(refID(Name)); }
	template<typename Key> const Windows &operator[](const Key& K) const noexcept { return window(K); }

	const_iterator begin() const noexcept { return _allWindows.data(); }
	const_iterator end() const noexcept { return _allWindows.data() + _allWindows.size(); }

	template<typename Key> size_t size(const Key& K) const noexcept { return window(K).size(); }
	template<typename Key> bool empty(const Key& K) const noexcept { return window(K).empty(); }

	const_iterator begin(size_t K) const noexcept { return window(K).begin(); }
	const_iterator begin(std::string_view K) const noexcept { return window(K).begin(); }
	const_iterator begin(const TGenomeWindow &Window) const noexcept {
		const auto &ws = window(Window.refID());

		auto begin = std::lower_bound(ws.begin(), ws.end(), Window);
		if (begin != ws.begin()) begin--;

		for (auto it = begin; it != ws.end(); ++it) {
			if (it->overlaps(Window)) return it;
			if (it->from().position() > Window.to().position()) break;
		}
		return end();
	}
	bool overlaps(const TGenomeWindow &Window) const noexcept { return (begin(Window) != end()); }
	bool overlaps(const TGenomePosition &Position) const noexcept { return (begin(TGenomeWindow{Position, 1}) != end()); }
};

class TBedWriter {
	std::vector<std::string> _chromosomes;
	std::vector<TGenomeWindow> _allWindows;

	void _merge();
public:
	TBedWriter() = default;
	TBedWriter(const TChromosomes &Chromosomes) {parse(Chromosomes);}

	void parse(const TChromosomes &Chromosomes);
	void write(std::string_view Filename);

	void add(const TGenomeWindow &Window) { _allWindows.push_back(Window); }
	void add(const TGenomePosition &Position) { _allWindows.emplace_back(Position); }
	void add(std::string_view Name, size_t from, size_t length=1) {
		const size_t refID = std::distance(_chromosomes.cbegin(), std::find(_chromosomes.cbegin(), _chromosomes.cend(), Name));
		if (refID == _chromosomes.size()) _chromosomes.emplace_back(Name);
		_allWindows.emplace_back(refID, from, length);

	}
};

} // namespace genometools

#endif /* TBED_H_ */
