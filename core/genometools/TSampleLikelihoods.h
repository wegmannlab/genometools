//
// Created by caduffm on 6/15/21.
//

#ifndef BANGOLIN_TSAMPLELIKELIHOODS_H
#define BANGOLIN_TSAMPLELIKELIHOODS_H

#include "coretools/Containers/TBitSet.h"
#include "coretools/Types/probability.h"
#include "genometools/THardyWeinbergGenotypeProbabilities.h"
#include <array>

namespace genometools {

template<typename Type>
// Type can be coretools::PhredInt or coretools::HPPhredInt
class TSampleLikelihoods {
public:
	using value_type = typename Type::value_type;

protected:
	/*
	 * store genotype likelihoods
	 * -> we want to avoid storing 3 GTL + a boolean for missingness + a boolean for haploid/diploid for each sample &
	 * locus
	 * -> insight: genotype likelihoods can be scaled, such that best genotype has likelihood = 1
	 * -> we only need to store the likelihoods for the two other (worse) genotypes _gtl
	 */
	std::array<Type, 2> _gtl;
	constexpr static const Type _maxGTL = Type::highest();

	/*
	 * We saved space by only storing 2 GTL; but we now need a code that tells us which is the best genotype; and
	 * whether or not it is missing or haploid encode these cases with a bitset: best genotype, ploidy and missingness
	 * first two bits (from right): encode best genotype
	 *      0 0 : homoFirst is max
	 *      0 1 : het is max
	 *      1 0 : homoSecond is max
	 *  next bit encodes diploid/haploid
	 *      0   : diploid
	 *      1   : haploid
	 *  last bit encodes missingness
	 *      0   : not missing
	 *      1   : missing
	 * results in 4 "meaningful" bits with 2 states each -> 2^4 = 16 combinations -> these are represented in
	 * _caseGenotypeLookup
	 */
	coretools::TBitSet<4> _case;

	/*
	 * lookup table for cases and genotype
	 * 16 times 7 dimensions (16 possible cases, 7 possible input genotypes (homoFirst, het, homoSecond, missingDiploid,
	 * haploidFirst, haploidSecond, missingHaploid)) Note: since this is a static array, there should be no overhead in
	 * it being nested.
	 *
	 * Elements in matrix encode the following:
	 *      0 -> index 0 in _gtl -> will return _gtl[0]
	 *      1 -> index 1 in _gtl -> will return _gtl[1]
	 *      2 -> best genotype or missing genotype -> will result in 1.0
	 *      3 -> invalid missing genotype 3 or 6 (diploid or haploid)
	 *      4 -> bug in program!! Combination of cases should never result in these values (We have more combinations
	 * than possible cases, because we need 2 bits to encode genotype 0,1,2 -> this also includes 3 as 1 1) -> will
	 * throw
	 */
	constexpr static const std::array<std::array<uint8_t, 7>, 16> _caseGenotypeLookup = {{
	    {2, 0, 1, 3, 4, 4, 4}, // not missing, diploid, geno0 is max
	    {0, 2, 1, 3, 4, 4, 4}, // not missing, diploid, geno1 is max
	    {0, 1, 2, 3, 4, 4, 4}, // not missing, diploid, geno2 is max
	    {4, 4, 4, 4, 4, 4, 4}, // not missing, diploid, geno3 is max -> impossible

	    {4, 4, 4, 4, 2, 0, 3}, // not missing, haploid, geno0 is max
	    {4, 4, 4, 4, 4, 4, 4}, // not missing, haploid, geno1 is max -> impossible
	    {4, 4, 4, 4, 0, 2, 3}, // not missing, haploid, geno2 is max
	    {4, 4, 4, 4, 4, 4, 4}, // not missing, haploid, geno3 is max -> impossible

	    {2, 2, 2, 3, 4, 4, 4}, // missing, diploid
	    {2, 2, 2, 3, 4, 4, 4}, // missing, diploid
	    {2, 2, 2, 3, 4, 4, 4}, // missing, diploid
	    {4, 4, 4, 4, 4, 4, 4}, // missing, diploid, geno3 is max -> impossible

	    {4, 4, 4, 4, 2, 2, 3}, // missing, haploid
	    {4, 4, 4, 4, 4, 4, 4}, // missing, haploid, geno1 is max -> impossible
	    {4, 4, 4, 4, 2, 2, 3}, // missing, haploid
	    {4, 4, 4, 4, 4, 4, 4}, // missing, haploid, geno3 is max -> impossible
	}};

	void _setMissing() {
		// missing: set all genotype likelihoods to the same value
		_gtl[0] = Type::highest();
		_gtl[1] = Type::highest();

		_case.reset();
		_case.set<3>(true); // missing
	}

	void _scaleDiploid(Type &GTL0, Type &GTL1, Type &GTL2) const noexcept {
		const auto highest = std::min({GTL0, GTL1, GTL2});

		GTL0.scale(highest);
		GTL1.scale(highest);
		GTL2.scale(highest);
	}

	void _scaleHaploid(Type &GTL0, Type &GTL2) const noexcept {
		const auto highest = std::min({GTL0, GTL2});

		GTL0.scale(highest);
		GTL2.scale(highest);
	}

	constexpr coretools::Probability _HWESumHaploid(const coretools::Probability HaploidFirst,
	                                                const coretools::Probability HaploidSecond) const noexcept {
		using coretools::P;
		if (_case.get<1>()) { // homoSecond is max
			return P(HaploidFirst * coretools::Probability(_gtl[0]) + HaploidSecond);
		} // homoFirst is max
		return P(HaploidFirst + HaploidSecond * coretools::Probability(_gtl[0]));
	}

	constexpr coretools::Probability _HWESumDiploid(const THardyWeinbergGenotypeProbabilities &HWProbs) const noexcept {
		using BG = BiallelicGenotype;
		if (_case.get<0>()) { // geno1 is max
			return coretools::Probability(HWProbs[BG::homoFirst] * coretools::Probability(_gtl[0]) + HWProbs[BG::het] +
			                              HWProbs[BG::homoSecond] * coretools::Probability(_gtl[1]));
		}
		if (_case.get<1>()) { // geno2 is max
			return coretools::Probability(HWProbs[BG::homoFirst] * coretools::Probability(_gtl[0]) +
			                              HWProbs[BG::het] * coretools::Probability(_gtl[1]) + HWProbs[BG::homoSecond]);
		} // geno0 is max
		return coretools::Probability(HWProbs[BG::homoFirst] + HWProbs[BG::het] * coretools::Probability(_gtl[0]) +
		                              HWProbs[BG::homoSecond] * coretools::Probability(_gtl[1]));
	}

public:
	explicit TSampleLikelihoods() noexcept {
		// default / missing constructor: diploid
		setMissingDiploid();
	}

	TSampleLikelihoods(Type GTL0, Type GTL1, Type GTL2) noexcept {
		// diploid constructor
		setDiploid(GTL0, GTL1, GTL2);
	}

	TSampleLikelihoods(Type GTL0, Type GTL1) noexcept {
		// haploid constructor
		setHaploid(GTL0, GTL1);
	}

	void setDiploid(Type GTL0, Type GTL1, Type GTL2) noexcept {
		// decide which genotype is the best
		// Note: if GTL are not scaled yet, we will scale them in here and call function again
		_case.reset();
		if (GTL0 == Type::highest()) {
			// case 0
			_gtl[0] = GTL1;
			_gtl[1] = GTL2;
		} else if (GTL1 == Type::highest()) {
			// case 1
			_gtl[0] = GTL0;
			_gtl[1] = GTL2;
			_case.set<0>(true); // het is max
		} else if (GTL2 == Type::highest()) {
			// case 2
			_gtl[0] = GTL0;
			_gtl[1] = GTL1;
			_case.set<1>(true); // homoSecond is max
		} else {
			// scale and call function again
			_scaleDiploid(GTL0, GTL1, GTL2);
			setDiploid(GTL0, GTL1, GTL2);
		}
	}

	void setHaploid(Type GTL0, Type GTL2) noexcept {
		// decide which genotype is the best
		// Note: if GTL are not scaled yet, we will scale them in here and call function again
		_case.reset();
		if (GTL0 == Type::highest()) {
			// case 4
			_gtl[0] = GTL2;
			_case.set<2>(true); // haploid
		} else if (GTL2 == Type::highest()) {
			// case 5
			_gtl[0] = GTL0;
			_case.set<1>(true); // homoSecond is max
			_case.set<2>(true); // haploid
		} else {
			// scale and call function again
			_scaleHaploid(GTL0, GTL2);
			setHaploid(GTL0, GTL2);
		}
	}

	void setMissingHaploid() noexcept {
		// cases 12-13
		_setMissing();
		_case.set<2>(true); // haploid
	}

	void setMissingDiploid() noexcept {
		// cases 8-10
		_setMissing();
	}

	constexpr Type operator[](const BiallelicGenotype Genotype) const {
		using coretools::str::toString;
		const uint8_t x = _caseGenotypeLookup[_case.to_ulong()][coretools::index(Genotype)];
		if (x < 2) {
			return _gtl[x];
		} else if (x == 2) {
			return _maxGTL;
		} else if (x == 3) {
			DEVERROR("Invalid missing genotype ", Genotype, "!");
		} else { // this should never happen, as this represents invalid combination of cases
			DEVERROR("How did you get here? X = ", Genotype, "; _case = ", _case.to_ulong(),
			         "; genotype = ", (uint8_t)Genotype, ".");
		}
	}

	friend bool operator==(const TSampleLikelihoods<Type> &lhs, const TSampleLikelihoods<Type> &rhs) {
		return lhs._case == rhs._case && lhs._gtl == rhs._gtl;
	}

	template<typename ReturnType>
	[[nodiscard]] constexpr ReturnType HWESum(const coretools::Probability f) const noexcept {
		// calculate weighted sum over genotypes
		//   sum_g P(D|g) P(g|f),
		// where P(D|g) are genotype likelihoods (stored by this class),
		// and P(g|f) are distributed according to Hardy-Weinberg.
		if (isMissing()) return ReturnType::highest();
		if (isHaploid()) return ReturnType(_HWESumHaploid(f.complement(), f));
		return ReturnType(_HWESumDiploid(THardyWeinbergGenotypeProbabilities(f)));
	}

	template<typename ReturnType>
	[[nodiscard]] constexpr ReturnType HWESum(const THardyWeinbergGenotypeProbabilities &HWProbs) const noexcept {
		// calculate weighted sum over genotypes
		//   sum_g P(D|g) P(g|f),
		// where P(D|g) are genotype likelihoods (stored by this class),
		// and P(g|f) are distributed according to Hardy-Weinberg.
		if (isMissing()) return ReturnType::highest();
		if (isHaploid())
			return ReturnType(
			    _HWESumHaploid(HWProbs[BiallelicGenotype::haploidFirst], HWProbs[BiallelicGenotype::haploidSecond]));
		return ReturnType(_HWESumDiploid(HWProbs));
	}

	[[nodiscard]] bool isMissing() const noexcept { return _case.get<3>(); }

	[[nodiscard]] bool isHaploid() const noexcept { return _case.get<2>(); }

	void resetMissing() noexcept { _case.set<3>(false); }

	[[nodiscard]] BiallelicGenotype mostLikelyGenotype() const {
		using BG = BiallelicGenotype;
		if (isHaploid()) return _case.get<1>() ? BG::haploidSecond : BG::haploidFirst;

		if (_case.get<0>()) { // geno1 is max
			return BG::het;
		}
		if (_case.get<1>()) { // geno2 is max
			return BG::homoSecond;
		} // geno0 is max
		return BG::homoFirst;
	}

	[[nodiscard]] BiallelicGenotype secondMostLikelyGenotype() const noexcept {
		using BG = BiallelicGenotype;
		if (isHaploid()) return _case.get<1>() ? BG::haploidFirst : BG::haploidSecond;

		// het is max
		if (_case.get<0>()) return _gtl[0].moreProbable(_gtl[1]) ? BG::homoFirst : BG::homoSecond;
		// homoSecond is max
		if (_case.get<1>()) return _gtl[0].moreProbable(_gtl[1]) ? BG::homoFirst : BG::het;
		// homoFirst is max
		return _gtl[0].moreProbable(_gtl[1]) ? BG::het : BG::homoSecond;
	}

	[[nodiscard]] double meanPosteriorGenotype() const {
		using BG = BiallelicGenotype;
		// meanPosteriorGenotype = weighted sum over P(g | D)
		//                       = (P(D | g = 1) + 2*P(D | g = 2)) / (sum_g P(D | g))
		using coretools::Probability;
		if (isHaploid()) {
			const double denumerator = coretools::Probability(_gtl[0]) + 1.;
			return (double)coretools::Probability(operator[](BG::haploidSecond)) / denumerator;
		}
		const double denumerator =
		    (double)coretools::Probability(_gtl[0]) + (double)coretools::Probability(_gtl[1]) + 1.;
		return ((double)coretools::Probability(operator[](BG::het)) +
		        2. * coretools::Probability(operator[](BG::homoSecond))) /
		       denumerator;
	}

	[[nodiscard]] coretools::Probability meanPosteriorAlleleFrequency() const noexcept {
		using coretools::P;
		if (isHaploid()) { return P(meanPosteriorGenotype()); }
		return P(0.5 * meanPosteriorGenotype());
	}

	explicit operator std::string() const {
		using BG = BiallelicGenotype;
		if (isMissing()) { return std::string(1, '.'); }
		if (isHaploid()) {
			return fmt::format("{},{}", operator[](BG::haploidFirst).get(), operator[](BG::haploidSecond).get());
		} else {
			return fmt::format("{},{},{}", operator[](BG::homoFirst).get(), operator[](BG::het).get(),
			                                                                operator[](BG::homoSecond).get());
		}
	}

	// Stream
	friend std::ostream &operator<<(std::ostream &os, const TSampleLikelihoods &Other) {
		using BG = BiallelicGenotype;
		// corresponds to VCF format
		if (Other.isMissing()) {
			os << '.';
		} else {
			if (Other.isHaploid()) {
				os << Other[BG::haploidFirst] << ',' << Other[BG::haploidSecond];
			} else {
				os << Other[BG::homoFirst] << ',' << Other[BG::het] << ',' << Other[BG::homoSecond];
			}
		}
		return os;
	}
};

} // end namespace genometools

template<typename T> struct fmt::formatter<genometools::TSampleLikelihoods<T>> : formatter<std::string> {
	template<typename FmtContext>
	constexpr auto format(const genometools::TSampleLikelihoods<T> &s, FmtContext &ctx) const {
		return formatter<std::string>::format(static_cast<std::string>(s), ctx);
	}
};

#endif // BANGOLIN_TSAMPLELIKELIHOODS_H
