
#ifndef DEFAULTCOLUMNNAMES_H
#define DEFAULTCOLUMNNAMES_H

#include <array>
#include <string>

namespace genometools{

//--------------------------------------------
// DefaultColumnNames
// This file defines default column names for standard columns, along with alternatives
//--------------------------------------------


const std::array<std::string, 7> defaultColumnNames_chromosome = {"Chr", "CHR", "chr", "Chromosome", "chromosome", "CHROMOSOME", "#CHROM"};
const std::array<std::string, 6> defaultColumnNames_position = {"Pos", "pos", "POS", "Position", "position", "POSITION"};
const std::array<std::string, 8> defaultColumnNames_reference = {"Ref", "ref", "REF", "Reference", "reference", "REFERENCE", "Allele1", "Allele"};
const std::array<std::string, 7> defaultColumnNames_alternative = {"Alt", "alt", "ALT", "Alternative", "alternative", "ALTERNATIVE", "Allele2"};
const std::array<std::string, 6> defaultColumnNames_sample = {"Sample", "sample", "SAMPLE", "Samples", "samples", "SAMPLES"};
const std::array<std::string, 9> defaultColumnNames_population = {"Population", "population", "POPULATION", "Populations", "populations", "POPULATIONS", "Pop", "pop", "POP"};

}; //end namespace


#endif // DEFAULTCOLUMNNAMES_H