 /*
 *  Created on: Jan 24, 2023
 *      Author: raphael
 */

#ifndef GENOMETOOLS_CORE_GENOMETOOLS_TFASTAINDEX_H_
#define GENOMETOOLS_CORE_GENOMETOOLS_TFASTAINDEX_H_

#include <string>
#include "coretools/Files/TInputFile.h"

namespace genometools {

struct fastaIndex{
    std::string contig;
    uint64_t length;
    uint64_t offset;
    uint16_t lineBases;
    uint16_t lineBytes;
};

class TFastaIndex{
private:
    coretools::TInputFile _file;
    fastaIndex _fastaIndex;

public:
	using value_type      = fastaIndex;
	using const_reference = const fastaIndex &;
	using iterator        = coretools::TLazyIterator<TFastaIndex>;
	using const_iterator  = coretools::TLazyIterator<const TFastaIndex>;

	TFastaIndex() = default;
    TFastaIndex(std::string_view filename);
    void open(std::string_view filename);       
    void close();

    // getters    
    bool empty() const { return !_file.isOpen(); }

    const_reference front() const {
        return _fastaIndex;    
    }

    //read next
    void popFront();

    //iterators
	iterator begin() { return iterator{this}; }
	iterator end() { return iterator{}; }
	const_iterator begin() const { return const_iterator{this}; }
	const_iterator end() const { return const_iterator{}; }
};

} // end namesapce genometools

#endif /* GENOMETOOLS_CORE_GENOMETOOLS_TFASTAWRITER_H_ */
