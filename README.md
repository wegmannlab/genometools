# README #

Genometools is a collection of tools for genomic analyses.
It contains the common codebase for all projects that deal with genomic data.

## Installation ##
This repository can be used as a submodule inside another project. 
To clone your project with all submodules, use

``git clone --recurse-submodules https://madleina@bitbucket.org/wegmannlab/bangolin.git`` 

To add the submodule to an existing project, go to the root directory of your project and enter

```git submodule add -b master https://bitbucket.org/WegmannLab/genometools.git```

For more information about submodules, see 
https://bitbucket.org/wegmannlab/coretools/src/master/Readme.md 
or https://git-scm.com/book/en/v2/Git-Tools-Submodules.